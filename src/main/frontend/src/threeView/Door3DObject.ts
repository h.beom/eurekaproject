import {Group, Texture, PlaneGeometry, MeshBasicMaterial, Mesh , RepeatWrapping, TextureLoader} from 'three';
import Box3DObject from "./Box3DObject";
import {IBox3DObject} from './I3DModel';

export default class Door3DObject{

    private _hBeamGroup:Group = new Group();

    constructor( x:number, y:number, z:number, hbeamWidth:number , hbeamHeight:number, hbeamDepth:number, rotationX?:number , rotationY?:number , color:number=0x006766 ,borderColor:number=0x000000 ,  opacity?:number , frontTexture?:Texture , endTexture?:Texture ){

        // frontTexture?:Texture , endTexture?:Texture
        // 1.front texture 영역
        if( frontTexture != undefined ){
            let frontPlaneGeometry:PlaneGeometry = new PlaneGeometry(hbeamWidth,hbeamDepth,1,1 );
            let frontPlaneMaterial:MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff,map:frontTexture});
            let frontPlane:Mesh = new Mesh(frontPlaneGeometry, frontPlaneMaterial);
            frontPlane.position.set(0 , 0 ,(hbeamHeight / 2) + 0.15);
            this._hBeamGroup.add(frontPlane);

            if(endTexture == undefined){
                endTexture = frontTexture;
            }
        }

        const boxParameter:IBox3DObject = {
            x:0, y:0 ,z:0,
            width:hbeamWidth,
            height:(hbeamHeight - 0.3),
            depth:hbeamDepth , edgesVisible:true , color:color , borderColor:borderColor
        };

        let hBeamTop:Box3DObject = new Box3DObject( boxParameter );
        this._hBeamGroup.add(hBeamTop.getGroup());

        if( endTexture != undefined ){
            let endPlaneGeometry:PlaneGeometry = new PlaneGeometry(hbeamWidth,hbeamDepth,1,1 );
            let endPlaneMaterial:MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff,map:endTexture});
            let endPlane:Mesh = new Mesh(endPlaneGeometry, endPlaneMaterial);
            endPlane.position.set(0 , 0 ,-(hbeamHeight / 2) - 0.15);
            endPlane.rotation.y = -0.5 * Math.PI * (180 / 90);
            this._hBeamGroup.add(endPlane);
        }

        const positionY:number = z;//+ (hbeamDepth / 2) ;

        if(rotationX !=  undefined){
            this._hBeamGroup.rotation.x = -0.5 * Math.PI * (rotationX / 90);
        }
        if(rotationY != undefined) {
            this._hBeamGroup.rotation.y = -0.5 * Math.PI * (rotationY / 90);
        }
        this._hBeamGroup.position.set( x , positionY , y );
    }

    public getGroup():Group{
        return this._hBeamGroup;
    }
}
