import {Group} from 'three';
import {ICylinder3DObject} from "./I3DModel";
import Cylinder3DObject from "./Cylinder3DObject";

export default class Tire3DObject{

    private _axis:Group = new Group();

    constructor( parameter:ICylinder3DObject ){

        //parameter.radiusTop , parameter.radiusBottom , parameter.depth , 12 , 1 , false, 0 , 6.3
        let tire:Cylinder3DObject = new Cylinder3DObject({
            x:0,
            y:0,
            z:0,
            radiusTop:parameter.radiusTop ,
            radiusBottom:parameter.radiusBottom ,
            depth:parameter.depth,
            edgesVisible:false
        });
        this._axis.add(tire.getGroup());

        let wheel:Cylinder3DObject = new Cylinder3DObject({
            x:0,
            y:0,
            z:(parameter.depth / 2)+ 0.5,
            radiusTop:(parameter.radiusTop / 3 * 2) ,
            radiusBottom:(parameter.radiusTop / 3 * 2) ,
            depth:1,
            edgesVisible:false,
            color:0xFFFFFF
        });
        this._axis.add(wheel.getGroup());

        if(parameter.rotateX != undefined){

            this._axis.rotateX( parameter.rotateX * Math.PI / 180);
        }

        this._axis.position.set( parameter.x , parameter.z , parameter.y );
    }

    public getGroup():Group{
        return this._axis;
    }
}
