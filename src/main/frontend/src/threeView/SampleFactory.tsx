import React, {Component} from 'react';
import {
    Camera,
    CanvasTexture,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    RepeatWrapping,
    Scene,
    Texture,
    TextureLoader,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";

import Stats from 'stats.js';
import Box3DObject from "./Box3DObject";

import Line3DObject from "./Line3DObject";
import WindowsWell3DObject from "./WindowsWell3DObject";
import HBeam3DObject from "./HBeam3DObject";
import PartitionDoor3DObject from "./PartitionDoor3DObject";
import Door3DObject from "./Door3DObject";
import FFU3DObject from "./FFU3DObject";

export interface IcubeBox{x:number,y:number,z:number , color:number , width:number , height:number , depth:number};

class SampleFactory extends Component<any, any> {
    private mount: HTMLElement | null = null;
    private scene : Scene | null = null;
    private camera : Camera | null = null;
    private renderer:WebGLRenderer | null = null;
    private controls:OrbitControls | null = null;
    private frameId :number = 0;
    private modelItem :Object3D | null= null;
    private stats:Stats = new Stats();
    private flowTextureList:Array<CanvasTexture> = [];
    private _stripMesh:Array<Mesh> = [];

    private _textBox:Array<Box3DObject> = [];

    // private _cameraRotationZ:number = 0.00001;
    // private _cameraRotationY:number = 1000;
    // private _cameraRotationX:number = 0.00001;


    constructor(props:any) {
        super(props);

        this.start = this.start.bind(this);
        this.animate = this.animate.bind(this);
        this.stop = this.stop.bind(this);
        this.setModel = this.setModel.bind(this);
    }

    private setModel(model:Object3D){
        this.modelItem = model;
    }

    componentDidMount() {

        if(this.mount === null) {
            return;
        }
        let width:number=this.mount.clientWidth - 28;
        let height:number=this.mount.clientHeight - 10;
        // 무대 만들기
        const scene:Scene = new Scene();

        // 카메라 종횡비 설정
        const camera:Camera = new PerspectiveCamera(
            60,// 시야
            width / height , // 종횡비
            1, // 어디 부터 (0.1)
            10000  // 어디 까지 1000
        );

        // 화면 사이즈 설정x
        const renderer:WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor( 0xcccccc, 1 );

        //라이트
        let light:HemisphereLight = new HemisphereLight( 0xffffff, 0x444444 );
        light.position.set( 0, 200, 0 );
        scene.add( light );
        // 카메라 컨트롤
        this.controls = new OrbitControls(camera , renderer.domElement );
        this.controls.enableDamping = true;
        // this.controls.dampingFactor = 0.25;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 1000;
        this.controls.maxPolarAngle = Math.PI* 2;

        // let windowsWell:WindowsWell3DObject = new WindowsWell3DObject(0,0,0,250,5,100 , {windowWidth:100});
        // scene.add(windowsWell.getGroup());

        // 공장 외벽
        let depth : number = 75;
        let windowsWidth:number = 80;
        let windowWellArr:Array<any> = [
            /* top */
            {x:-225 , y:-200 , z:( depth / 2 ) , width:150 , height:5 , depth:depth , window:{windowWidth : windowsWidth}},
            {x:-75 , y:-200 , z:( depth / 2 ) , width:150 , height:5 , depth:depth , window:{windowWidth : windowsWidth}},
            {x:75 , y:-200 , z:( depth / 2 ), width:150 , height:5 , depth:depth , window:{windowWidth : windowsWidth}},
            {x:225 , y:-200 , z:( depth / 2 ), width:150 , height:5 , depth:depth , window:{windowWidth : windowsWidth}},

            /* left */
            {x:-297.5 , y:-135 , z:( depth / 2 ), width:125 , height:5 , depth:depth , window:{windowWidth : windowsWidth} , rotateY :90 },
            {x:-297.5 , y:-10 , z:( depth / 2 ), width:125 , height:5 , depth:depth , window:{windowWidth : windowsWidth}, rotateY :90 },

            /* right */
            {x:297.5 , y:-135 , z:( depth / 2 ), width:125 , height:5 , depth:depth , window:{windowWidth : windowsWidth} , rotateY :90 },
            {x:297.5 , y:-10 , z:( depth / 2 ), width:125 , height:5 , depth:depth , window:{windowWidth : windowsWidth}, rotateY :90 },

            /* buttom */
            {x:-225 , y:50 , z:( depth / 2 ), width:150 , height:5 , depth:depth , window:{windowWidth : windowsWidth}},
            {x:125 , y:50 , z:( depth / 2 ), width:350 , height:5 , depth:depth , window:{windowWidth : windowsWidth , windowX:200}},
        ];

        for(let item of windowWellArr){
            let windowsWell3DObject:WindowsWell3DObject= new WindowsWell3DObject( item.x,  item.y,item.z, item.width , item.height, item.depth , item.window , item.rotateY );
            scene.add(windowsWell3DObject.getGroup());
        }


        let hbeamDepth:number = 10;
        let hbeamDownDepth:number = 38.5;
        // 공장 H 빔
        let HbeamArr:Array<any> = [
            /* top */
            {x:-290 , y:-196.5 , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:-150 , y:-196.5 , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:0 , y:-196.5 , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:150 , y:-196.5 , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:290 , y:-196.5 , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },

            /* left */
            {x:-290 , y:-76.5, z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            /* right */
            {x:290 , y:-76.5 , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },

            /* buttom */
            {x:-290 , y:hbeamDownDepth , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:-155 , y:hbeamDownDepth , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:-45 , y:hbeamDownDepth , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:150 , y:hbeamDownDepth , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
            {x:290 , y:hbeamDownDepth , z:(depth/2) - 3 , width:depth , height:10 , depth:hbeamDepth , rotateX:90 , rotateY:90 },
        ];

        for(let item of HbeamArr){
            let hBeam3DObject:HBeam3DObject = new HBeam3DObject(item.x,  item.y,item.z, item.width , item.height, item.depth , item.rotateX , item.rotateY);
            scene.add(hBeam3DObject.getGroup());
        }

        let PartitionDoorArr:Array<any> = [
            /* buttom */
            {x:195, y:35, z:(depth/2), width:185 , height:5 , depth:depth  },

            {x:120, y:-50, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:155, y:-50, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:190, y:-50, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:225, y:-50, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:262.5, y:-50, z:(depth/2), width:40, height:5 , depth:depth  ,doorVisible:true  },

            {x:120, y:-100, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:155, y:-100, z:(depth/2), width:35 , height:5 , depth:depth ,doorVisible:true  },
            {x:190, y:-100, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:225, y:-100, z:(depth/2), width:35 , height:5 , depth:depth  },
            {x:262.5, y:-100, z:(depth/2), width:40 , height:5 , depth:depth  },

            {x:100, y:20, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },
            {x:100, y:-15, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 ,doorVisible:true },
            {x:100, y:-50, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },
            {x:100, y:-85, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },

            {x:285, y:20, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },
            {x:285, y:-15, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },
            {x:285, y:-50, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },
            {x:285, y:-85, z:(depth/2), width:35 , height:5 , depth:depth , rotateY:90 },
        ];


        let PartitionDoorArr2:Array<any> = [
            /* right */
            {x:180, y:-172.5, z:(depth/2), width: 50, height:5 , depth:depth , rotateY:90  },

            {x:197, y:-145, z:(depth/2), width:38 , height:5 , depth:depth  },
            {x:235, y:-145, z:(depth/2), width:40 , height:5 , depth:depth , doorVisible:true  },
            {x:275, y:-145, z:(depth/2), width:40 , height:5 , depth:depth  },
        ];

        PartitionDoorArr = PartitionDoorArr.concat(PartitionDoorArr2);


        let woodDoorURL:string = require("../textures/right_door.jpg");
        const leftWoodDoorTexture:Texture = new TextureLoader().load( woodDoorURL );
        const rightWoodDoorTexture:Texture = new TextureLoader().load( woodDoorURL );
        rightWoodDoorTexture.wrapS = RepeatWrapping;
        rightWoodDoorTexture.repeat.x = - 1;

        for(let item of PartitionDoorArr){
            let hBeam3DObject:PartitionDoor3DObject = new PartitionDoor3DObject(item.x,  item.y,item.z, item.width , item.height, item.depth , item.doorVisible , item.rotateY );
            scene.add(hBeam3DObject.getGroup());
        }

        let leftDoorURL:string = require("../textures/left_door.jpg");
        const leftFrontDoorTexture:Texture = new TextureLoader().load(leftDoorURL);
        const leftEndDoorTexture:Texture = new TextureLoader().load(leftDoorURL);
        leftEndDoorTexture.wrapS = RepeatWrapping;
        leftEndDoorTexture.repeat.x = - 1;

        let door:Door3DObject = new Door3DObject(-150,  55, (depth/2), 50, 5, depth, undefined , undefined , 0x6a8687 , 0x000000 , undefined , leftFrontDoorTexture , leftEndDoorTexture );
        scene.add(door.getGroup());

        let rightDoorURL:string = require("../textures/right_door.jpg");
        const rightFrontDoorTexture:Texture = new TextureLoader().load(rightDoorURL);
        const rightEndDoorTexture:Texture = new TextureLoader().load(rightDoorURL);
        rightEndDoorTexture.wrapS = RepeatWrapping;
        rightEndDoorTexture.repeat.x = - 1;
        let door2:Door3DObject = new Door3DObject(-50,  55, (depth/2), 50, 5, depth, undefined , undefined , 0x6a8687 , 0x000000 , undefined , rightFrontDoorTexture , rightEndDoorTexture);
        scene.add(door2.getGroup());

        let tileTextureURL:string = require("../textures/tile.jpg");
        const tileTextureLoader:Texture = new TextureLoader().load(tileTextureURL);

        let downGeometry:PlaneGeometry = new PlaneGeometry(180,140,1,1 );
        let downPlaneMaterial:MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff,map:tileTextureLoader});
        let downPlane:Mesh = new Mesh(downGeometry, downPlaneMaterial);
        // let down:Box3DObject = new Box3DObject( 190,  -35, 5, 180, 140, 5, true , 0x222222 , 0x000000 , undefined , undefined,  tileTextureLoader); // #6a8687
        downPlane.rotation.x = -0.5 * Math.PI * (90/ 90);
        downPlane.position.set(190,  0, -35,);
        scene.add(downPlane);

        let yellowBoxArr:Array<any> = [
            /* right */
            {x:-25, y:-125, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:-25, y:-75, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:25, y:-125, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:25, y:-75, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },

            {x:-25, y:-125, z:(45/2)+50, width: 50, height:50, depth:45 , rotateY:90  },
            {x:-25, y:-75, z:(45/2)+50, width: 50, height:50, depth:45 , rotateY:90  },
            {x:25, y:-125, z:(45/2)+50, width: 50, height:50, depth:45 , rotateY:90  },
            {x:25, y:-75, z:(45/2)+50, width: 50, height:50, depth:45 , rotateY:90  },

            {x:-175, y:-125, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:-175, y:-75, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:-125, y:-125, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:-125, y:-75, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },
            {x:-270, y:-125, z:(45/2), width: 50, height:50, depth:45 , rotateY:90  },

            // {x:197, y:-145, z:(depth/2), width:38 , height:5 , depth:depth  },
            // {x:235, y:-145, z:(depth/2), width:40 , height:5 , depth:depth , doorVisible:true  },
            // {x:275, y:-145, z:(depth/2), width:40 , height:5 , depth:depth  },
        ];



        let boxTextureURL:string = require("../textures/wooden-box-3d-model-c4d.jpg");
        const boxTextureLoader:Texture = new TextureLoader().load(boxTextureURL);
        for(let item of yellowBoxArr ){
            let down:Box3DObject = new Box3DObject({
                x:item.x,
                y:item.y,
                z:item.z ,
                width:item.width,
                height:item.height,
                depth: item.depth ,
                edgesVisible:true ,
                color:0xfbbc00,
                borderColor:0x000000,
                texture:boxTextureLoader
            }); // #6a8687
            scene.add(down.getGroup());
        }

        const lineZPosition:number = 25;

        const list:Array<{x:number, y:number, z:number}> = [
            {x:75, y:-125, z:lineZPosition},
            {x:225, y:-125, z:lineZPosition},
            {x:225, y:-175, z:lineZPosition},
            {x:275, y:-175, z:lineZPosition},
        ];
        const list2:Array<{x:number , y:number, z:number}> = [
            {x:-100, y:150, z:lineZPosition},
            {x:-100, y:25, z:lineZPosition},
            {x:-75, y:25, z:lineZPosition},
            {x:-75, y:-175, z:lineZPosition},
            {x:75, y:-175, z:lineZPosition},
            {x:75, y:25, z:lineZPosition},
            {x:-200, y:25, z:lineZPosition},
            {x:-200, y:-175, z:lineZPosition},
            {x:-80, y:-175, z:lineZPosition},
        ];
        const list3:Array<{x:number , y:number, z:number}> = [
            {x:75, y:-20, z:lineZPosition},
            {x:265, y:-20, z:lineZPosition},
            {x:265, y:-70, z:lineZPosition},
            {x:155, y:-70, z:lineZPosition},
            {x:155, y:-125, z:lineZPosition},
        ];

        let line:Line3DObject = new Line3DObject(list, 1 , 0x0000FF , 0.5 );
        let line2:Line3DObject = new Line3DObject(list2, 5 , 0xFF0000, 0.5 );
        let line3:Line3DObject = new Line3DObject(list3, 3 , 0x00FF00, 0.5 );
        scene.add(line.getGroup());
        scene.add(line2.getGroup());
        scene.add(line3.getGroup());

        this.flowTextureList = this.flowTextureList.concat(line.getTextureList());
        this.flowTextureList = this.flowTextureList.concat(line2.getTextureList());
        this.flowTextureList= this.flowTextureList.concat(line3.getTextureList());

        const floor1ceilingZ:number = 115;
        let down:Box3DObject = new Box3DObject({
            x:0,
            y:-75,
            z:floor1ceilingZ ,
            width:600,
            height:250,
            depth: 1,
            edgesVisible:false,
            color:0xFFFFFF ,
            opacity : 0.5
        });
        //0,  -75, floor1ceilingZ , 600, 250, 1, false, 0xFFFFFF , 0x000000 , undefined , 0.5

        scene.add(down.getGroup());

        let ffu:FFU3DObject = new FFU3DObject({
            x:0,
            y:-50,
            z:floor1ceilingZ  + 5,
            ffuWidtdh:100,
            ffuHeight:100,
            ffuDepth:10,
        });
        scene.add( ffu.getGroup() );


        const airLIneList2:Array<{x:number, y:number, z:number}> = [
            {x:10, y:-50, z:130},
            {x:10, y:20, z:130},
            {x:310, y:20, z:130},
            {x:310, y:20, z:10},
            {x:350, y:20, z:10},
        ];
        const airLIneList3:Array<{x:number, y:number, z:number}> = [
            {x:350, y:40, z:10},
            {x:310, y:40, z:10},
            {x:310, y:40, z:130},
            {x:-10, y:40, z:130},
            {x:-10, y:-50, z:130},
        ];
        let airLine2:Line3DObject = new Line3DObject(airLIneList2, 10 , 0xFF0000 , 0.5 , true );
        scene.add(airLine2.getGroup());
        let airLine3:Line3DObject = new Line3DObject(airLIneList3, 10 , 0x0000FF , 0.5 , true );
        scene.add(airLine3.getGroup());
        //
        // this.flowTextureList = this.flowTextureList.concat(airLine.getTextureList());
        this.flowTextureList = this.flowTextureList.concat(airLine2.getTextureList());
        this.flowTextureList = this.flowTextureList.concat(airLine3.getTextureList());
        //
        // // this._stripMesh = this._stripMesh.concat(airLine.getTextureMeshList());
        // this._stripMesh = this._stripMesh.concat(airLine2.getTextureMeshList());
        // this._stripMesh = this._stripMesh.concat(airLine3.getTextureMeshList());
        //
        let airCon:Box3DObject = new Box3DObject({
            x:370,
            y:40,
            z:0,
            width:50,
            height:150,
            depth: 50,
            edgesVisible:false,
            color:0xFFFFFF,
            rotateY:0 ,
            // textInfo:{
            //     width:50,
            //     height:150,
            //     text:"실외기"
            // }
        });
         //   370,  40, 0 , 150, 50, 50, false, 0xFFFFFF , 0x000000,  90, undefined,undefined , "실외기"
        scene.add(airCon.getGroup());

        let textBox:Box3DObject = new Box3DObject({
            x:500,
            y:500,
            z:0,
            width:1300,
            height:150,
            depth: 1,
            edgesVisible:false,
            color:0xFFFFFF,
            opacity: 0.5,
            textInfo:{
                width:1200,
                height:200,
                text:"Yura Vietnam C building."
            }
        });

        scene.add(textBox.getGroup());
        //
        // this._textBox.push(airCon);


        // grid Tile 기준 정보
        let gridTile:{width:number, height:number}={ width:50 , height:50};
        let gridwidth:number = 600;
        let gridheight:number = 400;

        let planeGeometry:PlaneGeometry = new PlaneGeometry(gridwidth,gridheight,gridwidth / gridTile.width,gridheight / gridTile.height );
        let planeMaterial:MeshBasicMaterial = new MeshBasicMaterial({color: 0x000000, wireframe: true});
        let grid = new Mesh(planeGeometry, planeMaterial);
        grid.position.set (0, 0, 0);
        grid.rotation.x = -0.5 * Math.PI;
        scene.add(grid);

        // 카메라 위치 설정
        //camera.position.set(0,500,1000);
        camera.position.set(0,1500,0);

        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;

        this.mount.appendChild(this.renderer.domElement);

        this.stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom

        let stateHtml:HTMLElement= this.stats.dom;
        stateHtml.setAttribute("style", "position: absolute; top: 52px; cursor: pointer; opacity: 0.9; z-index: 10000;");
        this.mount.appendChild(stateHtml);

        this.start();
    }
    start():void{
        if(this.frameId == 0){
            this.frameId = requestAnimationFrame(this.animate);
        }
    }

    animate = () => {

        this.stats.begin();

        if(this.modelItem != null){

        }
        // this._cameraRotationZ = this.camera.rotation.z;
        // this._cameraRotationY = this.camera.rotation.y;
        // this._stripMesh.rotation.x = this.camera.rotation.x;
        // this._stripMesh.rotation.z = this.camera.rotation.y;

        if(this.camera != null){
            for(let item of this._textBox){
                // item.setRotation(this.camera.rotation.y);
                //, this.camera.rotation.x
            }
        }
        for(let item of this.flowTextureList){
            item.offset.x -=(0.01 * 3 % 1);
        }
        this.stats.end();

        this.renderScene();
        this.frameId = window.requestAnimationFrame(this.animate)
    }



    componentWillUnmount() {
        if(this.renderer == null || this.mount == null ){
            return;
        }
        this.stop();
        this.mount.removeChild(this.renderer.domElement);
    }

    stop = () => {
        cancelAnimationFrame(this.frameId)
    }

    renderScene() {
        if(this.renderer == null || this.scene == null || this.camera == null){
            return;
        }

        if(this.controls != null){
            this.controls.update();
        }

        this.renderer.render(this.scene, this.camera);
    }

    render() {
        // @ts-ignore
        return (
            <div style={{width:"100%" , height:"100%"}}
                 ref={(mount) => {
                     if(mount === null){
                         return;
                     }
                     this.mount = mount

                 }} />
        );
    }
}

export default SampleFactory;