import React, {Component, RefObject} from 'react';
import {
    Group,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Scene,
    Texture,
    TextureLoader,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import CoordinateHelper from "./CoordinateHelper";
import {IProcessAnimationInfo, IProcessMapInfo, IProcessModelInfo, IProcessModelMain} from "./IProcessModelMain";
import Model3DObject from "./Model3DObject";
import Box3DObject from "./Box3DObject";
import * as createjs from "createjs-module";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import '.././css/IndexPage.css';
import ModelService from "../admin/monitoring/typescript/ModelService";
import {ProcessModelType} from "./IProcessModel";
import Conveyor3DObject from "./Conveyor3DObject";
import Load3DObject from "./Load3DObject";

@inject('SimulationStore')
@observer
class ProcessModelMain extends Component<IProcessModelMain, IProcessModelMain> {

    // movieClip과 연결된 타임라인, 인스턴스에 전달하기위함 (일시중지, 되감기등 지원)
    private _timeline: createjs.Timeline = new createjs.Timeline([], {}, {paused: true});
    // private animationTween:Group = new Group();
    // private _tweenPauseLastAnimateTime:number =0;//Tween 정지후 마지막 Animate 시간
    // private _tweenPauseTimeSum:number=0;//Tween총 시간 일시 중지 (Tween의 타임 라인을 다시 평행으로 이동하는 데 사용)
    private _processModelInitPosition: { [index: string]: { object3D: Model3DObject, modelItem: IProcessModelInfo, processAnimationData: Array<IProcessAnimationInfo> } } = {};
    private _threejsConvas: RefObject<HTMLDivElement> | null | undefined;
    private iisUrl: string = 'http://115.145.177.34:3486';
    private mount: HTMLElement | null = null;
    private scene: Scene | null = null;
    private camera: PerspectiveCamera | null = null;
    private renderer: WebGLRenderer | null = null;
    private controls: OrbitControls | null = null;
    private frameId: number = 0;

    constructor(props: IProcessModelMain) {
        super(props);
        this._threejsConvas = React.createRef();
        this.state = this.props;
    }

    /**
     * @param width
     * @param height
     */
    @autobind
    public handleResize(width: number, height: number): void {

        if (this.camera != null) {
            this.camera.aspect = width / height;
            this.camera.updateProjectionMatrix()
        }
        if (this.renderer != null) {
            this.renderer.setSize(width, height);
        }
    }

    public updateDimensions() {

        let mount: RefObject<HTMLDivElement> | null | undefined = this._threejsConvas;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }

        let width: number = mount.current.clientWidth - 2;
        let height: number = mount.current.clientHeight - 7;
        this.handleResize(width, height);
    };

    componentDidMount() {
        this.load3DModelInfo();
    }


    /**
     * @description
     */
    private load3DModelInfo() {
        let mount: RefObject<HTMLDivElement> | null | undefined = this._threejsConvas;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }
        window.addEventListener('resize', this.updateDimensions.bind(this));

        let width: number = mount.current.clientWidth - 2;
        let height: number = mount.current.clientHeight - 7;


        // 무대 만들기
        const scene: Scene = new Scene();

        //지오메트리 설정
        let coordinatehelper: CoordinateHelper = new CoordinateHelper();
        scene.add(coordinatehelper.init());

        // 카메라 종횡비 설정
        const camera: PerspectiveCamera = new PerspectiveCamera(
            60,// 시야
            width / height, // 종횡비
            0.1, // 어디 부터 (0.1)
            1000  // 어디 까지 1000
        );

        //배경회색설정
        let backgroundColor: number = 0xcccccc;
        //부모컴포넌트에서 설정한 지오메틑리 배경색이 있으면 그걸로 선택.
        if (this.props.backgroundColor != undefined) {
            backgroundColor = this.props.backgroundColor;
        }

        // 화면 사이즈 설정x
        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(backgroundColor, 1);

        //라이트
        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);
        light.position.set(0, 200, 0);
        scene.add(light);

        // 카메라 컨트롤
        this.controls = new OrbitControls(camera, renderer.domElement);
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.minDistance = 50;
        this.controls.maxDistance = 500;
        this.controls.maxPolarAngle = Math.PI / 2;

        if (this.state.mapInfo == undefined || this.state.modelInfo == undefined) {
            return;
        }

        let mapInfo: { [index: string]: IProcessMapInfo } = {};
        // Map 정보 표기
        this.state.mapInfo.forEach(function (item: IProcessMapInfo): void {
            mapInfo[item.mapId] = item;

            // 1. Map Image 로딩 ...
            let loadTextURL: string = "/dexi-web/sim/getMapFilePatch.do?mapid=" + item.mapId + '&modelItemId=' + item.modelItemId;
            let loader: TextureLoader = new TextureLoader();

            // 2. Map Image 배치 ...
            let planeGeometry: PlaneGeometry = new PlaneGeometry(item.mapRangeLenx, item.mapRangeLeny, item.mapRangeLenx / item.mapGridx, item.mapRangeLeny / item.mapGridy);
            loader.load(
                loadTextURL,
                function (texture: Texture): void {
                    let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff, map: texture});
                    let mapImgMesh: Mesh = new Mesh(planeGeometry, planeMaterial);
                    mapImgMesh.position.set(0, -5, 0);
                    mapImgMesh.rotation.x = -0.5 * Math.PI;
                    scene.add(mapImgMesh);
                },
                undefined,
                // onError callback
                function (event: ErrorEvent): void {
                    let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff});
                    let mapImgMesh: Mesh = new Mesh(planeGeometry, planeMaterial);
                    mapImgMesh.position.set(0, -5, 0);
                    mapImgMesh.rotation.x = -0.5 * Math.PI;
                    scene.add(mapImgMesh);
                }
            );
            // 3. Grid 설정 ...
            let planeMaterialGrid: MeshBasicMaterial = new MeshBasicMaterial({color: 0xe3e3e3, wireframe: true});
            let grid: Mesh = new Mesh(planeGeometry, planeMaterialGrid);
            grid.position.set(0, -5, 0);
            grid.rotation.x = -0.5 * Math.PI;
            scene.add(grid);
        });

        // 공장 머신 배치 ..
        const modelList: Array<IProcessModelInfo> = this.state.modelInfo;


        let drainItem: IProcessModelInfo;
        // 공장 머신 배치 ..
        for (let item of modelList) {

            const modelItem: IProcessModelInfo = item;
            const positionX: number = -(mapInfo[item.mapId].mapRangeLenx / 2) + modelItem.stencPosx + (modelItem.stencLenx / 2);
            const positionY: number = -(mapInfo[item.mapId].mapRangeLeny / 2) + modelItem.stencPosy;
            const positionZ: number = modelItem.stencPosz + (modelItem.stencLenz / 2);
            const fillColor: number = (modelItem.fillColor != null) ? parseInt('0x' + modelItem.fillColor.toString(), 16) : 0xFFFFFF;


            if (item.stenc3did === 'EmptyCode') {
                switch( modelItem.stencNm ) {

                    case ProcessModelType.CONVEYOR :
                        let conveyor3DObject:Conveyor3DObject = new Conveyor3DObject( positionX , positionY, positionZ ,  modelItem.stencLenx,  modelItem.stencLeny , modelItem.stencLenz );
                        scene.add( conveyor3DObject.getGroup() );
                        break;
                        case ProcessModelType.CONVEYOR2 :
                        let conveyor3DObject2:Conveyor3DObject = new Conveyor3DObject( positionX , positionY, positionZ ,  modelItem.stencLenx,  modelItem.stencLeny , modelItem.stencLenz );
                        scene.add( conveyor3DObject2.getGroup() );
                        break;
                    default :
                        let glbURL:string = "/dexi-web/sim/getSimulationModelFile.do?stenc3did="+modelItem.stenc3did;
                        let obj:GLTFLoader = new GLTFLoader();
                        obj.load( glbURL, function ( object  : GLTF ) {
                                let object3dModel:Object3D =  object.scene.children[0];
                                let object3D:Model3DObject = new Model3DObject();
                                object3D.init({obj:object3dModel , modelItem : modelItem , mapRangeLenx:mapInfo[item.mapId].mapRangeLenx , mapRangeLeny:mapInfo[item.mapId].mapRangeLeny , text:modelItem.stencNm })
                                scene.add( object3D.getGroup() );

                            },undefined ,
                            function(event:ErrorEvent):void{
                                let box3DObject:Box3DObject = new Box3DObject({
                                    x:positionX ,
                                    y:positionY +(modelItem.stencLeny / 2),
                                    z:positionZ,
                                    width:modelItem.stencLenx,
                                    height:modelItem.stencLeny,
                                    depth:50 ,
                                    edgesVisible:false ,
                                    color:0xFFFF00
                                });
                                //positionX  , positionz, positionY +(modelItem.stencLeny / 2) , modelItem.stencLenx , modelItem.stencLeny , modelItem.stencLenz, false , 0xFFFF00 );;
                                scene.add( box3DObject.getGroup() );
                            });
                        break;
                }

            } else {


                let group: Group = new Group();
                //맵 범위
                const modelService: ModelService = new ModelService(
                    mapInfo[item.mapId].mapRangeLenx,
                    mapInfo[item.mapId].mapRangeLeny,
                    50,
                );

                let glbURL: string = this.iisUrl + item.stenc3dfile
                let obj: GLTFLoader = new GLTFLoader();
                obj.load(glbURL, function (object: GLTF) {
                        let obj: Object3D = object.scene;
                        obj = modelService.setModelSize(
                            obj,
                            modelItem.stencLenx,
                            modelItem.stencLeny,
                            25,
                        );
                        obj.rotateX(1.57 * (270 / 90))
                        group.add(obj);
                        group.rotation.x = 1.57 * (90 / 90);
                        obj.position.set(positionX, positionY + (modelItem.stencLeny), positionZ);
                        scene.add(group);
                    }, undefined,
                    function (event: ErrorEvent): void {
                        let box3DObject: Box3DObject = new Box3DObject({
                            x: positionX,
                            y: positionY + (modelItem.stencLeny / 2),
                            z: 25,
                            width: modelItem.stencLenx,
                            height: modelItem.stencLeny,
                            depth: 50,
                            edgesVisible: false,
                            color: 0xFFFF00
                        });
                        //positionX  , positionz, positionY +(modelItem.stencLeny / 2) , modelItem.stencLenx , modelItem.stencLeny , modelItem.stencLenz, false , 0xFFFF00 );;
                        scene.add(box3DObject.getGroup());
                    });
            }
            if(item.stencNm == "Drain"){
                drainItem = modelItem;
            }

        }
        // animation 용 Process 모델 호출 .
        let processObj: { [index: string]: { info: IProcessModelInfo, animation: Array<IProcessAnimationInfo> } } = {};
        let processAnimation: Array<IProcessAnimationInfo> | undefined = this.state.processAnimationData;
        if (processAnimation !== undefined && processAnimation !== null) {
            processAnimation.forEach(function (item: IProcessAnimationInfo) {
                if (processObj[item.productIdx] == undefined) {
                    const modelItem: IProcessModelInfo = {
                        stencId: "3d987a72-e917-4706-9f32-eee7447d402d",
                        stencNm: "PCB_청소_1",
                        stencLenx: 10,
                        stencLeny: 10,
                        stencLenz: 10,
                        stencPosx: item.stencPosx,
                        stencPosy: item.stencPosy,
                        stencPosz: 15,
                        zIndex: 1,
                        mapId: item.mapId,
                        modelScale: 10,
                        rotAngle: 1,
                        stenc3dTp: "Machine",
                        stenc3did: "9472ca43-a2ca-46a0-82c3-980abfbf3a3d",
                        stenc3dinfoid: "64073cb0-6549-4188-a1e0-ad6274176678",
                        fillColor: 0x000000,
                        lineColor: 0x000000,
                    };
                    processObj[item.productIdx] = {info: modelItem, animation: []};
                }
                processObj[item.productIdx].animation.push(item);
            });

        }
        // Process 모델 배치 및 animation
        for(let productIdx in processObj){

            let glbURL:string = require("../model/Product(PCB).glb")
            let obj:GLTFLoader = new GLTFLoader();

            let gltfloaderCallBack:( object : GLTF ) => void = function(this:ProcessModelMain , object  : GLTF):void{
                const modelItem:IProcessModelInfo = processObj[productIdx].info;
                const processAnimationList:Array<IProcessAnimationInfo> = processObj[productIdx].animation

                // 모델 배치 부분
                let object3dModel:Object3D =  object.scene.children[0];
                let object3D:Model3DObject = new Model3DObject();
                //, text:modelItem.stencNm
                object3D.init({obj:object3dModel , modelItem : modelItem , mapRangeLenx:mapInfo[modelItem.mapId].mapRangeLenx , mapRangeLeny:mapInfo[modelItem.mapId].mapRangeLeny });
                // object3D.getGroup().visible = false;
                scene.add( object3D.getGroup() );

                let processModelInitPosition:{ [index:string] : { object3D:Model3DObject, modelItem:IProcessModelInfo, processAnimationData:Array<IProcessAnimationInfo>} } = this._processModelInitPosition;
                processModelInitPosition[object3D.getGroup().uuid] = {
                    modelItem : modelItem,
                    object3D:object3D,
                    processAnimationData:processAnimationList
                };

                if(processAnimationList.length == 0){
                    return;
                }

                let cubeTween:createjs.Tween = new createjs.Tween(object3D.getGroup().position);
                processAnimationList.forEach(function(item:IProcessAnimationInfo){
                    cubeTween.to({x:item.stencPosx,y:0,z:item.stencPosy - modelItem.stencPosy } , item.startTime * 1 ).wait(item.waitingTime);
                    // cubeTween.wait(item.waitingTime * 1);
                });

                // animationList.push(cubeTween);

                this._timeline.addTween(cubeTween)

                // this.setAnimation( object3D , processAnimationList , modelItem );

            }.bind(this);
            obj.load( glbURL, gltfloaderCallBack ,undefined ,
                function(event:ErrorEvent):void{

                    console.log(drainItem);
                    console.log(event);
                });

        }

        // 카메라 위치 설정
        //camera.position.set(0,500,1000);
        camera.position.set(200, 200, 200);


        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;
        mount.current.appendChild(this.renderer.domElement);
        this.start();
    }

    @autobind
    public change(time: number) {
        this._timeline.setPosition(time);
    };


    // private setAnimation(object3D:Model3DObject,  processAnimationList:Array<IProcessAnimationInfo> , info:IProcessModelInfo  ){
    //     let startPosition:{x: number, y: number, z: number } = {x: 0 , y: 0, z: 0};
    //     let animationList:Array<Tween> = [];
    //
    //     // const processSuccessFaillEvent:Function | undefined = this.props.processSuccessFailFun;
    //
    //
    //     if(processAnimationList.length > 0){
    //
    //     }
    //
    //
    //     for(let num:number = 0; num <= processAnimationList.length ; num++){
    //         if(processAnimationList[num] == undefined) {
    //             break;
    //         }
    //         const processAnimationItem:IProcessAnimationInfo = processAnimationList[num];
    //         let objectTween:Tween = new Tween(startPosition, this.animationTween);
    //         objectTween.onUpdate(function(object:any):void{
    //             object3D.getGroup().position.set( object.x , 0 ,object.z );
    //         });
    //
    //         objectTween.to({
    //             x:( processAnimationItem.stencPosx  - info.stencPosx  ) ,
    //             y:0 ,
    //             z: processAnimationItem.stencPosy - info.stencPosy
    //         } ,processAnimationItem.startTime * 1 );
    //         // console.log("this.props.animationSpeed:::"+this.props.animationSpeed);
    //         if( processAnimationList.length == (num + 1) ){
    //             objectTween.onComplete(function(object:any){
    //
    //                 // object3D.getGroup().visible = false;
    //             });
    //         }
    //         animationList.push(objectTween);
    //
    //
    //         let waitingTween:Tween = new Tween(startPosition, this.animationTween);
    //         waitingTween.delay(processAnimationItem.waitingTime * 1);
    //         animationList.push(waitingTween);
    //     }
    //
    //     for(let num:number =0; num <= animationList.length ; num++){
    //         let nowTween:Tween  = animationList[num];
    //         let nextIdx:number  = num+1;
    //         let nextTween:Tween | undefined = animationList[nextIdx];
    //
    //         if(nextTween !=undefined){
    //             nowTween.chain(nextTween);
    //         }
    //     }
    //     let processModelInitPosition:{ [index:string] : { object3D:Model3DObject, modelItem:IProcessModelInfo, processAnimationData:Array<IProcessAnimationInfo> } } = this._processModelInitPosition;
    //     processModelInitPosition[object3D.getGroup().uuid].panimationTween = animationList;
    //     animationList[0].start(0);
    // }


    // public animationTweenReset(){
    //     let processModelInitPosition:{ [index:string] : { object3D:Model3DObject, modelItem:IProcessModelInfo, processAnimationData:Array<IProcessAnimationInfo> } } = this._processModelInitPosition;
    //     for(let uuid in processModelInitPosition){
    //         let object3D:Model3DObject = processModelInitPosition[uuid].object3D;
    //         object3D.getGroup().visible = false;
    //         let objectTween:Array<Tween> = processModelInitPosition[uuid].panimationTween;
    //
    //         if(objectTween.length == 0){
    //             break;
    //         }
    //         objectTween[0].stop();
    //         processModelInitPosition[uuid].panimationTween = [];
    //     }
    //     // Tween.Group 내에 모든 애니메이션 삭제
    //     this.animationTween.removeAll();
    //
    //     // 애니메이션 재설정
    //     // this._processModelInitPosition = {};
    //     for(let uuid in processModelInitPosition){
    //         let object3D:Model3DObject = processModelInitPosition[uuid].object3D;
    //         let modelItem:IProcessModelInfo = processModelInitPosition[uuid].modelItem;
    //         let processAnimationData:Array<IProcessAnimationInfo> = processModelInitPosition[uuid].processAnimationData;
    //
    //         this.setAnimation( object3D , processAnimationData , modelItem );
    //     }
    // }

    // public animationPlay(){
    //     if(this._btnPalyFirstClick == false){
    //         // this.animationTweenReset();
    //         this._btnPalyFirstClick = true;
    //     }
    //     this.setState({playType:IAnimationPlayType.PLAY});
    // }
    //
    // public animationPause(){
    //     this.setState({playType:IAnimationPlayType.PAUSE});
    // }
    //
    // public animationStop(){
    //     this.animationTweenReset();
    //     this.setState({playType:IAnimationPlayType.PAUSE});
    // }

    @autobind
    start(): void {
        if (this.frameId == 0) {
            this.frameId = requestAnimationFrame(this.animate);
        }
    }

    /**
     * 재생 , 일시정지 , 정지 참고 <br />
     * https://github.com/tweenjs/tween.js/issues/341
     * @param time
     */
    @autobind
    private animate(time: number) {
        // if(this.state.playType == IAnimationPlayType.PAUSE ){
        //     if (this._tweenPauseLastAnimateTime === 0) {
        //         //일시 정지 감지, 현재 캐시 time
        //         this._tweenPauseLastAnimateTime = time;
        //         console.debug('Tween 일시정지 (animate time):', this._tweenPauseLastAnimateTime);
        //     } else {
        //         //일시 정지 기간을 계산하고 누적
        //         this._tweenPauseTimeSum += time - this._tweenPauseLastAnimateTime;
        //         //현재 재 캐시 time
        //         this._tweenPauseLastAnimateTime = time;
        //     }
        // }else if(this._tweenPauseLastAnimateTime !== 0){
        //     //현재 시간 캐시 재설정
        //     this._tweenPauseLastAnimateTime = 0;
        // }
        //
        // this.animationTween.update(time - this._tweenPauseTimeSum);

        this.renderScene();
        this.frameId = window.requestAnimationFrame(this.animate)
    }


    componentWillUnmount() {

        let mount: RefObject<HTMLDivElement> | null | undefined = this._threejsConvas;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }

        if (this.renderer == null) {
            return;
        }

        this.stop();
        mount.current.removeChild(this.renderer.domElement);
        window.removeEventListener('resize', this.updateDimensions);
    }

    @autobind
    stop() {
        cancelAnimationFrame(this.frameId)
    };

    renderScene() {
        if (this.renderer == null || this.scene == null || this.camera == null) {
            return;
        }

        if (this.controls != null) {
            this.controls.update();
        }
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return (
            <div className={"col-5 col-2-5 col-3-5 col-4-5 col-5-5 col-6-5"}
                 style={{height: 250}}
                 ref={this._threejsConvas}/>
        );
    }
}

export default ProcessModelMain;