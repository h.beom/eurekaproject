import {BoxGeometry, EdgesGeometry, Group, LineBasicMaterial, LineSegments} from 'three';
import Box3DObject from "./Box3DObject";
import {IXcrossFrame3DObject} from "./I3DModel";

export default class XcrossFrame3DObject{

    private _xcrossFrameGroup:Group = new Group();
    private _totalSize:number = 20;
    private _parameter:IXcrossFrame3DObject = {x:0,y:0,z:0,width:0, frameHeight:0, depth:0, edgesVisible:false, color:0x000000 };

    private _xcrossBoxLeftList:Array<Box3DObject> = [];
    private _xcrossBoxRightList:Array<Box3DObject> = [];

    constructor( parameter:IXcrossFrame3DObject ){

        if(parameter.depth < 20){
            parameter.depth = 20;
        }
        this._parameter = parameter;
        this._totalSize = parameter.depth;

        let xcrossY:number = -(parameter.frameHeight/2);
        let xcrossAvgZPosition:number = parameter.depth / 4;
``
        const wheight:number = Math.sqrt( Math.pow(parameter.width , 2) + Math.pow(xcrossAvgZPosition,2) );
        const donumber:number = Math.PI / 2.0 - Math.acos(xcrossAvgZPosition / wheight );
        const donumber2:number = donumber * 180 / Math.PI; // 각도로 변환


        for(let xcrossPositionZ= -xcrossAvgZPosition * 2 + (xcrossAvgZPosition / 2) ; xcrossPositionZ<(xcrossAvgZPosition * 2);xcrossPositionZ+=xcrossAvgZPosition){
            let xcrossLeft:Box3DObject = new Box3DObject({
                x:0,
                y:xcrossY,
                z:xcrossPositionZ,
                width:wheight,
                height:parameter.frameHeight,
                depth:5 ,
                rotateZ:donumber2 ,
                edgesVisible:true ,
                color:parameter.color,
            });
            this._xcrossFrameGroup.add(xcrossLeft.getGroup());
            this._xcrossBoxLeftList.push(xcrossLeft);

            let xcrossRight:Box3DObject = new Box3DObject({
                x:0,
                y:-(xcrossY),
                z:xcrossPositionZ,
                width:wheight,
                height:parameter.frameHeight,
                depth:5,
                rotateZ:-donumber2 ,
                edgesVisible:true ,
                color:parameter.color,
            });
            this._xcrossFrameGroup.add(xcrossRight.getGroup());
            this._xcrossBoxRightList.push(xcrossRight);
        }

        if(parameter.rotateX != undefined){
            this._xcrossFrameGroup.rotateX( parameter.rotateX * Math.PI / 180);
        }
        if(parameter.rotateY != undefined){
            this._xcrossFrameGroup.rotateY( parameter.rotateY * Math.PI / 180);
        }
        if(parameter.rotateZ != undefined){
            this._xcrossFrameGroup.rotateZ( parameter.rotateZ * Math.PI / 180);
        }
        // let zposition:number = parameter.z + (rectFrameDepth / 2) - (rectFrameHeight/2);
        this._xcrossFrameGroup.position.set( parameter.x , parameter.z , parameter.y );
    }

    public getGroup():Group{
        return this._xcrossFrameGroup;
    }


    public setZSize(size:number){
        this._totalSize= size;
        const parameter:IXcrossFrame3DObject = this._parameter;

        const xcrossAvgZPosition:number = this._totalSize / 4;

        const wheight:number    = Math.sqrt( Math.pow(parameter.width , 2) + Math.pow(xcrossAvgZPosition,2) );
        const radiansNum:number = Math.PI / 2.0 - Math.acos(xcrossAvgZPosition / wheight );

        let num:number = 0;

        for(let xcrossPositionZ= -xcrossAvgZPosition * 2 + (xcrossAvgZPosition / 2) ; xcrossPositionZ<(xcrossAvgZPosition * 2);xcrossPositionZ+=xcrossAvgZPosition){
            let boxLeft:Box3DObject = this._xcrossBoxLeftList[num];
            let boxRight:Box3DObject = this._xcrossBoxRightList[num];

            boxLeft.setSize({width:wheight});
            boxRight.setSize({width:wheight});
            boxLeft.getGroup().position.y = xcrossPositionZ;
            boxRight.getGroup().position.y = xcrossPositionZ;

            let positionZ:number = radiansNum;

            boxLeft.getGroup().rotation.z = positionZ;
            boxRight.getGroup().rotation.z = -(positionZ);
            num++;
        }
    }
}
