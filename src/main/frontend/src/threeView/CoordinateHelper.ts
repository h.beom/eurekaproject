import {Geometry, Group, Line, LineBasicMaterial, Vector3, ConeGeometry, MeshBasicMaterial, Mesh, BufferGeometry,
    Float32BufferAttribute,
    MeshPhongMaterial,
    LineSegments,
    DoubleSide,
    CylinderGeometry} from 'three';

export default class CoordinateHelper{

    private _axis:Group = new Group();
    private _lineWidth:number =1;

    constructor(){}

    public init():Group{
        //red bar ..
        this.addLine([-340, 0.001, 0.001] , [340, 0.001, 0.001] , 0xFF0000);
        this.addCone(-340 , 0.001,0.001,0.001,0.001,0.5 * Math.PI, 0xFF0000);
        this.addCone(340 , 0.001,0.001,0.001,0.001,-0.5 * Math.PI, 0xFF0000);
        //blue bar ..
        this.addLine([0.001, -240, 0.001] , [0.001, 240, 0.001] , 0x00FF00);
        this.addCone(0.001 , 240,0.001,0.001,0.001,0.001, 0x00FF00);
        this.addCone(0.001 , -240,0.001,1 * Math.PI,0.001,0.001, 0x00FF00);

        //green bar ..
        this.addLine([0.001, 0.001, -240] , [0.001, 0.001, 240] , 0x0000FF);
        this.addCone(0.001 , 0.001,-240,-0.5 * Math.PI,0.001,0.001, 0x0000FF);
        this.addCone(0.001 , 0.001,240,0.5 * Math.PI,0.001,0.001, 0x0000FF);

        return this._axis;
    }

    private addLine(startPoint:Array<number>=[0.001,0.001,0.001] , endPoint:Array<number>=[0.001,0.001,0.001] , colorValue:number=0x000000 ):void {

        let lineWidthValue:number = this._lineWidth;

        if(startPoint.length != 3){
            console.error("start Point x y z 좌표를 정확히 입력해주세요.");
            return ;
        }
        if(endPoint.length != 3){
            console.error("end Point x y z 좌표를 정확히 입력해주세요.");
            return ;
        }

        let geometry:Geometry = new Geometry();
        geometry.vertices.push( new Vector3(startPoint[0], startPoint[1], startPoint[2]), new Vector3(endPoint[0], endPoint[1], endPoint[2]) );
        let material:LineBasicMaterial = new LineBasicMaterial({color: colorValue, linewidth: lineWidthValue });
        let axis:Line = new Line(geometry, material);
        this._axis.add(axis);
    }

    private addCone(positionX:number, positionY:number,positionZ:number , rotateX:number, rotateY:number, rotateZ:number , color:number ):void{
        let geometry:ConeGeometry = new ConeGeometry( 5, 10, 8 , 1 , false , 0.001 , 6.3 );
        let material:MeshBasicMaterial = new MeshBasicMaterial( {color: color} );

        let cone:Mesh = new Mesh( geometry, material );
        cone.position.x = positionX;
        cone.position.y = positionY;
        cone.position.z = positionZ;
        cone.rotation.x = rotateX;
        cone.rotation.y = rotateY;
        cone.rotation.z = rotateZ;

        this._axis.add(cone);
    }

}
