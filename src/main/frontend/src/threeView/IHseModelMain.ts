export interface IHseAnimationInfo{
    time:number,
    x:number,
    y:number,
    z:number,
    scissorSize?:number,
    rotationY?:0|90|180|270|360,
    rotationZ?:number,
    completeFunction?:Function
}
