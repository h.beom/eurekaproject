import {CanvasTexture, ClampToEdgeWrapping, Group, LinearFilter, Object3D, Sprite, SpriteMaterial} from 'three';
import Box3DObject from "./Box3DObject";
import {ICylinder3DObject, IScissorLift3DObject, IXcrossFrame3DObject} from "./I3DModel";
import Tire3DObject from "./Tire3DObject";
import ReactFrame3DObject from "./ReactFrame3DObject";
import XcrossFrame3DObject from "./XcrossFrame3DObject";
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";

export default class ScissorLift3DObject{

    private _scissorLift:Group = new Group();
    private _scaffolding:Group = new Group();
    private _xcross:XcrossFrame3DObject | null = null;
    private _xcrossPositionZ:number = 0;
    private _scaffoldingPositionZ:number = 0;
    private _leftSize:number = 20;
    private _workerGap:number = 50;
    private _workerObject:Object3D | null = null;


    constructor(paramter:IScissorLift3DObject){
        this.liftFrame( paramter.width , paramter.height , paramter.depth );

        const defaultScissorSize:number = 10;

        let xcrossPositionZ:number = (paramter.depth / 4) + (paramter.depth / 2)  + (defaultScissorSize / 2);
        this._xcrossPositionZ = xcrossPositionZ;
        // X 사다리 영역
        let parameter:IXcrossFrame3DObject = {
            x:0,
            y:0,
            z:xcrossPositionZ,
            width:100,
            frameHeight:20,
            depth:defaultScissorSize,
            edgesVisible:true,
            color:0xdbc58e
        };

        let xcross:XcrossFrame3DObject = new XcrossFrame3DObject( parameter );
        this._scissorLift.add( xcross.getGroup() );

        this._xcross = xcross;


        let scaffoldingPositionZ:number = xcrossPositionZ - (this._leftSize / 2) + (defaultScissorSize / 2);

        this._scaffoldingPositionZ = scaffoldingPositionZ;


        this.scaffolding(0,0,scaffoldingPositionZ , paramter.width, paramter.height, paramter.depth);

        // const positionY:number = paramter.z + (paramter.ffuDepth / 2);
        //

        let glbURL:string = require("../model/WorkerAtRoof.glb");
        let obj:GLTFLoader = new GLTFLoader();

        const leftGroup:Group = this._scissorLift;


        if(paramter.IsRiding == true){
            const workerGap:number = this._workerGap;

            let modelLoadFunction:(gltf: GLTF) => void = ( object  : GLTF ) =>{
                let obj : Object3D =  object.scene.children[0];
                obj.position.x = 70;
                obj.position.y = scaffoldingPositionZ + workerGap;
                obj.position.z = -5;
                obj.scale.set( 150, 150, 150 );
                obj.rotation.y = -0.5 * Math.PI;
                obj.rotateY( 1.57 * ( 0/ 90) );

                this._workerObject = obj;
                leftGroup.add(obj);
            }
            obj.load( glbURL, modelLoadFunction,
                undefined ,
                function(e:any){
                    console.log("error");
                });
        }

        if(paramter.rotationY !=  undefined){
            // this._scissorLift.rotation.x = -0.5 * Math.PI * (rotationX / 90);
            this._scissorLift.rotation.y = -0.5 * Math.PI * (paramter.rotationY / 90);
        }
        // if(paramter.rotationY != undefined) {
        //     this._scissorLift.rotation.y = -0.5 * Math.PI * (paramter.rotationY / 90);
        // }
        // this._scissorLift.position.set( paramter.x , positionY , paramter.y );
        this._scissorLift.position.set( paramter.x , paramter.z, paramter.y );


        // ===================================================================================================
        // Billboards
        if(paramter.text != undefined){
            const canvas:HTMLCanvasElement | null = this.makeLabelCanvas(100, 25, paramter.text );

            if(canvas == null){
                return;
            }
            const texture:CanvasTexture = new CanvasTexture(canvas);
            // because our canvas is likely not a power of 2
            // in both dimensions set the filtering appropriately.
            texture.minFilter = LinearFilter;
            texture.wrapS = ClampToEdgeWrapping;
            texture.wrapT = ClampToEdgeWrapping;

            const labelMaterial = new SpriteMaterial({
                map: texture,
                transparent: true,
            });
            const labelBaseScale = 0.51;

            const label:Sprite = new Sprite(labelMaterial);
            label.scale.x = canvas.width  * labelBaseScale;
            label.scale.y = canvas.height * labelBaseScale;

            label.position.x = 100;
            label.position.y = 30;
            label.position.z = 150 ;

            this._scissorLift.add(label);
        }


    }

    public getGroup():Group{
        return this._scissorLift;
    }

    // 시저 변경시에 리프트 사다리 변경 및 scaffolding 이동
    public setScissorSize(size:number){
        if(this._xcross != null){
            this._xcross.getGroup().position.y = this._xcrossPositionZ + size / 2;
            this._scaffolding.position.y = this._scaffoldingPositionZ + size - (this._leftSize / 2);

            if(this._workerObject != null){
                this._workerObject.position.y = this._scaffoldingPositionZ + size +this._workerGap - (this._leftSize / 2);
            }
            this._xcross.setZSize(size);
        }
    }



    public liftFrame(width:number ,height:number , depth:number ){
        const carFrameZPosition = (depth / 2) + (depth / 4);
        // 프레임
        let carMainFrame:Box3DObject = new Box3DObject({x:0,y:0,z:(depth / 2),width:width, height:(height / 3) * 2, depth:(depth / 2) , edgesVisible:true,color:0xdbc58e });
        this._scissorLift.add(carMainFrame.getGroup());
        let carLeftFrame:Box3DObject = new Box3DObject({x:0,y:(height /2),z:carFrameZPosition,width:(width / 2), height:(height / 3), depth:depth, edgesVisible:true,color:0xd75a00 });
        this._scissorLift.add(carLeftFrame.getGroup());
        let carRightFrame:Box3DObject = new Box3DObject({x:0,y:-(height /2) ,z:carFrameZPosition,width:(width / 2), height:(height / 3), depth:depth , edgesVisible:true,color:0xd75a00 });
        this._scissorLift.add(carRightFrame.getGroup());

        let radius:number = (depth / 2.5);
        // 바퀴
        const listTire:Array<ICylinder3DObject> = [
            {x:-(width / 2)  ,y:(height /2)   , z:radius , radiusTop:radius,radiusBottom:radius , depth:(height / 3),edgesVisible:false, rotateX:90},
            {x:-(width / 2)  ,y:-(height /2)  , z:radius , radiusTop:radius,radiusBottom:radius , depth:(height / 3),edgesVisible:false, rotateX:270},
            {x:(width / 2)   ,y:(height /2)   , z:radius , radiusTop:radius,radiusBottom:radius , depth:(height / 3),edgesVisible:false, rotateX:90},
            {x:(width / 2)   ,y:-(height /2)  , z:radius , radiusTop:radius,radiusBottom:radius , depth:(height / 3),edgesVisible:false, rotateX:270},
        ];

        const group:Group = this._scissorLift;

        listTire.forEach(function(item:ICylinder3DObject){
            let cylinderGeometry:Tire3DObject = new Tire3DObject(item);
            group.add(cylinderGeometry.getGroup());
        });
    }


    public scaffolding( x:number , y:number , z:number , width:number, height:number , depth:number ){
        let zPosition:number = z;

        let heightDepth:number = (height / 3) * 2;

        let reactFrameBoad:Box3DObject = new Box3DObject({x:x,y:y,z:15, width:width, height:40, depth :5 , edgesVisible:true,color:0xdbc58e });
        this._scaffolding.add(reactFrameBoad.getGroup());

        let reactFrameBoardLine1_1:ReactFrame3DObject = new ReactFrame3DObject(0,19,35,width, 3 ,35, false, 0xd75a00 ,0x000000   , 0);
        this._scaffolding.add(reactFrameBoardLine1_1.getGroup());
        let reactFrameBoardLine1_2:ReactFrame3DObject = new ReactFrame3DObject(0,19,35,heightDepth, 3 ,35, false, 0xd75a00 ,0x000000   , 0);
        this._scaffolding.add(reactFrameBoardLine1_2.getGroup());

        let reactFrameBoardLine2_1:ReactFrame3DObject = new ReactFrame3DObject(0,-19,35,heightDepth, 3 ,35, false, 0xd75a00 ,0x000000   , 0);
        this._scaffolding.add(reactFrameBoardLine2_1.getGroup());
        let reactFrameBoardLine2_2:ReactFrame3DObject = new ReactFrame3DObject(0,-19,35,width, 3 ,35, false, 0xd75a00 ,0x000000   , 0);
        this._scaffolding.add(reactFrameBoardLine2_2.getGroup());

        let reactFrameBoardLine3:ReactFrame3DObject = new ReactFrame3DObject((width /2),0,40,40, 3 ,40, false, 0xd75a00 ,0x000000   , 90 , 90);
        this._scaffolding.add(reactFrameBoardLine3.getGroup());

        let reactFrameBoardLine4:ReactFrame3DObject = new ReactFrame3DObject(-(width /2),0,40,40, 3 ,40, false, 0xd75a00 ,0x000000   , 90 , 90);
        this._scaffolding.add(reactFrameBoardLine4.getGroup());

        this._scaffolding.position.x = x;
        this._scaffolding.position.y = zPosition;
        this._scaffolding.position.z = y;

        this._scissorLift.add(this._scaffolding);
    }


    // https://threejsfundamentals.org/threejs/lessons/threejs-billboards.html 참조
    //캔버르 라벨 만들기
    private makeLabelCanvas(baseWidth:number, size:number, name:string):HTMLCanvasElement | null{
        const ctx:CanvasRenderingContext2D | null = document.createElement('canvas').getContext('2d');
        if(ctx == null){
            return null;
        }

        const fontWidth : number = name.length * 5.2;
        if(baseWidth < fontWidth){
            baseWidth = fontWidth;
        }
        const borderSize:number = 2;
        const font:string =  `${size}px bold sans-serif`;
        const textWidth:number = ctx.measureText(name).width;

        const doubleBorderSize:number = borderSize * 2;
        const width:number = baseWidth + doubleBorderSize;
        const height:number = size + doubleBorderSize;

        ctx.font = font;
        ctx.canvas.width = width;
        ctx.canvas.height = height;

        // need to set font again after resizing canvas
        ctx.font = font;
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';

        // ctx.strokeRect(-10,-10,width,height);
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, width, height);

        ctx.fillStyle = 'black';
        ctx.strokeRect(0, 0, width, height);

        // scale to fit but don't stretch
        const scaleFactor = Math.min(1, baseWidth / textWidth);
        ctx.translate(width / 2, height / 2);
        ctx.scale(scaleFactor, 1);
        ctx.fillStyle = 'black';
        ctx.fillText(name, 0, 0);

        return ctx.canvas;
    }
}
