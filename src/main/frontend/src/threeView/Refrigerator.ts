import Box3DObject from "./Box3DObject";
import {BoxGeometry, Group, Mesh, MeshLambertMaterial, MeshPhongMaterial} from "three";

export default class refrigerator {
    private readonly _frameColor:number = 0xe7e7bb;
    private _axis: Group = new Group();
    private readonly _centerFrameWidth:number = 5;

    constructor(x: number, y: number, z: number, width: number, height: number, depth: number, rotate?: number, doorVisible:boolean = false, doorVisible2:boolean = false) {

        this.addBox(x, depth / 2, z, width, height, depth,0,  doorVisible, doorVisible2);

        if(rotate != undefined){
            this._axis.rotation.y = 1.57 * (rotate / 90);
        }
        this._axis.position.set( x , y, z);
    }
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    private addBox(x: number, y: number, z: number, width: number, height: number, depth: number, rotate?: number, doorVisible:boolean = false, doorVisible2:boolean = false ): void {
        let hFrameGroup: Group = new Group();
        const refBoxer: number = this._centerFrameWidth;


        let backRefGeometry: Box3DObject = new Box3DObject({
            x: width / 2,
            y: 0,
            z: (depth * 3) / 2,
            width: refBoxer,
            height: height,
            depth: (depth * 3),
            edgesVisible: true,
            color: this._frameColor
        });
        hFrameGroup.add(backRefGeometry.getGroup());

        let leftRefGeometry: Box3DObject = new Box3DObject({
            x: 0,
            y: height / 2,
            z: (depth * 3) / 2,
            width: width,
            height: refBoxer,
            depth: (depth * 3),
            edgesVisible: true,
            color: this._frameColor
        });
        hFrameGroup.add(leftRefGeometry.getGroup());

        let rightRefGeometry: Box3DObject = new Box3DObject({
            x: 0,
            y: -height / 2,
            z: (depth * 3) / 2,
            width: width,
            height: refBoxer,
            depth: (depth * 3),
            edgesVisible: true,
            color: this._frameColor
        });
        hFrameGroup.add(rightRefGeometry.getGroup());

        let bottomRefGeometry: Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: 0,
            width: width,
            height: height + refBoxer,
            depth: refBoxer,
            edgesVisible: true,
            color: this._frameColor
        });
        hFrameGroup.add(bottomRefGeometry.getGroup());

        let midRefGeometry: Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: (depth * 3) / 2,
            width: width,
            height: height + refBoxer,
            depth: refBoxer,
            edgesVisible: true,
            color: this._frameColor
        });
        hFrameGroup.add(midRefGeometry.getGroup());

        let topRefGeometry: Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: (depth * 3),
            width: width,
            height: height + refBoxer,
            depth: refBoxer,
            edgesVisible: true,
            color: this._frameColor
        });
        hFrameGroup.add(topRefGeometry.getGroup());

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//
//         let bottomDoorGeometry: Box3DObject = new Box3DObject({
//             x:-width / 2,
//             y:height,
//             z:depth - (refBoxer * 3) ,
//             width:refBoxer,
//             height:height + refBoxer ,
//             depth:(depth * 3) / 2,
//             edgesVisible:true ,
//             color:this._frameColor,
//             rotateY:0
//         });
//         hFrameGroup.add(bottomDoorGeometry.getGroup());
//         -width/2
//         (height - (height * Math.cos(45))+ refBoxer) - height/2
        if (doorVisible == true) {
            let bottomDoorGeometry2: Box3DObject = new Box3DObject({
                x: -(((height * Math.cos(45)) / 2) + (width/ 2) +refBoxer),
                y: -(((height * Math.cos(45)) / 2) + (height/ 2)+(refBoxer * 2)),
                z: depth - (refBoxer ),
                width: refBoxer,
                height: height + refBoxer,
                depth: (depth * 3) / 2,
                edgesVisible: true,
                color: this._frameColor,
                rotateY: 45
            });
            hFrameGroup.add(bottomDoorGeometry2.getGroup());

        } else if (doorVisible == false){
            let bottomDoorGeometry2: Box3DObject = new Box3DObject({
                x: -width / 2,
                y: 0,
                z: depth - (refBoxer * 2),
                width: refBoxer,
                height: height + refBoxer,
                depth: (depth * 3) / 2,
                edgesVisible: true,
                color: this._frameColor,
                rotateY: 0
            });
            hFrameGroup.add(bottomDoorGeometry2.getGroup());
        }
        // :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        if (doorVisible2 == true) {
            let bottomDoorGeometry: Box3DObject = new Box3DObject({
                x: -(((height * Math.cos(45)) / 2) + (width/ 2) +refBoxer),
                y: -(((height * Math.cos(45)) / 2) + (height/ 2)+(refBoxer * 2)),
                z: (depth*3)/2 + depth -(refBoxer * 2) ,
                width: refBoxer,
                height: height,
                depth: (depth * 3) / 2,
                edgesVisible: true,
                color: this._frameColor,
                rotateY: 45
            });
            hFrameGroup.add(bottomDoorGeometry.getGroup());

        } else if (doorVisible2 == false){
            let bottomDoorGeometry: Box3DObject = new Box3DObject({
                x: -width / 2,
                y: 0,
                z: (depth*3)/2 + depth -(refBoxer * 2) ,
                width: refBoxer,
                height: height ,
                depth: (depth * 3) / 2,
                edgesVisible: true,
                color: this._frameColor,
                rotateY: 0
            });
            hFrameGroup.add(bottomDoorGeometry.getGroup());
        }
        // group으로 묶은(hFrameGroup) 위치가 이동
        let zPosition: number = z;
        let yPosition: number = refBoxer;
        let xPosition: number = x;
        hFrameGroup.position.set(xPosition, yPosition, zPosition);
        this._axis.add(hFrameGroup);
    }

    public getGroup():Group{
        return this._axis;
    }
}