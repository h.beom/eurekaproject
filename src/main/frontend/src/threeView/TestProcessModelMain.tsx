import React, {Component} from 'react';
import ReactInterv from 'react-interval';
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {
    AnimationMixer,
    Camera,
    Clock,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Scene,
    TextureLoader,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import * as createjs from "createjs-module";

import Stats from 'stats.js';
import DeskTable from '../model3D/Component/DeskTable';
import HTable from "../model3D/HTable";
import ConfirmTable from "../model3D/Component/ConfirmTable";
import Box3DObject from "../model3D/Box3DObject";
import Lac3DObject from "../model3D/Component/Lac3DObject";
import {Card} from "antd";


class TestProcessModelMain extends Component<any, any> {


    // 시간
    private _clock: Array<Clock> = [];
    // 애니메이션 플레이어
    private _workerAnimationList: Array<AnimationMixer> = [];
    private stats: Stats = new Stats();
    private mixers = [];
    private _mount: HTMLElement | null = null;
    private scene: Scene | null = null;
    private camera: Camera | null = null;
    private renderer: WebGLRenderer | null = null;
    private controls: OrbitControls | null = null;
    private frameId: number = 0;
    private modelItem: Object3D | null = null;


    private _timeline: createjs.Timeline = new createjs.Timeline([], {}, {paused: true});

    constructor(props: any) {
        super(props);
        this.state = {
            count: 0,
        };
        this.start = this.start.bind(this);
        this.animate = this.animate.bind(this);
        this.stop = this.stop.bind(this);
    }


    private getData(scene: Scene) {

        let Cmiddle004: any = {
            positionX: 1000,
            positionY: 30,
            positionZ: 0,
            width: 70,
            height: 70,
            depth: 70,
            column: 2,
            row: 4,
            column_row: 1,
            scene: scene
        };

        this.makeLec(Cmiddle004);
    }

    private makeLec(parameter: any) {
        for (let x = 0; x < parameter.column; x++) {
            let box3DObject: Lac3DObject = new Lac3DObject({
                x: parameter.positionX,
                y: parameter.positionY,
                z: parameter.positionZ + (x * (65 * (parameter.depth / 100))),
                width: parameter.width,
                height: parameter.height,
                depth: parameter.depth,
                edgesVisible: false,
                color: 0xFFFFFF,
                borderColor: 0x04B431
            });

            parameter.scene.add(box3DObject.getGroup());
            for (let y = 0; y < parameter.row; y++) {
                let box3DObject: Lac3DObject = new Lac3DObject({
                    x: parameter.positionX + (y * (65 * (parameter.width / 100))),
                    y: parameter.positionY,
                    z: parameter.positionZ + (x * (65 * (parameter.depth / 100))),
                    width: parameter.width,
                    height: parameter.height,
                    depth: parameter.depth,
                    edgesVisible: false,
                    color: 0xFFFFFF,
                    borderColor: 0xFFFFFF
                });
                parameter.scene.add(box3DObject.getGroup());

                for (let z = 0; z < parameter.column_row; z++) {
                    let box3DObject: Lac3DObject = new Lac3DObject({
                        x: parameter.positionX + (y * (65 * (parameter.width / 100))),
                        y: parameter.positionY + (z * (65 * (parameter.height / 100))),
                        z: parameter.positionZ + (x * (65 * (parameter.depth / 100))),
                        width: parameter.width,
                        height: parameter.height,
                        depth: parameter.depth,
                        edgesVisible: false,
                        color: 0xFFFFFF,
                        borderColor: 0xFFFFFF
                    });
                    parameter.scene.add(box3DObject.getGroup());
                }
            }
        }
    }

    async componentDidMount() {
        if (this._mount === null) {
            return;
        }
        let width: number = this._mount.clientWidth - 28;
        let height: number = this._mount.clientHeight - 10;
        const scene: Scene = new Scene();

        const camera: Camera = new PerspectiveCamera(
            100,
            width / height,
            1,
            10000
        );

        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(0xcccccc, 1);


        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);

        light.position.set(0, 200, 0);

        scene.add(light);


        this.controls = new OrbitControls(camera, renderer.domElement);

        this.controls.enableDamping = true;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 10000;
        this.controls.maxPolarAngle = Math.PI * 2;

        this.getData(scene);

        let gridTile: { width: number, height: number } = {width: 50, height: 100};
        let gridwidth: number = 1200;
        let gridheight: number = 800;

        // let loadTextURL: string = require("./map01.jpg");
        let loader: TextureLoader = new TextureLoader();


        let planeGeometry: PlaneGeometry = new PlaneGeometry(15000, 7000, 150, 70);
        let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0x000000, wireframe: true});
        let grid = new Mesh(planeGeometry, planeMaterial);
        grid.position.set(0, 0, 0);


        grid.rotation.x = -0.5 * Math.PI;
        scene.add(grid);

// ======================================================================================================================================
//         let modelGlbURL: string = '/File/3DModel/48c61cb0-8ebf-4375-8568-7b081eaffb92/9b278f83-5e93-43a1-ade3-d28b88bbbe85' ;

        let modelGlbURL: string = "/dexi-eureka/sim/getSimulationModelFile.do?48c61cb0-8ebf-4375-8568-7b081eaffb92/9b278f83-5e93-43a1-ade3-d28b88bbbe85";
        let modelLoader: GLTFLoader = new GLTFLoader();
        modelLoader.load(modelGlbURL, function (object: GLTF) {
            console.log(object, '');
        });






// ====================================구역나누기===========================================================================

        let desk8: HTable = new HTable({
            x: 2000,
            y: 1650,
            z: 6,
            width: 220,
            height: 120,
            depth: 140,
            color: 0xE3CEF6,
        });
        scene.add(desk8.getGroup());
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


        // 카메라 위치 설정
        camera.position.set(0, 1500, 0);
        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;
        this._mount.appendChild(this.renderer.domElement);

        this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom

        let stateHtml: HTMLElement = this.stats.dom;
        stateHtml.setAttribute("style", "position: absolute; top: 60px; cursor: pointer; opacity: 0.9; z-index: 1000;");
        this._mount.appendChild(stateHtml);

        this.start();

    }

    start(): void {
        this.animate();
    }

    animate = () => {

        this.stats.begin();
        // this.stats.end();
        this.frameId = window.requestAnimationFrame(this.animate);
        //push호 mixers에 담아두었던 것을 반복문을 통해 전체 프레임을 실행시킨다.
        if (this.mixers.length > 0) {
            for (let i = 0; i < this.mixers.length; i++) {
                // @ts-ignore
                //Object for keeping track of time. This uses performance.now if it is available, otherwise it reverts to the less accurate Date.now.
                //시간을 추척하는 용도.
                this.mixers[i].update(this._clock.getDelta());
            }
        }

        //반복문을 돌려서 다음 프레임(1000ms)를 계속 돌린다.
        if (this._workerAnimationList.length > 0) {
            for (let i: number = 0; i < this._workerAnimationList.length; i++) {
                // this.workerAnimationList[i].update(time);
                this._workerAnimationList[i].update(this._clock[i].getDelta());
            }
        }


        this.renderScene();
    };

    componentWillUnmount() {
        if (this.renderer == null || this._mount == null) {
            return;
        }
        this.stop();
        this._mount.removeChild(this.renderer.domElement);
    }

    stop = () => {
        cancelAnimationFrame(this.frameId)
    };


    public change = (time: number) => {
        this._timeline.setPosition(time);

        this.setState({})

    };

    renderScene() {
        if (this.renderer == null || this.scene == null || this.camera == null) {
            return;
        }

        if (this.controls != null) {
            this.controls.update();
        }
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return (
            <Card style={{margin: 8}}>
                <h4>Simulation Monitor</h4>
                <div style={{
                    width: "700", height: "400px", margin: "auto"
                }}

                     ref={(mount) => {
                         if (mount === null) {
                             return;
                         }
                         this._mount = mount
                     }}

                />
            </Card>
        )
    }
}

export default TestProcessModelMain;