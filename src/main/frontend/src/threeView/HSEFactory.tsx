import React, {Component, RefObject} from 'react';
import {Slider} from "antd";
import HSEProcessModel from "./HSEProcessModel";
import {inject, observer} from "mobx-react";
import ReactInterval from 'react-interval';
import autobind from "autobind-decorator";
import {SliderValue} from "antd/lib/slider";
import StringUtile from "../utile/StringUtile";
import {
    BtnTimeLine,
    HSEControl,
    SliderLayout,
    TimeControlBtnLayout,
    TImeLineLayout,
    TimeText
} from "../admin/simulation/store/SimStyle_Componet";


/**
 * ```
 * simulation에대한 공정모델(3d)을 표현하는 class입니다.
 * ```
 * mobx [[resultViewStore]]을 상속한 클래스 (ProcessModel.tsx)
 */
@inject('resultViewStore','processModelStore')
@observer
class HSEFactory extends Component<any, any> {

    // 전체 재생카운트(시간) 에대한변수.
    private _timeMaxCount:number = 0;

    private _threejsConvas:RefObject<HSEProcessModel>= React.createRef();

    public async componentDidMount(){
        const {processModelStore} = this.props;
        processModelStore.setResetProcess();
        // const service:SimulationTapsService = new SimulationTapsService();
        // 전체 값을 가져오지 못해서 임시로 5000초
        // const data:any = await service.getFactoryModel('59d53a57-a0a5-4cb2-a4a9-8dc66618fb53');
        // this._timeMaxCount = data.전체시간;
        this._timeMaxCount = 400;

    }


    /**
     * @description ReactInterval에서 전달받은후 후처리 이벤트
     * @param nextCount setInterval에서 전달받은 count +1 이 된 수
     * @param iconType 재생 상태에따라 표시될 아이콘
     */
    @autobind
    private setTimeLineCount( nextCount:number , iconType ?: "caret-right" | "pause" ){
        const {processModelStore} = this.props;

        //조건 미충족시 그냥 반환
        if( !(nextCount >=0 && nextCount <= this._timeMaxCount) ){
            return;
        }

        processModelStore.setTimeValue(nextCount);

        // 화면값이 null이 아니라면 변경되는 화면에 +1된 nextCount값 주입
        if(this._threejsConvas.current != null){
            // DOM 직접접근 HSEProcessModel의 change()를 이용할 것.
            this._threejsConvas.current.change( nextCount );
        }

        //재생시간 다채우면 pause , 못채웠으면 계속 재생
        if( nextCount >= this._timeMaxCount || iconType == "pause" ){
            // if(this._threejsConvas.current != null){
            //     this._threejsConvas.current.personStop( 'pause' );
            // }
            processModelStore.setEndTimeStart(nextCount, 'pause', false);
        }else{
            processModelStore.setCount(nextCount);
        }
    }

    /**
     * @description 간격시간 설정 reactInterval에서 callback함수로 사용되고, +1씩(100/1 0.1초)추가되어 콜백되어진다.
     * 추가되어진 값은 setTimeLineCount에 넘겨 처리한다.
     */
    @autobind
    private setInterval(){
        const {processModelStore} = this.props;
        this.setTimeLineCount( processModelStore.getCount + 1);
    }


    /**
     * @description siderBar에서 위치 이동시 count(시간)을 재설정해준다.
     * @aprame value 변경된 시간값
     */
    @autobind
    private onTimeChange (value: SliderValue){
        const nextCount: number = parseInt(value.toString());
        this.setTimeLineCount(nextCount , 'pause');
    }

    /**
     * @description 현 재생속도를 받아 되감기(-1 감소)하는 메서드
     */
    @autobind
    private _btnBackWardClick(){
        const {processModelStore} = this.props;
        //현 재생속도
        const speed:number= processModelStore.getSpeedValue;
        //재생이 되고있는 조건하에 적용
        if(speed > 1){
            processModelStore.setSpeedValue(speed - 1);
        }
    }

    /**
     * @description ■ 버튼, count 수를 0으로 잡고 중지시키는 메서드
     */
    @autobind
    private _btnStopClick() {
        this.setTimeLineCount(0 , 'pause');
    }


    /**
     * @description 재생버튼 클릭시 아이콘 타입변경 및 재생 enable 변경
     */
    @autobind
    private _btnPlayClick(){
        const {processModelStore} = this.props;
        if(this._threejsConvas.current != null){
        this._threejsConvas.current.personStop( processModelStore.getPalyIcon );
        }
        let iconType:"caret-right" | "pause" = "caret-right";
        let timerPalyEnable:boolean = true;
        if(processModelStore.getPalyIcon == "caret-right"){
            iconType = "pause";
            timerPalyEnable = false;
        }
        processModelStore.setPalyIcon(iconType);
        processModelStore.setTimerPlayEnable(timerPalyEnable);
    }

    /**
     * @description 빨리감기 메서드
     */
    @autobind
    private _btnForwardClick(){
        const {processModelStore} = this.props;
        //현 재생속도를 가져온후
        const speed:number= processModelStore.getSpeedValue;
        //최고속도 10밑이면 조건 발동
        if(speed < 10){
            processModelStore.setSpeedValue(speed + 1);
        }
    }

    render() {
        const {processModelStore} = this.props;
        return (
            <div style={{margin:"50px 0 50px 50px", height: "700px"}}>
                <h2>HSE Factory</h2>
                <HSEProcessModel ref={this._threejsConvas} />
                <HSEControl>
                    {/*리액트인터벌  시간설정 (1000ms) // 조건이 충족되면 중지, 충족못하면 계속. // callback함수로 +1씩 set해준다. */}
                    <ReactInterval timeout={100 / processModelStore.getSpeedValue } enabled={processModelStore.getTimerPlayEnable} callback={()=>this.setInterval()} />
                    <TImeLineLayout>
                        {/*frame에 현재 재생시간을 출력하는 Text구문*/}
                        <TimeText>{StringUtile.setTimerhhmmss(processModelStore.getTimeValue / 10)}</TimeText>
                        <SliderLayout>
                            {/* 재생 bar 를 나타낸다.
                                default는 0, value는 count되는 값, onchange는 해당 bar위치 눌를때 시간이동, max는 전체재생시간, tooltipVisible은 현시간을 말풍선으로 나타내주는 기능*/}
                            <Slider style={{marginTop:"5px"}} defaultValue={processModelStore.getTimeValue} value={processModelStore.getTimeValue} onChange={this.onTimeChange} tooltipVisible={false} max={this._timeMaxCount} />
                        </SliderLayout>
                        {/*전체시간 출력 (hhmmss로 시간계onTimeChange산) */}
                        <TimeText>{StringUtile.setTimerhhmmss( this._timeMaxCount  / 10) }</TimeText>
                    </TImeLineLayout>
                    <TimeControlBtnLayout>
                        {/*뒤로감기*/}
                        <BtnTimeLine icon={"backward"} onClick={() => this._btnBackWardClick()} />
                        {/*중지*/}
                        <BtnTimeLine onClick={() => this._btnStopClick()} >■</BtnTimeLine>
                        {/*플레이*/}
                        <BtnTimeLine icon={processModelStore.getPalyIcon}  onClick={ () => this._btnPlayClick() } />
                        {/*빨리감기*/}
                        <BtnTimeLine icon={"forward"}  onClick={() => this._btnForwardClick()} />
                        {/*현재 재생속도 출력*/}
                        <TimeText>x {processModelStore.getSpeedValue}</TimeText>
                    </TimeControlBtnLayout>
                </HSEControl>
            </div>
        );
    }
}

export default HSEFactory;