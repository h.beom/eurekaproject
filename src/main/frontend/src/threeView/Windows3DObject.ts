import {Group} from 'three';
import Box3DObject from "./Box3DObject";

export default class Windows3DObject{

    private _windowGroup:Group = new Group();

    constructor( x:number, y:number, z:number, width:number , height:number = 2.5 , depth:number, edgesVisible:boolean = false, color:number = 0x000000 , borderColor:number = 0x000000, rotate?: number , opacity?:number ){
        let frameWidth:number =height;
        this.addWindowFrame(width , depth, frameWidth );
        this.addSashFrame(width , depth , frameWidth);
        const positionY:number = z + (depth / 2) - height;
        this._windowGroup.position.set( x , positionY , y );
    }

    public getGroup():Group{
        return this._windowGroup;
    }

    private addWindowFrame(wWidth:number, wDepth:number , frameWidth:number):void{
        // 1. 창문 상단
        let windowsFrame:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:(frameWidth / 2),
            width:wWidth,
            height:frameWidth,
            depth: frameWidth,
            edgesVisible:true,
            color:0x646464
        });
        this._windowGroup.add(windowsFrame.getGroup());

        let windowsFrameLeftX:number = -(wWidth / 2) + (frameWidth / 2);
        let windowsFrameLeftY:number = 0;
        let windowsFrameLeftZ:number = -(wDepth / 2) + frameWidth;
        // 2. 창문 좌측
        let windowsFrameLeft:Box3DObject = new Box3DObject({
            x:windowsFrameLeftX,
            y:windowsFrameLeftY,
            z:windowsFrameLeftZ,
            width:frameWidth,
            height:frameWidth,
            depth: (wDepth - (frameWidth * 2)),
            edgesVisible:true,
            color:0x646464
        });
        this._windowGroup.add(windowsFrameLeft.getGroup());

        let windowsFrameRightX:number = (wWidth / 2) - (frameWidth / 2);
        let windowsFrameRightY:number = 0;
        let windowsFrameRightZ:number = -(wDepth / 2) + frameWidth;

        // 2. 창문 우측
        let windowsFrameRight:Box3DObject = new Box3DObject({
            x:windowsFrameRightX,
            y:windowsFrameRightY,
            z:windowsFrameRightZ,
            width:frameWidth,
            height:frameWidth,
            depth: (wDepth - (frameWidth * 2)),
            edgesVisible:true,
            color:0x646464
        });
        this._windowGroup.add(windowsFrameRight.getGroup());

        let windowsFrameButtomX:number = 0;
        let windowsFrameButtomY:number = 0;
        let windowsFrameButtomZ:number = -(wDepth )+ frameWidth + (frameWidth /2);
        // 4. 창문 하단
        let windowsFrameButtom:Box3DObject = new Box3DObject({
            x:windowsFrameButtomX,
            y:windowsFrameButtomY,
            z:windowsFrameButtomZ,
            width:wWidth,
            height:frameWidth,
            depth: frameWidth,
            edgesVisible:true,
            color:0x646464
        });
        this._windowGroup.add(windowsFrameButtom.getGroup());
    }


    private addSashFrame(wWidth:number, wDepth:number , frameWidth:number):void{
        let sashWidth:number = 2;
        let sashHeight:number = 1;

        let windowsSashTopZ:number =  -(sashHeight / 2) ;
        // 1. 창문 샤시 상단
        let windowsSashTop:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:windowsSashTopZ,
            width:(wWidth - (frameWidth * 2)),
            height:sashWidth,
            depth: sashHeight,
            edgesVisible:true,
            color:0x646464
        });
        this._windowGroup.add(windowsSashTop.getGroup());

        // 2. 창문 샤시 우측
        let windowsSashRightX:number = (wWidth / 2) - frameWidth - (sashHeight / 2);
        let windowsSashRightY:number = 0;
        let windowsSashRightZ:number = -( (wDepth - (frameWidth * 2)) / 2) ;
        let windowsSashRight:Box3DObject = new Box3DObject({
            x:windowsSashRightX,
            y:windowsSashRightY,
            z:windowsSashRightZ,
            width:sashHeight,
            height:sashWidth,
            depth: (wDepth - (frameWidth * 2) - (sashHeight * 2) ),
            edgesVisible:true,
            color:0xFFFFFF
        });
//        windowsSashRightX , windowsSashRightY , windowsSashRightZ, sashHeight, sashWidth, (wDepth - (frameWidth * 2) - (sashHeight * 2) ), true , 0xFFFFFF , 0x000000);
        this._windowGroup.add(windowsSashRight.getGroup());

        let windowsSashCenterZ:number = -( (wDepth - (frameWidth * 2)) / 2) ;

        // // // 2.5. 창문 샤시 중앙
        let windowsSashCenter:Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: windowsSashCenterZ,
            width: sashWidth,
            height: sashWidth,
            depth: (wDepth - (frameWidth * 2) - (sashHeight * 2) ),
            edgesVisible: true,
            color: 0xFFFFFF
        });
        this._windowGroup.add(windowsSashCenter.getGroup());
        // //
        let windowsSashLeftX:number = -(wWidth / 2) + frameWidth + (sashHeight / 2);
        let windowsSashLeftY:number = 0;
        let windowsSashLeftZ:number = -( (wDepth - (frameWidth * 2)) / 2);
        // // 3. 창문 샤시 좌측
        let windowsSashLeft:Box3DObject = new Box3DObject({
            x: windowsSashLeftX,
            y: windowsSashLeftY,
            z: windowsSashLeftZ,
            width: sashHeight,
            height: sashWidth,
            depth: (wDepth - (frameWidth * 2) - (sashHeight * 2) ),
            edgesVisible: true,
            color: 0xFFFFFF
        });
        this._windowGroup.add(windowsSashLeft.getGroup());

        let windowsSashBottomX:number = 0;
        let windowsSashBottomY:number = 0;
        let windowsSashBottomZ:number = (frameWidth*2)-(wDepth) + (sashHeight / 2);
        // 4. 창문 샤시 하단
        let windowsSashButtom:Box3DObject = new Box3DObject({
            x: windowsSashBottomX,
            y: windowsSashBottomY,
            z: windowsSashBottomZ,
            width: (wWidth - (frameWidth * 2)),
            height: sashWidth,
            depth:sashHeight,
            edgesVisible: true,
            color: 0xFFFFFF
        });
        this._windowGroup.add(windowsSashButtom.getGroup());

        let windowGlassZ:number = - ( (wDepth - (frameWidth * 2)) / 2 );
        let windowGlass:Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: windowGlassZ,
            width: (wWidth - sashWidth - (frameWidth *2) ),
            height: 1,
            depth:(wDepth - (frameWidth * 2)),
            edgesVisible: false,
            color: 0x4d5a87,
            opacity:0.5
        });
        this._windowGroup.add(windowGlass.getGroup());
    }
}
