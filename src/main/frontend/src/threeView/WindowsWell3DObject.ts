import {Group} from 'three';
import Box3DObject from "./Box3DObject";
import Windows3DObject from "./Windows3DObject";

export default class WindowsWell3DObject{

    private _windowWellGroup:Group = new Group();

    constructor( x:number, y:number, z:number, wellWidth:number , wellHeight:number, wellDepth:number , windowsInfo?:{ windowWidth:number, windowX?:number } , rotateY ?:number){
        let wellLeftWidth:number = wellWidth;

        if(windowsInfo != undefined){
            if(windowsInfo.windowX != undefined){
                wellLeftWidth = windowsInfo.windowX;
            }else{
                wellLeftWidth = (wellWidth - windowsInfo.windowWidth ) / 2;
            }
        }
        let wellDepthDivisionThree:number = wellDepth / 3;
        // 1.well 하단
        let wellTop:Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: wellDepthDivisionThree,
            width: wellWidth,
            height: wellHeight,
            depth: wellDepthDivisionThree,
            edgesVisible: true,
            color: 0xFFFFFF
        });
        //0,  0 , wellDepthDivisionThree, wellWidth, wellHeight, wellDepthDivisionThree , true , 0xFFFFFF , 0x000000);
        this._windowWellGroup.add(wellTop.getGroup());

        let wellLeftX:number = -(wellWidth / 2 -(wellLeftWidth/2) );
        let wellLeftY:number = 0;
        let wellLeftZ:number = 0;
        // 2.well 좌측
        let wellLeft:Box3DObject = new Box3DObject({
            x: wellLeftX,
            y: wellLeftY,
            z: wellLeftZ,
            width: wellLeftWidth,
            height: wellHeight,
            depth: wellDepthDivisionThree,
            edgesVisible: true,
            color: 0xb32323
        });
        // wellLeftX , wellLeftY , wellLeftZ , wellLeftWidth, wellHeight, wellDepthDivisionThree, true , 0xb32323 , 0x000000);
        this._windowWellGroup.add(wellLeft.getGroup());


        if(windowsInfo != undefined){

            let windowPositionX:number = -(wellWidth / 2) + (wellLeftWidth) + (windowsInfo.windowWidth / 2);
            windowPositionX = windowPositionX;

            let windowPositionY:number = 0;
            let windowPositionZ:number = 0;
            // windows
            let window:Windows3DObject = new Windows3DObject(windowPositionX ,windowPositionY ,windowPositionZ, windowsInfo.windowWidth ,wellHeight ,wellDepthDivisionThree );
            this._windowWellGroup.add(window.getGroup());

            let wellRightWidth:number = wellWidth - wellLeftWidth - windowsInfo.windowWidth;
            let wellRightPositionX:number = windowPositionX +(windowsInfo.windowWidth / 2) + (wellRightWidth / 2);
            // 3.well 우측
            let wellRight:Box3DObject = new Box3DObject({
                x: wellRightPositionX,
                y: 0,
                z: 0,
                width: wellRightWidth,
                height: wellHeight,
                depth: wellDepthDivisionThree,
                edgesVisible: true,
                color: 0xb32323
            });
            this._windowWellGroup.add(wellRight.getGroup());
        }

        // 4. well 상단
        let wellButton:Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: -(wellDepthDivisionThree),
            width: wellWidth,
            height: wellHeight,
            depth: wellDepthDivisionThree,
            edgesVisible: true,
            color: 0xFFFFFF
        });
        //0, 0, -(wellDepthDivisionThree), wellWidth, wellHeight, wellDepthDivisionThree, true , 0xFFFFFF , 0x000000);
        this._windowWellGroup.add(wellButton.getGroup());

        if(rotateY != undefined){
            this._windowWellGroup.rotateY( -0.5 * Math.PI * ( rotateY / 90))
        }

        const positionY:number = z ;//+ (wellDepth / 2);
        this._windowWellGroup.position.set( x , positionY , y );
    }

    public getGroup():Group{
        return this._windowWellGroup;
    }
}
