//import Collator = Intl.Collator;
import axios, {AxiosRequestConfig} from "axios";


/**
 * ```
 * UserCode에 관련하여 수정, 추가, 삭제, 조회를 하는 class입니다.
 * ```
 *
 * axios(http 클라이언트 라이브러리--typeScript지원) (Searvice.ts)
 */
export default class Searvice{
    /**
     * POST 처리 메소드입니다.
     * @param url 데이터를 요청할 통신주소
     * @param parameter 데이터 요청시 필요한 파라미터
     */
    public static getPost(url:string , parameter: any | null ):any{
        const ajaxconfig:AxiosRequestConfig = {
            headers: {
                'content-Type': 'application/json;charset=UTF-8'
            }
        };
        if( parameter == null){
            return axios.post("/dexi-eureka/"+url ,undefined ,ajaxconfig)
        }else{
            return axios.post("/dexi-eureka/"+url ,JSON.stringify(parameter) ,ajaxconfig)
        }
    }

    /**
     * Get 처리 메소드입니다.
     * @param url 데이터를 요청할 통신주소
     * @param parameter 데이터 요청시 필요한 파라미터
     */
    public static get(url:string , parameter: any | null ):any{
        const ajaxconfig:AxiosRequestConfig = {
            headers: {
                'content-Type': 'application/json;charset=UTF-8'
            }
        };

        return axios.get("/dexi-eureka/"+url ,ajaxconfig)

    }

    /**
     * 엑셀파일형식으로 데이터를 요청하기 위한 Method입니다.
     * @param url 데이터를 요청할 통신주소
     * @param parameter 엑셀 파일에 필요한 데이터 요청시 필요한 파라미터
     */
    public static getFileDownload(url:string , parameter: any | null ):any{
        let urlStr:string = '/dexi-eureka/'+url;
        let paramterStr:string = "";
        if(parameter != null){
            for(let key in parameter){
                paramterStr += `&${key}=${parameter[key]}`;
            }
            paramterStr = paramterStr.substring(1,paramterStr.length);
            paramterStr = "?"+paramterStr;
        }
        window.location.assign(urlStr + paramterStr);
    }
}