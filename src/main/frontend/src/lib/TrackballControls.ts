/**
 * @author Eberhard Graether / http://egraether.com/
 */
import {
    Quaternion,
    Vector3,
    Vector2,
} from 'three';


const enum STATE{
    NONE=-1,
    ROTATE=0,
    ZOOM=1,
    PAN=2,
    TOUCH_ROTATE=3,
    TOUCH_ZOOM=4,
    TOUCH_PAN=5
};
export interface ScreenOption{ width: number, height:number, offsetLeft:number, offsetTop:number };

export default class TrackballControls{

    private _object:any;
    private _domElement:HTMLElement;
    private _enabled:Boolean        = true;
    private _screen:ScreenOption    = { width: 0, height: 0, offsetLeft: 0, offsetTop: 0 };
	private _radius:number          = ( this._screen.width + this._screen.height ) / 4;

	private _rotateSpeed:number = 1.0;
	private _zoomSpeed:number = 1.2;
	private _panSpeed:number = 0.3;
    private _noRotate:boolean = false;
    private _noZoom:boolean = false;
    private _noPan:boolean = false;
    
    private _staticMoving:boolean = false;
    private _dynamicDampingFactor:number = 0.2;
    private _minDistance:number = 0;
    private _maxDistance:number = Infinity;
    
    private _keys:Array<number>=[ 65 /*A*/, 83 /*S*/, 68 /*D*/ ];
    private target:Vector3 = new Vector3();

	private lastPosition:Vector3 = new Vector3();

	private _state:STATE = STATE.NONE;
	private _prevState:STATE = STATE.NONE;

	private _eye:Vector3 = new Vector3();

	private _rotateStart:Vector3 = new Vector3();
	private _rotateEnd:Vector3 = new Vector3();

	private _zoomStart:Vector2 = new Vector2();
	private _zoomEnd:Vector2 = new Vector2();

	private _touchZoomDistanceStart:number = 0;
	private _touchZoomDistanceEnd:number = 0;

    private _panStart:Vector2 = new Vector2();
	private _panEnd:Vector2 = new Vector2();

    // for reset
    private _target0:Vector3;
    private _position0:any;
    private _up0:any;

	// events

	private _changeEvent:{ type: 'change' } = { type: 'change' };


    constructor( object:any, domElement:HTMLElement){
        this._object = object;
        this._domElement = domElement;
        this._target0 = this.target.clone();
        this._position0 = this._object.position.clone();
        this._up0 = this._object.up.clone();
    }


    public handleResize():void {

		this._screen.width = window.innerWidth;
		this._screen.height = window.innerHeight;

		this._screen.offsetLeft = 0;
		this._screen.offsetTop = 0;

		this._radius = ( this._screen.width + this._screen.height ) / 4;
	};

	// public handleEvent( event:Function ) {

	// 	if ( typeof this[ event.type ] == 'function' ) {

	// 		this[ event.type ]( event );

	// 	}

	// };

	// this.getMouseOnScreen = function ( clientX, clientY ) {

	// 	return new THREE.Vector2(
	// 		( clientX - _this.screen.offsetLeft ) / _this.radius * 0.5,
	// 		( clientY - _this.screen.offsetTop ) / _this.radius * 0.5
	// 	);

	// };

	// this.getMouseProjectionOnBall = function ( clientX, clientY ) {

	// 	var mouseOnBall = new THREE.Vector3(
	// 		( clientX - _this.screen.width * 0.5 - _this.screen.offsetLeft ) / _this.radius,
	// 		( _this.screen.height * 0.5 + _this.screen.offsetTop - clientY ) / _this.radius,
	// 		0.0
	// 	);

	// 	var length = mouseOnBall.length();

	// 	if ( length > 1.0 ) {

	// 		mouseOnBall.normalize();

	// 	} else {

	// 		mouseOnBall.z = Math.sqrt( 1.0 - length * length );

	// 	}

	// 	_eye.copy( _this.object.position ).sub( _this.target );

	// 	var projection = _this.object.up.clone().setLength( mouseOnBall.y );
	// 	projection.add( _this.object.up.clone().cross( _eye ).setLength( mouseOnBall.x ) );
	// 	projection.add( _eye.setLength( mouseOnBall.z ) );

	// 	return projection;

	// };

	// this.rotateCamera = function () {

	// 	var angle = Math.acos( _rotateStart.dot( _rotateEnd ) / _rotateStart.length() / _rotateEnd.length() );

	// 	if ( angle ) {

	// 		var axis = ( new THREE.Vector3() ).crossVectors( _rotateStart, _rotateEnd ).normalize();
	// 			quaternion = new THREE.Quaternion();

	// 		angle *= _this.rotateSpeed;

	// 		quaternion.setFromAxisAngle( axis, -angle );

	// 		_eye.applyQuaternion( quaternion );
	// 		_this.object.up.applyQuaternion( quaternion );

	// 		_rotateEnd.applyQuaternion( quaternion );

	// 		if ( _this.staticMoving ) {

	// 			_rotateStart.copy( _rotateEnd );

	// 		} else {

	// 			quaternion.setFromAxisAngle( axis, angle * ( _this.dynamicDampingFactor - 1.0 ) );
	// 			_rotateStart.applyQuaternion( quaternion );

	// 		}

	// 	}

	// };

	// this.zoomCamera = function () {

	// 	if ( _state === STATE.TOUCH_ZOOM ) {

	// 		var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
	// 		_touchZoomDistanceStart = _touchZoomDistanceEnd;
	// 		_eye.multiplyScalar( factor );

	// 	} else {

	// 		var factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

	// 		if ( factor !== 1.0 && factor > 0.0 ) {

	// 			_eye.multiplyScalar( factor );

	// 			if ( _this.staticMoving ) {

	// 				_zoomStart.copy( _zoomEnd );

	// 			} else {

	// 				_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

	// 			}

	// 		}

	// 	}

	// };

	// this.panCamera = function () {

	// 	var mouseChange = _panEnd.clone().sub( _panStart );

	// 	if ( mouseChange.lengthSq() ) {

	// 		mouseChange.multiplyScalar( _eye.length() * _this.panSpeed );

	// 		var pan = _eye.clone().cross( _this.object.up ).setLength( mouseChange.x );
	// 		pan.add( _this.object.up.clone().setLength( mouseChange.y ) );

	// 		_this.object.position.add( pan );
	// 		_this.target.add( pan );

	// 		if ( _this.staticMoving ) {

	// 			_panStart = _panEnd;

	// 		} else {

	// 			_panStart.add( mouseChange.subVectors( _panEnd, _panStart ).multiplyScalar( _this.dynamicDampingFactor ) );

	// 		}

	// 	}

	// };

	// this.checkDistances = function () {

	// 	if ( !_this.noZoom || !_this.noPan ) {

	// 		if ( _this.object.position.lengthSq() > _this.maxDistance * _this.maxDistance ) {

	// 			_this.object.position.setLength( _this.maxDistance );

	// 		}

	// 		if ( _eye.lengthSq() < _this.minDistance * _this.minDistance ) {

	// 			_this.object.position.addVectors( _this.target, _eye.setLength( _this.minDistance ) );

	// 		}

	// 	}

	// };

	// this.update = function () {

	// 	_eye.subVectors( _this.object.position, _this.target );

	// 	if ( !_this.noRotate ) {

	// 		_this.rotateCamera();

	// 	}

	// 	if ( !_this.noZoom ) {

	// 		_this.zoomCamera();

	// 	}

	// 	if ( !_this.noPan ) {

	// 		_this.panCamera();

	// 	}

	// 	_this.object.position.addVectors( _this.target, _eye );

	// 	_this.checkDistances();

	// 	_this.object.lookAt( _this.target );

	// 	if ( lastPosition.distanceToSquared( _this.object.position ) > 0 ) {

	// 		_this.dispatchEvent( changeEvent );

	// 		lastPosition.copy( _this.object.position );

	// 	}

	// };

	// this.reset = function () {

	// 	_state = STATE.NONE;
	// 	_prevState = STATE.NONE;

	// 	_this.target.copy( _this.target0 );
	// 	_this.object.position.copy( _this.position0 );
	// 	_this.object.up.copy( _this.up0 );

	// 	_eye.subVectors( _this.object.position, _this.target );

	// 	_this.object.lookAt( _this.target );

	// 	_this.dispatchEvent( changeEvent );

	// 	lastPosition.copy( _this.object.position );

	// };



}