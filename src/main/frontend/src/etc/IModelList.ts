import styled from "styled-components";
import {Button, Card, Input, PageHeader} from "antd";

/**
 *
 */
export interface IModelViewProps {
    positionInfo: IModelListDataSource,
}


export interface IModelInfoPopup {
    visible: boolean,
    modalOKClickEvent: Function,
    modelInfo: IModelListDataSource,
}

export interface IModelListDataSource {
    stenc3dInfoId: string,
    stenc3dId: string,
    stenc3dTp: string,
    stenc3dFileNm: string,
    insuser: string,
    modelScale: number,
    isacty: number
}

export enum ModelActiveType {
    ALL = "ALL",
    ACTIVE = "1",
    NOACTIVE = "0"
}

export interface IModelListHeader {
    onSearchEvent: Function
}

export interface IModelListParame {
    search: string,
    modeltype: ModelActiveType,
    loclid: string,
}


// =======================================================================================================
// =======================================================================================================
// =======================================================================================================
// =======================================================================================================
// 스타일 컴포넌트

export const TextSpan = styled.span` 
line-height: 42px;
{margin-left: 30px;
`;

export const InputHeader = styled(Input)`
    width:32%;
    margin-top:12px;
     margin-left:'10px'
`;

export const ButtonBind = styled.span`
             margin-top:12px;
             float: right;
        `;

export const SearchButton = styled(Button)`
        width: 150px;
`;
export const ResetButton = styled(SearchButton)`
        width: 150px;
        margin-left:30px;
`;

export const Header = styled(PageHeader)`
             gridArea: ant-page-header;
             min-width: 1440px;
             margin: 20px;
             background-color:white;
        `;

export const Comp = styled.div`
    margin : 20px;
`;

export const CardComponent = styled(Card)`
     width: 100% ;
     height:calc(350px - 47px)
`;
