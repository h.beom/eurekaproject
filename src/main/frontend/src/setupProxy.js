const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    // 운영 설정
    // app.use(proxy('/dexi-eureka/', { target: 'http://cloud.dexi.kr/' }));
    // 개발 설정
    app.use(proxy('/dexi-eureka/', { target: 'http://127.0.0.1:9090/' }));
    // app.use(proxy('/dexi-eureka/', { target: 'http://http://192.168.0.200:34800/' , changeOrigin: true }));
    // app.use(proxy('/File/', { target: 'http://115.145.177.34:3486/' , changeOrigin: true }));
};
