import React, {Component, Fragment} from 'react';
import {inject, observer} from "mobx-react";
import Media from "react-media";
import DropMenu from "./DropMenu";
import DropMenu2 from "./DropMenu2";

@inject('IndexStore')
@observer
class DropTopMenu extends Component<any, any> {



    render() {

        return (
            <Fragment>


                <Media queries={{
                    small: "(max-width: 650px)",
                    small1: "(min-width: 651px) and (max-width: 767px)",
                    medium: "(min-width: 768px) and (max-width: 1140px)",
                    large: "(min-width: 1140px)"
                }}>
                    {matches => (
                        <Fragment>
                            {matches.small &&<DropMenu/>}
                            {matches.small1 && <DropMenu2/>}
                            {matches.medium && null}
                            {matches.large && null}
                        </Fragment>
                    )}
                </Media>
            </Fragment>
        );
    };
}


export default DropTopMenu;
