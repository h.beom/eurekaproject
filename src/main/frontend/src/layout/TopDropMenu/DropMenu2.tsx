import React, {Component, Fragment} from 'react';
import {inject, observer} from "mobx-react";
import {BrowserRouter, Link} from "react-router-dom";
import {Icon} from "semantic-ui-react";

@inject('IndexStore')
@observer
class DropMenu2 extends Component<any, any> {


    render() {

        return (
            <Fragment>
                <div className="ui menu">

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"tachometer alternate"}/>
                        Dash
                        <Link to={"/amchart"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">DashBoard</div>
                            </div>
                        </Link>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"info circle"}/>
                        Info
                        <Link to={"/InformationPlant"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">Plant Information</div>
                            </div>
                        </Link>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"desktop"}/>
                        Monitor
                        <Link to={"/Monitoring"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">Monitoring</div>
                            </div>
                        </Link>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"chart bar"}/>
                        Simul
                        <div className="menu" id={"dropdown-content"}>
                            <Link to={"/simulation"}>
                                <a className="item">Simulation</a>
                            </Link>
                        </div>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"search"}/>
                        Analysis
                        <Icon className={"#"}/>
                        <div className="menu" id={"dropdown-content"}>
                            <div className="item">Analysis</div>
                        </div>
                    </div>


                <div className="ui pointing dropdown link item" id={"dropdown"}>
                    <Icon className={"sliders horizontal"}/>
                    Control
                    <Link to={"/Control"}>
                        <div className="menu" id={"dropdown-content"}>
                            <div className="item">Control</div>
                        </div>
                    </Link>
                </div>

                </div>
            </Fragment>
        );
    };
}


export default DropMenu2;
