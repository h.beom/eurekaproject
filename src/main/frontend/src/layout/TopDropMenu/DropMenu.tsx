import React, {Component, Fragment} from 'react';
import {inject, observer} from "mobx-react";
import {Link} from "react-router-dom";
import {Icon} from "semantic-ui-react";

@inject('IndexStore')
@observer
class DropMenu extends Component<any, any> {


    render() {

        return (
            <Fragment>
                <div className="ui menu" id={'dMenuTop'}>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"tachometer alternate"}/>
                        <Link to={"/amchart"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">DashBoard</div>
                            </div>
                        </Link>
                    </div>
                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"info circle"}/>
                        <Link to={"/InformationPlant"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">Plant Information</div>
                            </div>
                        </Link>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"desktop"}/>
                        <Link to={"/Monitoring"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">Monitoring</div>
                            </div>
                        </Link>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"chart bar"}/>
                        <div className="menu" id={"dropdown-content"}>
                            <Link to={"/simulation"}>
                                <div className="item">Simulation</div>
                            </Link>
                        </div>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"search"}/>
                        <div className="menu" id={"dropdown-content"}>
                            <div className="item">Analysis</div>
                        </div>
                    </div>

                    <div className="ui pointing dropdown link item" id={"dropdown"}>
                        <Icon className={"sliders horizontal"}/>
                        <Link to={"/Control"}>
                            <div className="menu" id={"dropdown-content"}>
                                <div className="item">Control</div>
                            </div>
                        </Link>
                    </div>
                </div>
            </Fragment>
        );
    };
}


export default DropMenu;
