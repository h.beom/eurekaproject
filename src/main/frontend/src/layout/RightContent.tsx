import React, {Component} from 'react';
import 'antd/dist/antd.css';


class RightContent extends Component<any, any> {

    render() {

        return (
            <div className="col-3 col-s-12">
                <div className="aside">
                    <h2>What?</h2>
                    <p>Chania is a city on the island of Crete.</p>
                    <h2>Where?</h2>
                    <p>Crete is a Greek island in the Mediterranean Sea.</p>
                    <h2>How?</h2>
                    <p>You can reach Chania airport from all over Europe.</p>
                </div>
            </div>
        );
    };
}


export default RightContent;
