import {action, computed, observable} from 'mobx';
import {MenuItemList} from "../typescript/IndexType";
import {Avatar, Layout, Menu, Icon} from 'antd';
import {Link} from 'react-router-dom';


const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;

class IndexStore {
    @observable leftMenuList: Array<MenuItemList> = [
        {
            "id": 1,
            "url": "/",
            "parentid": 0,
            "text": "Monitoring",
            "depth": 1,
            "visible": true,
            "icon": 'dashboard',
            "child": [
                {
                    "id": 99,
                    "url": "/Main",
                    "parentid": 1,
                    "icon": "fa fa-group",
                    "text": "Monitoring",
                    "depth": 2,
                    "visible": true,
                    "child": []
                }
            ]
        },
        {
            "id": 4,
            "url": "/",
            "parentid": 0,
            "text": "Simulation",
            "depth": 1,
            "visible": true,
            "icon": 'laptop',
            "child": [
                {
                    "id": 13,
                    "url": "/Main/Simulation",
                    "parentid": 4,
                    "icon": "fa fa-group",
                    "text": "Simulation List",
                    "depth": 2,
                    "visible": true,
                    "child": []
                }

            ]
        },
        {
            "id": 3,
            "url": "/",
            "parentid": 0,
            "text": "Analysis",
            "depth": 1,
            "visible": true,
            "icon": 'area-chart',
            "child": [
                {
                    "id": 9,
                    "url": "/",
                    "parentid": 3,
                    "icon": "fa fa-group",
                    "text": "Product Constrol",
                    "depth": 2,
                    "visible": true,
                    "child": []
                },
                {
                    "id": 49,
                    "url": "/",
                    "parentid": 3,
                    "icon": "fa fa-group",
                    "text": "Quailty",
                    "depth": 2,
                    "visible": true,
                    "child": []
                }
            ]
        },

        {
            "id": 2,
            "url": "/",
            "parentid": 0,
            "text": "Information",
            "depth": 1,
            "visible": true,
            "icon": 'info',
            "child": [
                {
                    "id": 6,
                    "url": "/",
                    "parentid": 2,
                    "icon": "fa fa-group",
                    "text": "Plant Information",
                    "depth": 2,
                    "visible": true,
                    "child": []
                }
            ]
        }
    ];

    @observable leftKpiMenuList: Array<MenuItemList> = [
        {
            "id": 1,
            "url": "/Information",
            "parentid": 0,
            "text": "Information",
            "depth": 1,
            "visible": true,
            "icon": 'info',
            "child": []
        },
        {
            "id": 2,
            "url": "/Information/Master",
            "parentid": 0,
            "text": "Master",
            "depth": 1,
            "visible": true,
            "icon": 'copy',
            "child": []
        },
        {
            "id": 3,
            "url": "/Information/Record",
            "parentid": 0,
            "text": "Record",
            "depth": 1,
            "visible": true,
            "icon": 'laptop',
            "child": []
        },
    ];

    @computed get getLeftMenuList() {
        return this.leftMenuList
    }

    @computed get getKpiLeftMenuList() {
        return this.leftKpiMenuList
    }

}

export default IndexStore;