import {action, computed, observable} from 'mobx';
import React from "react";
import {KpiMasterData, KpiRecordData, KpiRecordSearch} from "../typescript/KpiType";
import StringUtile from "../../../utile/StringUtile";


class kpiStore {


    // KPI MASTER INSERT하기 위한 값 Getter▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable kpiId: string = '';

    @action setKpiId(param: string) {
        this.kpiId = param;
    }

    @computed get getKpiId() {
        return this.kpiId;
    }

    @observable kpiName: string = '';

    @action setKpiName(param: string) {
        this.kpiName = param;
    }

    @computed get getKpiName() {
        return this.kpiName;
    }

    @observable uiType: string = 'N-Line';

    @action setUiType(param: string) {
        this.uiType = param;
    }

    @computed get getUiType() {
        return this.uiType;
    }

    @observable unit: string = '';

    @action setUnit(param: string) {
        this.unit = param;
    }

    @computed get getUnit() {
        return this.unit;
    }

    @observable description: string = '';

    @action setDesr(param: string) {
        this.description = param;
    }

    @computed get getDesr() {
        return this.description;
    }

    @observable scope: string = '';

    @action setScope(param: string) {
        this.scope = param;
    }

    @computed get getScope() {
        return this.scope;
    }

    @observable formula: string = '';

    @action setFormula(param: string) {
        this.formula = param;
    }

    @computed get getFormula() {
        return this.formula;
    }

    @observable unitOfMeasure: string = '';

    @action setUnitOfM(param: string) {
        this.unitOfMeasure = param;
    }

    @computed get getUnitOfM() {
        return this.unitOfMeasure;
    }

    @observable range: string = '';

    @action setRange(param: string) {
        this.range = param;
    }

    @computed get getRange() {
        return this.range;
    }

    @observable trend: string = '';

    @action setTrend(param: string) {
        this.trend = param;
    }

    @computed get getTrend() {
        return this.trend;
    }

    @observable timing: string = '';

    @action setTiming(param: string) {
        this.timing = param;
    }

    @computed get getTiming() {
        return this.timing;
    }

    @observable audience: string = '';

    @action setAud(param: string) {
        this.audience = param;
    }

    @computed get getAud() {
        return this.audience;
    }

    @observable prodMeth: string = '';

    @action setProdMeth(param: string) {
        this.prodMeth = param;
    }

    @computed get getProdMeth() {
        return this.prodMeth;
    }

    @observable notes: string = '';

    @action setNotes(param: string) {
        this.notes = param;
    }

    @computed get getNotes() {
        return this.notes;
    }

    @observable midCate: string = '';

    @action setMidCate(param: string) {
        this.midCate = param;
    }

    @computed get getMidCate() {
        return this.midCate;
    }

    @observable subCate: string = '';

    @action setSubCate(param: string) {
        this.subCate = param;
    }

    @computed get getSubCate() {
        return this.subCate;
    }

    @observable preference: string = '';

    @action setPrefer(param: string) {
        this.preference = param;
    }

    @computed get getPrefer() {
        return this.preference;
    }

    @observable diagramFile: string = '';

    @action setDiagramFile(param: string) {
        this.diagramFile = param;
    }

    @computed get getDiagramFile() {
        return this.diagramFile;
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description KPI MASTER 의 데이터 Insert 하기위한 파라미터들.
     */
    @computed get getMasterParam() {
        let parameter: KpiMasterData = {
            kpiId: this.kpiId,
            kpiName: this.kpiName,
            uiType: this.uiType,
            unit: this.unit,
            description: this.description,
            scope: this.scope,
            formula: this.formula,
            unitOfMeasure: this.unitOfMeasure,
            range: this.range,
            trend: this.trend,
            timing: this.timing,
            audience: this.audience,
            prodMeth: this.prodMeth,
            notes: this.notes,
            midCate: this.midCate,
            subCate: this.subCate,
            preference: this.preference,
            diagramFile: this.diagramFile,
        }
        return parameter;
    }


    /**
     * @desciption KPI MASTER 전체값
     */
    @observable kpiMaster: Array<KpiMasterData> = [];
    @observable subKpiMaster: Array<KpiMasterData> = [];

    @action setKpiMaster(param: Array<KpiMasterData>) {
        this.kpiMaster = param;
        this.subKpiMaster = param;
    }

    @computed get getKpiMaster() {
        return this.kpiMaster
    }
    @action setSubKpiMaster(param: Array<KpiMasterData>) {
        this.subKpiMaster = param;
    }
    @computed get getSubKpiMaster() {
        return this.subKpiMaster
    }


    /**
     * description pk값이 되는 kpiId
     */
    @observable selectKpi: string = '';

    @action setSelectKpi(param: string) {
        this.selectKpi = param;
    }

    @computed get getSelectKpi() {
        return this.selectKpi
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @action masterUpdate(param: any) {
        this.kpiId = param.kpiId;
        this.kpiName = param.kpiName;
        this.uiType = param.uiType;
        this.unit = param.unit;
        this.description = param.description;
        this.scope = param.scope;
        this.formula = param.formula;
        this.unitOfMeasure = param.unitOfMeasure;
        this.range = param.range;
        this.trend = param.trend;
        this.timing = param.timing;
        this.audience = param.audience;
        this.prodMeth = param.prodMeth;
        this.notes = param.notes;
        this.midCate = param.midCate;
        this.subCate = param.subCate;
        this.preference = param.preference;
        this.diagramFile = param.diagramFile;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @description KPI MASTER input값을 초기화
     */
    @action resetMasterKpi() {
        this.kpiId = '';
        this.kpiName = '';
        this.uiType = 'N-Line';
        this.unit = '';
        this.description = '';
        this.scope = '';
        this.formula = '';
        this.unitOfMeasure = '';
        this.range = '';
        this.trend = '';
        this.timing = '';
        this.audience = '';
        this.prodMeth = '';
        this.notes = '';
        this.midCate = '';
        this.subCate = '';
        this.preference = '';
        this.diagramFile = '';
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// RDCORD▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// RDCORD▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// RDCORD▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable hirerachyName: string = '';

    @action setHirerachyName(param: string) {
        this.hirerachyName = param;
    }

    @computed get getHirerachyName() {
        return this.hirerachyName
    }


    /**
     * @desciption Record 데이터 가져오기 (Depth = 1 의 값만 가지고온것)
     */

    @observable baseData: Array<KpiRecordData> = [];
    @observable kpiRecord: Array<KpiRecordData> = [];

    @computed get getBaseData() {
        return this.baseData
    }

    @action setKpiRecord(param: Array<KpiRecordData>) {
        this.baseData = param;
        this.kpiRecord = param;
    }

    @computed get getKpiRecord() {
        return this.kpiRecord
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable searchFactory: string = '';
    @action setSearchFactory(param:any){
        this.searchFactory = param;
    }
    @computed get getSearchFactory(){
        return this.searchFactory;
    }



// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 1 의 값만 가지고온것)
     */
    @observable kpiSearch: Array<KpiRecordSearch> = [];

    @action setKpiSearch(param: Array<KpiRecordSearch>) {
        this.kpiSearch = param;
    }

    @computed get getKpiSearch() {
        return this.kpiSearch
    }


    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 2 의 값만 가지고온것)
     */
    @observable kpiSearch2: Array<KpiRecordSearch> = [];

    @action setKpiSearch2(param: Array<KpiRecordSearch>) {
        this.kpiSearch2 = param;
    }

    @computed get getKpiSearch2() {
        return this.kpiSearch2
    }

    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 3 의 값만 가지고온것)
     */
    @observable kpiSearch3: Array<KpiRecordSearch> = [];

    @action setKpiSearch3(param: Array<KpiRecordSearch>) {
        this.kpiSearch3 = param;

    }

    @computed get getKpiSearch3() {
        return this.kpiSearch3
    }

    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 3 의 값만 가지고온것)
     */
    @observable kpiSearch4: Array<KpiRecordSearch> = [];

    @action setKpiSearch4(param: Array<KpiRecordSearch>) {
        this.kpiSearch4 = param;

    }

    @computed get getKpiSearch4() {
        return this.kpiSearch4
    }


    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 4 의 값만 가지고온것)
     */
    @observable kpiSearch5: Array<KpiRecordSearch> = [];

    @action setKpiSearch5(param: Array<KpiRecordSearch>) {
        this.kpiSearch5 = param;

    }

    @computed get getKpiSearch5() {
        return this.kpiSearch5
    }



    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 4 의 값만 가지고온것)
     */
    @observable kpiSearch6: Array<KpiRecordSearch> = [];

    @action setKpiSearch6(param: Array<KpiRecordSearch>) {
        this.kpiSearch6 = param;

    }

    @computed get getKpiSearch6() {
        return this.kpiSearch6
    }

    /**
     * @desciption Record 정렬기준 조건데이터 가져오기 (Depth = 4 의 값만 가지고온것)
     */
    @observable kpiSearch7: Array<KpiRecordSearch> = [];

    @action setKpiSearch7(param: Array<KpiRecordSearch>) {
        this.kpiSearch7 = param;

    }

    @computed get getKpiSearch7() {
        return this.kpiSearch7
    }
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @desciption
     */
    @observable relatedId: string = 'all';

    @action setRelatedId(param: string) {
        this.relatedId = param;
    }

    @computed get getRelatedId() {
        return this.relatedId
    }

    /**
     * @desciption
     */
    @observable relatedId2: string = 'all';

    @action setRelatedId2(param: string) {
        this.relatedId2 = param;
    }

    @computed get getRelatedId2() {
        return this.relatedId2
    }

    /**
     * @desciption
     */
    @observable relatedId3: string = 'all';

    @action setRelatedId3(param: string) {
        this.relatedId3 = param;
    }

    @computed get getRelatedId3() {
        return this.relatedId3
    }

    /**
     * @desciption
     */
    @observable relatedId4: string = 'all';

    @action setRelatedId4(param: string) {
        this.relatedId4 = param;
    }

    @computed get getRelatedId4() {
        return this.relatedId4
    }

    /**
     * @desciption
     */
    @observable relatedId5: string = 'all';

    @action setRelatedId5(param: string) {
        this.relatedId5 = param;
    }

    @computed get getRelatedId5() {
        return this.relatedId5
    }

    /**
     * @desciption
     */
    @observable relatedId6: string = 'all';

    @action setRelatedId6(param: string) {
        this.relatedId6 = param;
    }

    @computed get getRelatedId6() {
        return this.relatedId6
    }
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @desciption
     */
    @observable hirerachy: string = '';

    @action setHirerachy(param: string) {
        this.hirerachy = param;
    }

    @computed get getHirerachy() {
        return this.hirerachy
    }

    /**
     * @description 필터 조건에따른 데이터 정제
     */
    @action setRecordFilter(param: string) {
        if (param == 'all') {
            this.kpiRecord = this.baseData;
        } else {
            const filterData = this.baseData.filter((parame: KpiRecordData) => parame.relatedId == param && parame.day ==this.pickerDate);
            this.kpiRecord = filterData;
        }
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @desciption KPI RECORD LIST에서 선택한 내용 저장
     */
    @observable selectRecord: any = [];

    @action setSelectRecord(param: any) {
        this.selectRecord = param;
    }

    @computed get getSelectRecord() {
        return this.selectRecord
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @action recordUpdate(param: any) {
        this.reRelatedId = param.relatedId;
        this.recordKpiId = param.kpiId;
        this.product = param.text;
        this.recordValue = param.value;
        this.recordUnit = param.unit;
        this.recordDay = param.day;

    }

// getter, setter▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable reRelatedId: string = '';

    @action setReRelatedId(param: string) {
        this.reRelatedId = param;
    }

    @computed get getReRelatedId() {
        return this.reRelatedId;
    }

    @observable recordKpiId: string = '';

    @action setRecordKpiId(param: string) {
        this.recordKpiId = param;
    }

    @computed get getRecordKpiId() {
        return this.recordKpiId;
    }

    @observable product: string = '';

    @action setProduct(param: string) {
        this.product = param;
    }

    @computed get getProduct() {
        return this.product;
    }

    @observable recordValue: string = '';

    @action setRecordValue(param: string) {
        this.recordValue = param;
    }

    @computed get getRecordValue() {
        return this.recordValue;
    }

    @observable recordUnit: string = '';

    @action setRecordUnit(param: string) {
        this.recordUnit = param;
    }

    @computed get getRecordUnit() {
        return this.recordUnit;
    }

    @observable recordDay: string = '';

    @action setRecordDay(param: string) {
        this.recordDay = param;
    }

    @computed get getRecordDay() {
        return this.recordDay;
    }

// Update하기위한 파라미터 보내기▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @computed get getRecordParame() {
        let param = {
            relatedId: this.reRelatedId,
            p_id:null,
            kpiId: this.recordKpiId,
            text: this.product,
            value: this.recordValue,
            unit: this.recordUnit,
            day: this.recordDay,
        }
        return param
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable pickerDate: string = StringUtile.formatDate();

    @action setPickerDate(param: string) {
        this.pickerDate = param;
        this.setKpiRecord(this.baseData);
    }

    @computed get getPickerDate() {
        return this.pickerDate;
    }

// KPI MASTER 검색조건▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    @observable masterSelect: string = 'kpiId';

    @action setMasterSelect(param: string) {
        this.masterSelect = param;
    }

    @computed get getMasterSelect() {
        return this.masterSelect;
    }


    @observable searchString: string = 'kpiId';

    @action SetSearchString(param: string) {
        this.searchString = param;
    }

    @computed get getSearchString() {
        return this.searchString;
    }
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    @action setSearch(param: string) {
       switch (this.masterSelect) {
           case 'kpiId':
                const test1 =  this.getKpiMaster.filter(function(store:any){
                    return store.kpiId.indexOf(param) !== -1
                    }
           )
               this.subKpiMaster = test1
               break;
           case 'kpiName':
              const test2 = this.getKpiMaster.filter(function(store:any){
                       return store.kpiName.indexOf(param) !== -1
                   }
               )
               this.subKpiMaster = test2
               break;
       }
    }
}

export default kpiStore;