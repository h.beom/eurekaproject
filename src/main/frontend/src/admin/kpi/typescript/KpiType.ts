
/**
 * @description KPI MASTER 의 데이터 타입
*/
 export interface KpiMasterData{
    kpiId: string,
    kpiName: string,
    uiType: string,
    unit: string,
    description:string,
    scope:string,
    formula:string,
    unitOfMeasure: string,
    range: string,
    trend: string,
    timing: string,
    audience: string,
    prodMeth: string,
    notes: string,
    midCate: string,
    subCate: string,
    preference: string,
    diagramFile: string,
    day?: string,
    time?: string,
    writer?: string
}



/**
 * @description KPI Record 의 데이터 타입
 */
export interface KpiRecordData{
   relatedId : string,
   p_id :string,
   name :string,
   kpiId :string,
   text :string,
   value :string,
   unit :string,
   day :string
}

/**
 * @description KPI Search 의 데이터 타입
 */
export interface KpiRecordSearch{
   id : string,
   type :string,
   name :string,
   hirerachyName :string
}