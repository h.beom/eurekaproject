import React, {Component} from 'react';
import {Card, Tabs} from "antd";
import {Link} from "react-router-dom";

const { TabPane } = Tabs;

class En extends Component<any, any>{


    callback = (key: any) => {
        // 후에 탭 변경시 변경되어야 할 것이 있다면 기입.
    }


    render() {
        return (

                        <Card>

                            <h1 id={'main'} >Automation systems and integration - Key performance
                                indicators (KPIs) for manufacturing operations management</h1>


                            <br /><br />
                            <br /><br />

                            <h1>
                                Contents
                            </h1>



                            <div style={{ textAlign: "left", fontWeight: "bold" }}> Part 1: Overview, concepts and terminology</div>
                            <div style={{ textAlign: "left", fontWeight: "bold" }}> Part 2: Definitions and descriptions</div>


                            <br /><br />
                            <br /><br />
                            <br /><br />
                            <br /><br />

                            <h2 id={'scope'}  style={{ textAlign: "left", fontWeight: "bold" }}> Scope</h2>
                            <div style={{ textAlign: "left" }}>ISO 22400 specifies an industry-neutral framework for defining, composing, exchanging, and using key performance indicators (KPIs) for
                                manufacturing operations management (MOM), as defined in IEC 62264-1, for batch, continuous and discrete industries.</div>
                            <div style={{ textAlign: "left" }}>This part of ISO 22400 <br />
                                — provides an overview of a KPI;<br />
                                — presents concepts of relevance for working with KPIs, including criteria for constructing KPIs; <br />
                                — specifies terminology related to KPIs;<br />
                                — describes how a KPI can be used.
                            </div>

                            <br /><br />


                            <h2 id={'1'} style={{ textAlign: "left", fontWeight: "bold" }}> Terms and definitions</h2>

                            <div style={{ textAlign: "left" }}>ISO 22400 specifies an industry-neutral framework for defining, composing, exchanging, and using key performance indicators (KPIs) for
                                manufacturing operations management (MOM), as defined in IEC 62264-1, for batch, continuous and discrete industries.</div>
                            <div style={{ textAlign: "left" }}>This part of ISO 22400 <br />
                                — provides an overview of a KPI;<br />
                                — presents concepts of relevance for working with KPIs, including criteria for constructing KPIs; <br />
                                — specifies terminology related to KPIs;<br />
                                — describes how a KPI can be used.
                            </div>

                            <br /><br />


                            <h2 style={{ textAlign: "left", fontWeight: "bold" }}>Terms and definitions</h2>
                            <p>For the purposes of this document, the following terms and definitions apply.</p>
                            <p>capability</p>
                            <p>ability to perform actions</p>
                            <p>Note 1 to entry: The definition includes attributes on qualifications and measures of the ability, as in the definition of capacity.</p>
                            <p>element</p>
                            <p>relevant measurements for use in the formula of a key performance indicator.</p>
                            <p>integration</p>
                            <p>state or condition wherein two or more entities are able to form, or be observed as, a single entity exhibiting a structure, a behavior, and a boundary</p>
                            <p>that are determined by the interoperability properties of the forming entities, as needed to perform a common task.</p>
                            <p>interoperability</p>
                            <p>capability of two or more entities to exchange items in accordance with a set of rules and mechanisms implemented by an interface in each entity, in</p>
                            <p>order to perform their respective tasks</p>
                            <p>Note 1 to entry: Examples of entities include devices, equipment, machines, people, processes, applications, software units, systems and enterprises.</p>
                            <p>Note 2 to entry: Examples of items include information, material, energy, control, assets and ideas.</p>

                                <br/>

                            <br /><br /><br /><br /><br />
                            <h2 id={'2'} style={{ textAlign: "left", fontWeight: "bold" }}>Abbreviated terms</h2>
                            <p>ID Identification</p>
                            <p>KPI Key Performance Indicator</p>
                            <p>KPI-E Key Performance Indicator Effectiveness (핵심 성과 지표 효과)</p>
                            <p>MOM Manufacturing Operations Management (제조 운영 관리)</p>
                            <p>UML Unified Modeling Language</p>
                            <p>URL Uniform Resource Locator</p>
                            <p>XML eXtensible Mark-up Language</p>
                            <br /><br />
                            <h2 id={'3'}  style={{ textAlign: "left", fontWeight: "bold" }}>Concept of KPIs – Criteria for KPIs</h2>
                            <p>A good KPI has certain criteria which ensure its usefulness in achieving various goals in the manufacturing operation. The criteria are listed below,</p>
                            <p>along with the process for performing each individual measurement.</p>
                            <p>a) Aligned: the KPI is aligned to the degree to which the KPI affects change in relevant higher-level KPIs, where alignment implies a high ratio of the</p>
                            <p>percent improvement (assuming positive impact) in important higher-level metrics to the percent improvement in a KPI (or KPI set), given no other</p>
                            <p>changes in the system.</p>
                            <p>b) Balanced: the extent to which a KPI is balanced within its chosen set of KPIs.</p>
                            <p>c) Standardized: the KPI is standardized to the extent to which a standard for the KPI exists and that standard is correct, complete, and unambiguous;</p>
                            <p>the standard can be plant-wide, corporate-wide, or industry-wide.</p>
                            <p>d) Valid: the KPI is valid to the extent of the syntactic (i.e. grammar) and semantic (i.e. meaning) compliance between the operational definition of the</p>
                            <p>KPI and the standard definition. If no standard exists, then validity is zero.</p>
                            <p>e) Quantifiable: the KPI is quantifiable to the extent to which the value of the KPI can be numerically specified; there is no penalty for the presence of</p>
                            <p>uncertainty, as long as the uncertainty can also be quantified.</p>
                            <p>f) Accurate: the KPI is accurate to the extent to which the measured value of the KPI is close to the true value, where a departure from the true value</p>
                            <p>can be affected by poor data quality, poor accessibility to the measurement location, or the presence of substandard measurement devices and</p>
                            <p>methods.</p>
                            <p>g) Timely: the KPI is timely to the extent it is computed and accessible in real-time, where real-time depends on the operational context.</p>
                            <p>h) Predictive: the KPI is predictive to extent to which a KPI is able to predict non-steady-state operations.</p>
                            <p>i) Actionable: the KPI is actionable to the extent to which a team responsible for the KPI has the knowledge, ability, and authority to improve the</p>
                            <p>actual value of the KPI within their own process.</p>
                            <p>j) Trackable: the KPI is trackable to the extent to which the appropriate steps to take to fix a problem are known, documented, and accessible, where</p>
                            <p>the particular problem is indicated by particular values or temporal trends of the KPI.</p>
                            <p>k) Relevant: the KPI is relevant to the extent to which the KPI enables performance improvement in the target operation, demonstrates real-time</p>
                            <p>performance, allows the accurate prediction of future events, and reveals a record of the past performance valuable for analysis and feedback control.</p>
                            <p>l) Correct: the KPI is correct to the extent that, compared to the standard definition (if one exists), the calculation required to compute the value of</p>
                            <p>the KPI compared to the standard definition (if one exists) has no errors with respect to the standard definition.</p>
                            <p>m) Complete: the KPI is complete to the extent that, compared to the standard definition (if one exists), the definition of the KPI, and the calculation</p>
                            <p>required to compute the value of the KPI, covers allparts, and no more, of the standard definition.</p>
                            <p>n) Unambiguous: the KPI is unambiguous to the extent that the syntax (i.e. grammar) and semantics (i.e. meaning) in the definition of the KPI lacks</p>
                            <p>ambiguity or uncertainty.</p>
                            <p>o) Automated: the KPI is automated to the extent that KPI collection, transfer, computation, implementation, and reporting are automated.</p>
                            <p>p) Buy-in: the KPI has buy-in to the extent that the team responsible for the target operation, as well as teams responsible for both upper and lower</p>
                            <p>level KPIs, are willing to support the use of the KPI and perform the tasks necessary to achieve target values for the KPI; includes difficulty of obtaining</p>
                            <p>official approval by management for the KPI.</p>
                            <p>q) Documented: the KPI is documented to the extent that the documented instructions for implementation of a KPI are up-to date, correct, complete,</p>
                            <p>and unambiguous, including instructions on how to compute the KPI, what measurements are necessary for its computation, and what actions to take</p>
                            <p>for different KPI values.</p>
                            <p>r) Comparable: the KPI is comparable to the extent that means are defined to reference supporting measurements over a period of time, and a</p>
                            <p>normalizing factor to express the indicator in absolute terms with appropriate units of measure.</p>
                            <p>s) Understandable: the KPI is understandable to the extent that the meaning of the KPI is comprehended by team members, management, and</p>
                            <p>customers, particularly with respect to corporate goals.</p>
                            <p>t) Inexpensive: the KPI is inexpensive to the extent that the cost of measuring, computing, and reporting the KPI is low.</p>
                            <p>Characterization of KPIs – General</p>
                            <p>A KPI is characterized by information regarding its content and context.</p>
                            <p>a) content information: a quantifiable element with a specific unit of measure; (특정 측정 단위를 가진 계측 가능한 요소)</p>
                            <p>b) context information: a verifiable list of conditions that are met. (충족되는 조건의 검증 가능한 목록)</p>
                            <p>The factors that determine the value of a KPI are assumed to be accessible to change using a particular action plan. The action plan describes the</p>
                            <p>activities that will lead to achieving the objective of the operation, the resources and actors required for performing the activities, and the timeframe</p>
                            <p>for completing these activities</p>
                            <br /><br /><br /><br /><br />
                            <h2 id={'4'}  style={{ textAlign: "left", fontWeight: "bold" }}>Characterization of KPIs – Content information</h2>
                            <p>When a definition of a KPI is given, it should contain information about its content:</p>
                            <p>a) name: name of KPI, e.g. availability, worker effectiveness;</p>
                            <p>b) ID: a user-defined unique identification of the KPI in the user’s environment;</p>
                            <p>c) description: a description of the KPI;</p>
                            <p>d) scope: identification of the element for which the KPI is relevant, e.g. a work unit, work center, work order, product, or personnel;</p>
                            <p>e) formula: the mathematical formula of the KPI defined in terms of elements;</p>
                            <p>f) unit of measure: the basic unit or dimension in which the KPI is expressed;</p>
                            <p>g) range: the upper and lower logical limits of the KPI;</p>
                            <p>h) trend: information about the improvement direction, i.e. higher-is-better or lower-is-better.</p>
                            <br /><br /><br /><br /><br />
                            <h2 style={{ textAlign: "left", fontWeight: "bold" }}>Characterization of KPIs – Context information</h2>
                            <p>The specification of a KPI should contain information about its context, including timing, audience, production methodology, effect model diagram,</p>
                            <p>and notes.</p>
                            <p>a) The timing context information should specify the frequency of KPI calculation as following:</p>
                            <p>1) real-time (as the process is occurring): after each new data acquisition event,</p>
                            <p>2) periodically: done at a certain interval, e.g. one time per day, or</p>
                            <p>3) on-demand: after a specific data selection request.</p>
                            <p>b) Constraints: information about possible constraints on how the KPI can be used.</p>
                            <p>c) Usage: information about how to use the KPI.</p>
                            <p>d) The audience context information should specify the user group typically utilizing the KPI. The user-groups in ISO 22400 may include:</p>
                            <p>1) operators: personnel responsible of direct operation of the equipment,</p>
                            <p>2) supervisors: personnel responsible for directing the activities of the operators, and</p>
                            <p>3) management: personnel responsible for the overall execution of production.</p>
                            <p>e) The production methodology should identify the method of production for which the KPI is generally applicable: batch, continuous, and/or discrete.</p>
                            <p>f) The effect model diagram information should specify the location of the diagram depicting the composition of the KPI from measurement sources.</p>
                            <p>An effect model diagram is a graphical representation of the dependencies of the KPI elements that is useful for understanding the impact of the</p>
                            <p>source values.</p>
                            <br /><br /><br /><br /><br />
                            <h2 style={{ textAlign: "left", fontWeight: "bold" }}>Characterization of KPIs – Context information</h2>
                            <p>g) The notes should specify additional information related to the KPI calculation or use. This information may include:</p>
                            <p>1) Constraints that apply to the KPI in certain situations that make the KPI valid or invalid,</p>
                            <p>2) Usage situations where the KPI is particularly useful for understanding performance improvement opportunities or needs, and</p>
                            <p>3) Other info that can be of relevance for the usage of the KPI, e.g. physical structure necessary, related operational categories, and</p>
                            <p>improvement methods.</p>
                            <p>EXAMPLE 1 KPI constraints can be that a certain KPI only holds for production with a single path structure, or a certain KPI is useful only if the</p>
                            <p>personnel are permanent to a working unit.</p>
                            <p>EXAMPLE 2 Physical structure can be single path, multiple path, or network-based with single or multiple products.</p>
                            <p>EXAMPLE 3 Related categories can be production operations, maintenance operations, inventory operations, or quality operations.</p>
                            <p>EXAMPLE 4 Improvement methods can be lean, total quality management, world class manufacturing, Six Sigma, etc..</p>
                            <br /><br /><br /><br /><br />

                            <h2 id={'5'}  style={{ textAlign: "left", fontWeight: "bold" }}>dentification and selection of KPIs</h2>
                            <p>To identify KPIs, the following aspects of the target operation are assumed to be known:</p>
                            <p>— well-defined component processes of the target operation;</p>
                            <p>— required conditions to conduct the component processes;</p>
                            <p>— quantitative and qualitative measures of the outcomes and of the objectives;</p>
                            <p>— available courses of action for adjusting the processes and resources to achieve the operation’s objectives.</p>
                            <p>KPIs are selected to focus on users’ needs and expectations regarding the outcomes of the manufacturing operations, without restricting the means of</p>
                            <p>meeting those needs and expectations. The intent of ISO 22400 is to allow the broadest possible use of the KPI definitions across a variety of industry</p>
                            <p>sectors and regional markets.</p>
                            <p>The selection and use of KPIs within a manufacturing enterprise are illustrated by the following steps:</p>
                            <p>a) operations and elements of operations to be evaluated are identified;</p>
                            <p>b) objectives to be realized with use of performance indicators are determined;</p>
                            <p>c) operational actions when using performance indicators to realize expectations are described;</p>
                            <p>d) defining assessment criteria and associated measurements for performance indicators;</p>
                            <p>e) selecting KPIs;</p>
                            <p>f) assessing performance versus objectives with the KPIs obtained;</p>
                            <p>g) performing associated actions to meet objectives.</p>
                            <p>In some manufacturing enterprises, the KPIs have an owner, i.e. a person or a team responsible for achieving the desired result.</p>


                            <br /><br /><br /><br /><br />

                            <h2 id={'6'}  style={{ textAlign: "left", fontWeight: "bold" }}>Definitions and descriptions</h2>

                            <Link to={"/files/표준정리_Part1"}  target="_blank" download>Download</Link>



                        </Card>

        );
    };


}

export default En;