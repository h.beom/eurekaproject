import React, {Component} from 'react';
import {Card, Tabs} from "antd";
import Ko from "./Ko";
import En from "./En";
import {Link} from "react-router-dom";

const {TabPane} = Tabs;

class Information extends Component<any, any> {


    callback = (key: any) => {
        // 후에 탭 변경시 변경되어야 할 것이 있다면 기입.
    }


    render() {
        return (
            <>
                <Card style={{width:'21%', float:"right", marginTop:55
                    ,position:"fixed", top:180, zIndex:1, right:30
                }}>
                    <a href={'#main'}>Main</a><br/>
                    <a href={'#scope'}>Scope</a><br/>
                    <a href={'#1'}>Terms and definitions</a><br/>
                    <a href={'#2'}>Abbreviated terms</a><br/>
                    <a href={'#3'}>Concept of KPIs – Cri...</a><br/>
                    <a href={'#4'}>Characterization of K...</a><br/>
                    <a href={'#5'}>dentification and sel...</a><br/>
                    <a href={'#6'}>Definitions and descr...</a><br/>

                </Card>
                <Tabs onChange={this.callback} type="card">
                    <TabPane tab="KO" key="1">

                        <Ko/>
                    </TabPane>
                    <TabPane tab="EN" key="2">
                        <En/>

                    </TabPane>
                </Tabs>
            </>
        );
    };


}

// @ts-ignore
export default Information;