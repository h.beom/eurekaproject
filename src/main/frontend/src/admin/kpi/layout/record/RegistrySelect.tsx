import React, {Component} from 'react';
import {Button, Card, Input, PageHeader, Select} from "antd";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import {Link} from "react-router-dom";

const {Option} = Select;

@inject('kpi')
@observer
class RegistrySelect extends Component<any, any> {

    componentDidMount(){
    }

    render() {
    const {kpi} = this.props;
    const pk = kpi.getSelectRecord;
        return (
            <>

                <PageHeader style={{backgroundColor: 'white'}}
                            title="KPI Record Select"
                            subTitle="key perfomance indicator select "/>

                <Card>
                    <div style={{width: 500, margin: "auto"}}>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>name</div>
                            <div style={{width: 300, float: "right"}}>{pk.name}</div>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>KPI ID</div>
                            <div style={{width: 300, float: "right"}}>{pk.kpiId}</div>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Product</div>
                            <div style={{width: 300, float: "right"}}>{pk.text}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>값</div>
                            <div style={{width: 300, float: "right"}}>{pk.value}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Unit</div>
                            <div style={{width: 300, float: "right"}}>{pk.unit}</div>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Day</div>
                            <div style={{width: 300, float: "right"}}>{pk.day}</div>
                        </div>
                    </div>

                    <div style={{width: 300, margin: "auto"}}>
                        <Link to={'/Information/recordUpdate/' + pk.relatedId + pk.text} onClick={()=>{kpi.recordUpdate(pk)}}>
                        <Button style={{width:'45%', float:"left"}} type={'primary'}>수정</Button>
                        </Link>
                        <Button style={{width:'45%', float:"right"}} type={'danger'}onClick={() => history.go(-1)}>취소</Button>
                    </div>
                </Card>
            </>
        );
    };


}

export default RegistrySelect;