import React, {Component} from 'react';
import MasterListHeader from './List/MasterListHeader';
import {Button, Card, Select, Table} from 'antd';
import {Link} from 'react-router-dom';
import Searvice from "../../../../common/Searvice";
import {inject, observer} from "mobx-react";
import {KpiMasterData} from "../../typescript/KpiType";


@inject('kpi')
@observer
class Master extends Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = {
            selectedRowKeys: [], //테이블 체크박스 key 정보를 담는 state
            selectedRows: [], //테이블 체크박스 row 정보를 담는 state
        };
    }



    async componentDidMount(){
        const {kpi} = this.props;

        let parameter ={
            mapId : '1'
        }
        if(kpi.getKpiMaster.length == 0) {
            const responseData: any = await Searvice.getPost("dash/getKpiMaster.do", parameter);
            kpi.setKpiMaster(responseData.data.kpiMasterList)
        }else{
            kpi.setSubKpiMaster(kpi.getKpiMaster);

        }
    }

    private delete = async () => {
        const {selectedRows} = this.state;

        let parameData: Array<string>[] = selectedRows.map((info: KpiMasterData) => {
            return info.kpiId
        });
        let param ={
            selectKpiId : parameData
        }
        await Searvice.getPost("dash/deleteKpiMaster.do", param);


    }

    onSelectChange = (selectedRowKeys:any, selectedRows:any) => {
        this.setState({ selectedRowKeys, selectedRows });
    };

    render() {
        const columns = [
            {
                title: 'KPIID',
                dataIndex: 'kpiId',
                key: 'kpiid',
            },
            {
                title: 'SCOPE',
                dataIndex: 'scope',
                key: 'scope',
            },
            {
                title: 'KPINAME',
                dataIndex: 'kpiName',
                key:'kpiName',
                render: function (text: string, record: KpiMasterData) {
                    const item: KpiMasterData = record;
                    return <Link to={"/Information/MasterSelect/" + item.kpiId}>{text}</Link>
                },
                onCell: (recode: KpiMasterData, rowIndex: number) => {
                    const data:KpiMasterData = recode;
                    return {
                        onClick:() => {
                            this.props.kpi.setSelectKpi(data);
                        }
                    }
                }
            },
            {
                title: 'UPDATE TIME',
                dataIndex: 'time',
            }
        ];


    const {kpi} = this.props;
        const { selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        return (
            <>
                <MasterListHeader />
                <br />
                <Card>
                    <div>

                        <div style={{ width: '25%', margin: 'auto', float: 'right', textAlign: 'center' }}>
                            <Link to={'/Information/MasterRegistry'}>
                                <Button style={{ width: '33%', float: 'left' }} type={'primary'} size={'small'}>추가</Button>
                            </Link>
                            <Button style={{ width: '33%' }} type={'danger'} size={'small'} onClick={this.delete}>삭제</Button>
                            <Button style={{ width: '33%', float: 'right' }} type={'default'} size={'small'}>Excel</Button>
                        </div>
                    </div>
                    <br />
                    <Table
                        pagination={false}
                        style={{width:730}}
                        columns={columns}
                        rowKey={((record:any, index:any) => index)}
                        rowSelection={rowSelection}
                        dataSource={kpi.getSubKpiMaster}
                        bordered
                    />
                </Card>


            </>
        );
    };
}

export default Master;