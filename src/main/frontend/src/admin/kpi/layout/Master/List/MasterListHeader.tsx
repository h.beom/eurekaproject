
import React, { Component } from 'react';
import { PageHeader, Select, Input, Button } from 'antd';
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";

const { Option } = Select;

@inject('kpi')
@observer
class MasterListHeader extends Component<any, any>{

    @autobind
    public searchResult(){
        const {kpi} = this.props;
        kpi.setSearch(kpi.getSearchString);
    }
    @autobind
    public searchString(e: React.FormEvent<HTMLInputElement>){
        this.props.kpi.SetSearchString(e.currentTarget.value);
    }
    @autobind
    public kpiSearch(value:string){
        this.props.kpi.setMasterSelect(value);
    }
    render() {
        const {kpi} = this.props;
        return (
            <>
                <PageHeader style={{ backgroundColor: 'white'}}
                    title="KPI Master List"
                    subTitle="key perfomance indicator list ">
                    <div style={{width:'70%', float:'left'}}>
                        <span style={{ textAlign: 'left' }}>조건 : </span>
                        <Select value={kpi.getMasterSelect} onChange={this.kpiSearch}
                            style={{ width: '20%'}}
                        >
                            <Option value="kpiId">KPI ID</Option>
                            <Option value="kpiName">KPI NAME</Option>

                        </Select>
                        <Input onChange={this.searchString} style={{width:'70%'}}/>
                    </div>

                    <div style={{width:'30%', margin:'auto', float:'right', textAlign:'center'}}>
                        <Button style={{width:'48%',float:'left'}} type={'primary'} onClick={this.searchResult}>조회</Button>
                        <Button style={{width:'48%',float:'right'}} type={'danger'}>초기화</Button>
                    </div>


                </PageHeader>

            </>
        );
    };
}

export default MasterListHeader;