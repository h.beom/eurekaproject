export interface loginType{
    id: string,
    pw:string,
};

export interface signUpType{
    id: string,
    email: string,
    name:string,
    pw:string,
    pw2:string,
    phone:string,
    birth:string,
    address:string
};
export interface FindPwType{
    id: string,
    email:string,
};
