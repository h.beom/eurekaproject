import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Button, Card, DatePicker, Icon, Input, Menu, Modal, Tooltip, message} from "antd";
import {Credentials, RegisterButton, RegistInput} from "../typescript/UserStyled";
import SignUpStep from "./SignUpStep";
import {Link} from "react-router-dom";
import autobind from "autobind-decorator";
import moment from 'moment';
import DaumPostcode from 'react-daum-postcode';
import Searvice from "../../../common/Searvice";

@inject('user')
@observer
class SignUpForm extends Component<any, any> {
    state = {
        current: 'user',
        checkId: false
    };

    /**
     * @description 회원가입 아이디
     */
    @autobind
    private changeId(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignId(e.currentTarget.value);
    }

    /**
     * @description 회원가입 이메일
     */
    @autobind
    private changeEmail(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignEmail(e.currentTarget.value);
    }

    /**
     * @description 회원가입 이름
     */
    @autobind
    private changeName(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignName(e.currentTarget.value);
    }

    /**
     * @description 회원가입 비밀법호
     */
    @autobind
    private changePw(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignPw(e.currentTarget.value);
    }

    /**
     * @description 회원가입 비밀번호 확인
     */
    @autobind
    private changeConfirmPw(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignConfirmPw(e.currentTarget.value);
    }

    /**
     * @description 회원가입 전화번호
     */
    @autobind
    private changePhone(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignPhone(e.currentTarget.value);
    }

    /**
     * @description 회원가입 생일
     */
    @autobind
    private changeBirthday(date: moment.Moment | null, dateString: string) {
        this.props.user.setSignBirthday(dateString);
    }

    /**
     * @description 회원가입 주소
     */
    @autobind
    private changeAddress(e: React.FormEvent<HTMLInputElement>) {
        this.props.user.setSignAddress(e.currentTarget.value);
    }

    /**
     * @description 회원가입 요청
     */
    @autobind
    private async submitSignUp() {
        const {user} = this.props;
        if(this.state.checkId == false){
            message.error('중복확인을 해야합니다.');
        }else if(user.getSignPw !== user.getSignConfirmPw){
            message.error('두 비밀번호가 일치하지 않습니다.');
        }else{
            let parameter = this.props.user.registParame
            try {
                await Searvice.getPost("user/register.do", parameter);
                window.location.replace("/user/signupsucess");
            } catch (e) {
                console.log(e);
            }
        }
    }

    /**
     * @description 우편번호 찾기
     **/
    @autobind
    public searchAdress() {
        this.props.user.setSearchAddress(true);
    }

    @autobind
    public searchAdressOK() {
        this.props.user.setSearchAddress(false);
    }

    @autobind
    public searchAdressCancle() {
        this.props.user.setSearchAddress(false);
    }

    @autobind
    public handleData(data: any) {
        // 받은 데이터 우편번호로 쓰기
    }

    /**
     * @description 아이디 중복확인 모달
     **/
    @autobind
    public async idOverlapModal() {


        let parameter = {
            id: this.props.user.getSignId,
            pw: ''
        }
        try {
            const resultInt = await Searvice.getPost("user/idCheck.do", parameter);
            if (resultInt.data.checkId == 1) {
                message.error('아이디가 이미 존재합니다.');
            } else {
                message.success('사용 가능한 아이디입니다.');
                this.setState({
                    checkId : true
                })
            }
        } catch (e) {
            console.log(e);
        }

        this.props.user.setOverlap(true);
    }


    render() {
        const dateFormat = 'YYYY/MM/DD';
        const {user} = this.props;
        return (
            <div>
                <SignUpStep/>
                <Credentials selectedKeys={[this.state.current]} mode="horizontal">
                    <Menu.Item key="user">
                        <Icon type="user"/>
                        Register
                    </Menu.Item>
                </Credentials>

                <Card style={{backgroundColor: '#f0f2f5'}} title={<span style={{float: "left"}}>필수 정보</span>}
                      size={'small'}>
                    <div>
                        <Button type={'default'}
                                style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>아이디</Button>
                        <Input style={{float: 'left', width: '45%', marginTop: 12, height: 35, marginLeft: 9}}
                               onChange={this.changeId}
                               placeholder="Enter your User"
                               prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                               suffix={
                                   <Tooltip title="User Name">
                                       <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                   </Tooltip>
                               }
                        />
                        <Button type={'primary'} style={{float: "right", marginTop: 12, height: 35, width: '24%'}}
                                onClick={this.idOverlapModal}>중복 확인</Button>
                    </div>
                    <div>
                        <Button type={'default'} style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>사용자
                            이메일</Button>
                        <RegistInput
                            onChange={this.changeEmail}
                            placeholder="Enter your Email"
                            prefix={<Icon type="mail" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            suffix={
                                <Tooltip title="User Email">
                                    <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                </Tooltip>
                            }
                        />
                    </div>
                    <div>
                        <Button type={'default'} style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>사용자
                            이름</Button>
                        <RegistInput
                            onChange={this.changeName}
                            placeholder="Enter your User"
                            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            suffix={
                                <Tooltip title="User Name">
                                    <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                </Tooltip>
                            }
                        />
                    </div>
                    <div>
                        <Button type={'default'}
                                style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>비밀번호</Button>
                        <RegistInput
                            type={'password'}
                            onChange={this.changePw}
                            placeholder="Enter your password"
                            prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            suffix={
                                <Tooltip title="Extra information">
                                    <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                </Tooltip>
                            }
                        />
                    </div>
                    <div>
                        <Button type={'default'} style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>비밀번호
                            확인</Button>
                        <RegistInput
                            type={'password'}
                            onChange={this.changeConfirmPw}
                            placeholder="Enter your password again"
                            prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            suffix={
                                <Tooltip title="Extra information">
                                    <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                </Tooltip>
                            }
                        />
                    </div>
                    <div>
                        <Button type={'default'}
                                style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>전화번호</Button>
                        <RegistInput
                            onChange={this.changePhone}
                            placeholder="Enter your Phone number"
                            prefix={<Icon type="phone" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            suffix={
                                <Tooltip title="Extra information">
                                    <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                </Tooltip>
                            }
                        />
                    </div>
                    <div>
                        <Button type={'default'}
                                style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>생년월일</Button>
                        <DatePicker
                            onChange={this.changeBirthday}
                            style={{width: '70%', float: "right", height: 35, marginTop: 12}}
                            defaultValue={moment('1900/01/01', dateFormat)} format={dateFormat}/>
                    </div>
                    <div>
                        <Button type={'default'}
                                style={{float: "left", marginTop: 12, height: 35, width: '28%'}}>주소</Button>
                        <Button style={{float: 'right', width: '70%', marginTop: 12, height: 35}} type={'primary'}
                                onClick={this.searchAdress}> 우편번호 검색</Button>
                        <Input
                            style={{width: '100%', float: 'left', height: 35, marginTop: 12}}
                            onChange={this.changeAddress}
                            placeholder="Enter your Adress"
                            prefix={<Icon type="environment" style={{color: 'rgba(0,0,0,.25)'}}/>}
                            suffix={
                                <Tooltip title="Extra information">
                                    <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                                </Tooltip>
                            }
                        />
                    </div>
                </Card>

                <div style={{width: '100%', marginTop: 5}}>
                        <RegisterButton type={'primary'} onClick={this.submitSignUp}>Registry</RegisterButton>
                    <Link to={'/user/login'}>
                        <div style={{float: "right", color: 'rgb(24, 144, 255)', marginTop: 15}}>Already have an
                            account?
                        </div>
                    </Link>
                </div>
                {/*우편찾기모달*/}
                <Modal
                    title="Search Adress"
                    visible={user.getSearchAddress}
                    onOk={this.searchAdressOK}
                    onCancel={this.searchAdressCancle}
                >
                    <DaumPostcode
                        onComplete={this.handleData}
                    />
                </Modal>


            </div>
        );
    };
}


export default SignUpForm;
