import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Icon, Typography} from "antd";
import {LoginFormBox} from "../typescript/UserStyled";
import LoginForm from "./LoginForm";
import {Link, Route, RouteComponentProps, Switch} from "react-router-dom";
import SignUpForm from "./SignUpForm";
import ForgotPw from "./ForgotPw";
import Terms from "./Terms";
import SignUpSuccess from "./SignUpSuccess";
// import SignUpForm from "./SignUpForm";
// import ForgotPw from "./ForgotPw";
// import Terms from "./Terms";
// import SignUpSuccess from "./SignUpSuccess";

@inject('user')
@observer
class Login extends Component<any, any> {

    render() {
        const {Title, Text} = Typography;
        return (
            <LoginFormBox>
                <Title>
                    <Link to={'/'}>
                        <img src={require('../../../store/logo.png')} width="200" height="150" style={{margin:30}} />
                    </Link>
                </Title>
                <Switch>
                    <Route path="/user/login" component={LoginForm}/>
                    <Route path="/user/signup" component={SignUpForm}/>
                    <Route path="/user/forgotpw" component={ForgotPw}/>
                    <Route path="/user/terms" component={Terms}/>
                    <Route path="/user/signupsucess" component={SignUpSuccess}/>
                    <LoginForm/>
                </Switch>
            </LoginFormBox>
        );
    };
}


export default Login;
