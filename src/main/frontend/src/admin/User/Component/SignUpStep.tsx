import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Icon, Steps} from "antd";

@inject('user')
@observer
class SignUpStep extends Component<any, any> {

    render() {
        const { Step } = Steps;
        return (
            <div>
                <Steps style={{marginLeft:-40}}>
                    <Step status="finish" title="Terms" icon={<Icon type="bars"/>} />
                    <Step status="finish" title="Information" icon={<Icon type="user"/>} />
                </Steps>,
            </div>
        );
    };
}


export default SignUpStep;
