import {action, computed, observable} from 'mobx';
import {FindPwType, loginType, signUpType} from "../typescript/userType";
// @ts-ignore
import {nodemailer} from 'nodemailer';

class user {
    // ============================================
    // 로그인
    /**
     * @description 로그인시 입력하는 Id
     */
    @observable loginId:string ='';
    @action setLoginId(param: string) {
        
        
        this.loginId = param;
    }

    /**
     * @description 로그인시 입력하는 Pw
     */
    @observable loginPw:string ='';
    @action setLoginPw(param: string) {
        this.loginPw = param;
    }

    /**
     * @description 로그인 요청
     */
    @computed get submitLogin() {
        let parameter:loginType = {
            id: this.loginId,
            pw: this.loginPw
        }
        return parameter;
    }


    // ============================================
    // 회원가입
    /**
     * @description 회원가입시 Email
     */
    @observable signId:string ='';
    @action setSignId(param: string) {
        this.signId = param;
    }
    @computed get getSignId(){
        return this.signId;
    }

    /**
     * @description 회원가입시 Email
     */
    @observable signEmail:string ='';
    @action setSignEmail(param: string) {
        this.signEmail = param;
    }
    /**
     * @description 회원가입시 Name
     */
    @observable signName:string ='';
    @action setSignName(param: string) {
        this.signName = param;
    }
    /**
     * @description 회원가입시 Pw
     */
    @observable signPw:string ='';
    @action setSignPw(param: string) {
        this.signPw = param;
    }
    @computed get getSignPw(){
        return this.signPw;
    }

    /**
     * @description 회원가입시 Pw 확인
     */
    @observable signConfirmPw:string ='';
    @action setSignConfirmPw(param: string) {
        this.signConfirmPw = param;
    }
    @computed get getSignConfirmPw(){
        return this.signConfirmPw;
    }

    /**
     * @description 회원가입시 생년월일
     */
    @observable signPhone:string ='';
    @action setSignPhone(param: string) {
        this.signPhone = param;
    }
    /**
     * @description 회원가입시 생년월일
     */
    @observable signBirthday:string ='';
    @action setSignBirthday(param: string) {
        this.signBirthday = param;
    }
    /**
     * @description 회원가입시 주소
     */
    @observable signAddress:string ='';
    @action setSignAddress(param: string) {
        this.signAddress = param;
    }
    /**
     * @description 회원가입시 소속
     */
    @observable signPart:string ='';
    @action setSignPart(param: string) {
        this.signPart = param;
    }
    /**
     * @description 회원가입시 직무
     */
    @observable signJob:string ='';
    @action setSignJob(param: string) {
        this.signJob = param;
    }

    /**
     * @description 회원가입 요청
     */
    @action submitSignUp() {

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'lhbhb21c@gmail.com',  // gmail 계정 아이디를 입력
                pass: 'fkdlcb1324'          // gmail 계정의 비밀번호를 입력
            }
        });

        const mailOptions = {
            from: 'lhbhb21c@gmail.com',    // 발송 메일 주소 (위에서 작성한 gmail 계정 아이디)
            to: 'beom21c@dexta.kr',                     // 수신 메일 주소
            subject: 'Sending Email using Node.js',   // 제목
            text: 'That was easy!'  // 내용
        };

        transporter.sendMail(mailOptions, function(error:any, info:any){
            if (error) {
                console.log(error);
            }
            else {
                console.log('Email sent: ' + info.response);
            }
        });

    }




    /**
     * @description 비밀번호 찾기 아이디
     */
    @observable findId:string ='';
    @action setFindId(param: string) {
        this.findId = param;
    }
    /**
     * @description 비밀번호 찾기 이메일
     */
    @observable findEmail:string ='';
    @action setFindEmail(param: string) {
        this.findEmail = param;
    }

    /**
     * @description 비밀번호찾기 요청
     */
    @action submitFindPw() {
        let parameter:FindPwType = {
            id: this.findId,
            email: this.findEmail
        }
    }


    /**
     * @description 주소찾기 모달
     */
    @observable searchAddress:boolean = false ;

    @action setSearchAddress(param: boolean) {
        this.searchAddress = param;
    }

    @computed get getSearchAddress() {
        return this.searchAddress;
    }

    /**
     * @description 아이디 중복확인 모달
     */
    @observable overlap:boolean = false ;

    @action setOverlap(param: boolean) {
        this.overlap = param;
    }

    @computed get getOverlap() {
        return this.overlap;
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @computed get registParame() {

        let parameter:signUpType = {
            id : this.signId,
            email: this.signEmail,
            name:this.signName,
            pw:this.signPw,
            pw2:this.signConfirmPw,
            phone:this.signPhone,
            birth:this.signBirthday,
            address:this.signAddress
        }
        return parameter;
    }
}
export default user;