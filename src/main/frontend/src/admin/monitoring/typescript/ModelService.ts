import {Box3, Object3D, Vector3} from "three";

/**
 * ```
 * Model 관련 공통로직을 관리 하는 class
 * ```
 *
 */
export default class ModelService{

    private _onePercentWidth:number = 0;
    private _onePercentHeight:number= 0;
    private _onePercentDepth:number = 0;

    private _originalModelSize:{x:number, y:number,z:number} = {x:0,y:0,z:0};


    constructor(mapWidth:number , mapHeight:number , mapDepth:number){
        this._onePercentWidth   = mapWidth  / 100;
        this._onePercentHeight  = mapHeight / 100;
        this._onePercentDepth   = mapDepth  / 100;
    };

    public setOriginalModelSize( originalModelSize : {x:number, y:number,z:number} ){
        this._originalModelSize = originalModelSize;
    }
    /**
     * 모델 position
     * @param url 데이터를 요청할 통신주소
     * @param parameter 데이터 요청시 필요한 파라미터
     */
    public setSize(x:number, y:number , z:number ):{x:number, y:number,z:number}{
        let resultX:number = 0;
        let resultY:number = 0;
        let resultZ:number = 0;

        if( (x > 0) && (this._onePercentWidth > 0) ){
            resultX = ( this._onePercentWidth / this._originalModelSize.x ) * (( x ) / this._onePercentWidth);
        }

        if( (y > 0) && (this._onePercentDepth > 0) ){
            resultY = ( this._onePercentDepth / this._originalModelSize.y ) * (( y ) / this._onePercentDepth);
        }

        if( (z > 0) && (this._onePercentHeight > 0) ){
            resultZ = ( this._onePercentHeight / this._originalModelSize.z ) * (( z ) / this._onePercentHeight);
        }

        const result:{x:number ,y:number, z:number} = {
            x:resultX ,
            y:resultY ,
            z:resultZ
        }

        return result;
    }

    public setModelSize( object3dModel:Object3D , x:number , y:number , z:number ):Object3D{
        let box:Box3 = new Box3();
        box.setFromObject( object3dModel );
        let originalSize:{x:number ,y:number, z:number} = box.getSize( new Vector3() );
        this.setOriginalModelSize(originalSize);
        let item:{x:number ,y:number, z:number} = this.setSize( x,y,z );
        object3dModel.scale.set(item.x , item.y , item.z );
        return object3dModel;
    }
}