import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {DatePicker, PageHeader} from "antd";
import Searvice from "../../../common/Searvice";
import {Route, RouteComponentProps, Switch} from "react-router-dom";
import autobind from "autobind-decorator";
import Entire from "./Entire";
import moment from "moment";
import {GlobalParameter} from "../../DashBoard/typescript/DashBoardType";

/**
 * @description DASHBOARD 공장 정보KPI, 3D 맵을 구현하는 클래스,
 * 회사, 공장컴포넌트를 포함하는 Router가 포함된 클래스이다.
 */
@inject('dashboard', 'monitor', 'kpi')
@observer
class FactoryAll extends Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            renderEvent: function (type: boolean) {
            },
            depthList: []

        };
    }

    /**
     * @description 공장전체 렌더
     */
    @autobind
    private onRenderEntireFactory(props: RouteComponentProps<any>): React.ReactNode {
        return <Entire setRenderEvent={this.setRenderEvent}/>;
    }

    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function): void {
        this.setState({renderEvent: callBack});
    }

    // =-------------------------------------------------
    async componentDidMount() {
        const {dashboard, monitor} = this.props;
        monitor.setDashLoading(true);
        let parameter: { companyid: string, date: string } = {
            companyid: monitor.getPostId,
            date: dashboard.getPickerDate
        }

        try {
            const resultData: any = await Searvice.getPost("dash/getCPALSCompanyMap.do", parameter);
            monitor.setKpiArr(resultData.data.portletInfo);
            monitor.setComData(resultData.data.portletData);
        } catch (e) {
            console.log(e);
        }

        try {
            const resultData: any = await Searvice.getPost("dash/getFactoryList.do", parameter);
            monitor.setFactoryList(resultData.data.FactoryList);
        } catch (e) {
            console.log(e);
        }


        // depth0 (공장전체) 의 3d 정보 가져오기
        if (monitor.getDepth0 !== null) {
            let modelParam = {
                mapId: monitor.getDepth0.mapId,
            }

            try {
                const resultData: any = await Searvice.getPost("dash/getSimulation.do", modelParam);
                monitor.setMonitorModel(resultData.data.simulationModel);
                monitor.setMonitorMap(resultData.data.simulationMap);
            } catch (e) {
                console.log(e);
            }
        }
        let param = {
            id: monitor.getPostId
        }
        try {
            const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
            monitor.setKpiSearch(resultData.data.kpiSearch)
            monitor.setRelatedId(resultData.data.kpiSearch[0].id + '|' + resultData.data.kpiSearch[0].hirerachyName)
        } catch (e) {
            console.log(e);
        }
        monitor.setDashLoading(false);
    }

    /**
     * @description 가공라인 링크 탈때 데이터 호출
     **/
    @autobind
    async lineChange(value: string) {
        const {monitor} = this.props;
        this.props.monitor.setLineSelect(value);
        monitor.setDashLoading(true);
        //depth lvl = 2 중 해당 modelItemId의 맵정보를 가지고온다
        const resultData = monitor.getDepth2.filter((store: any) =>
            store.modelItemId == value
        );
        let parame = {
            simulationid: resultData[0].mapId,
            loclid: 'KO'
        }
        try {
            const resultData: any = await Searvice.getPost("dash/getMonitorModel.do", parame);
            monitor.setMonitorMap(resultData.data.mapData);
            monitor.setMonitorModel(resultData.data.modelListData);
        } catch (e) {
            console.log(e);
        }
        monitor.setDashLoading(false);
    }

    @autobind
    private async hirerachy1(value: any) {
        const {monitor} = this.props;
        this.props.monitor.setRelatedId(value);
    }

    @autobind
    private backWay(value: any) {
        const {monitor} = this.props;
        monitor.settest(value)
    }


    globalMap = () => {
        window.location.replace("/eureka/Main");
    }

    @autobind
    public async dateSelect(date: moment.Moment | null, selectDate: string) {
        const {dashboard,monitor} = this.props;
        dashboard.setDashLoading(true);
        monitor.setPickerDate(selectDate);

        if(sessionStorage.getItem('currentState')  == 'Factory'){
            console.log('여기작동');
            let parameter: GlobalParameter = {
                factoryId: 1009,
                date: selectDate,
                factoryType: 'Factory',
                userId:sessionStorage.getItem('loggedInfo')
            }

            try {
                const resultData: any = await Searvice.getPost("dash/selectFactorylMap.do", parameter);
                // monitor.setHirerachyName2(resultData.data.factoryInfo[0].hirerachyName);
                monitor.setFactory(resultData.data)
            } catch (e) {
                console.log(e);
            }

        } else if(sessionStorage.getItem('currentState')  == 'Building') {

            let parameter: GlobalParameter = {
                factoryId: 5006,
                date: selectDate,
                factoryType: 'Building',
                userId: sessionStorage.getItem('loggedInfo')
            }

            try {
                const resultData: any = await Searvice.getPost("dash/selectFactorylMap.do", parameter);
                // monitor.setHirerachyName2(resultData.data.factoryInfo[0].hirerachyName);
                monitor.setFactory2(resultData.data)
            } catch (e) {
                console.log(e);
            }

            dashboard.setDashLoading(false);

        }
    }
    render() {
        const {monitor,dashboard} = this.props;
        const dateFormat = 'YYYY-MM-DD';
        return (
            <>
                <PageHeader style={{backgroundColor: "white"}} title="">
                    <h2 style={{marginRight:5, float:"left"}} onClick={this.globalMap}><a>Monitoring</a></h2>
                    {monitor.getNaviMenu.map((store: any, index:number) => {
                        if(monitor.getNaviMenu.length == index + 1){
                            return <h3 onClick={() => this.backWay(store)} style={{float:"left", marginTop:6}}>><a style={{color:"black", textDecoration:"underline"}}> {store.name}</a></h3>
                        }else{
                        return <h3 onClick={() => this.backWay(store)} style={{float:"left", marginTop:6}}> ><a> {store.name}</a></h3>
                        }})}
                    <div style={{float:"right"}}>
                        <DatePicker value={moment(monitor.getPickerDate)} onChange={this.dateSelect} format={dateFormat}
                                    style={{marginRight: 4.9, width: 250}}
                        />
                    </div>
                </PageHeader>
                <Switch>
                    <Route path="/Main/Monitoring/Entire/:postid" render={this.onRenderEntireFactory}/>
                </Switch>
            </>
        );
    };
}


export default FactoryAll;
