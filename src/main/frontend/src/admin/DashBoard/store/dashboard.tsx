import {action, computed, observable} from 'mobx';
import React from "react";
import {Link} from "react-router-dom";
import StringUtile from "../../../utile/StringUtile";
import {FactoryMessage, GlobalKpi, MapLocation} from "../typescript/DashBoardType";
import {kpiInfo} from "../../monitoring/typescript/MonitorType";

class dashboard {


// ================================================================Global Map=====================================================================
    // ====================================GlobalMapap.tsx
    @observable pickerDate: string = StringUtile.formatDate();

    @action setPickerDate(param: string) {
        this.pickerDate = param;
    }

    @computed get getPickerDate() {
        return this.pickerDate;
    }

    // ===================================== 로딩셋팅
    @observable dashLoading: boolean = false;

    @action setDashLoading(param: boolean) {
        this.dashLoading = param;
    }

    @computed get getDashLoading() {
        return this.dashLoading;
    }


// 2.===================================== portletData 데이터 정보

    // 3.===================================== companyList 데이터 정보
    @observable companyList: any = [];

    @computed get getCompanyList() {
        return this.companyList;
    }

// 4.===================================== portletInfo 데이터 정보
    @observable portletInfo: Array<any> = [];

    @action setPortletInfo(parameter: any) {
        this.causeChart = parameter[2];
        this.makeupfailurechart = parameter[1];
        this.manufacfailurechart = parameter[0];
    }

    @computed get getPortletInfo() {
        return this.portletInfo;
    }

// ========================================
    // 조립불량 차트
    @observable manufacfailurechart: Array<any> = [];

    @computed get getManufacfailurechart() {
        return this.manufacfailurechart;
    }

// ========================================
    // 가공불량 차트
    @observable makeupfailurechart: Array<any> = [];

    @computed get getMakeupfailurechart() {
        return this.makeupfailurechart;
    }

// ========================================
// 가공/조립 불량 원인차트
    @observable causeChart: Array<any> = [];


    @computed get getCauseChart() {
        return this.causeChart;
    }

    //조립 불량 PPM
    @observable menuChartData: any = [];

    @computed get getMenuChartData() {
        return this.menuChartData;
    }

    //가공 불량 PPM
    @observable makeChartData: any = [];

    @computed get getMakeChartData() {
        return this.makeChartData;
    }

    //가공/조립 불량 WORST
    @observable causeChartData: any = [];

    @computed get getCauseChartData() {
        return this.causeChartData;
    }


    //popup 위치
    @observable facSpot: any = [];

    @action setFacSpot(param: any) {
        this.facSpot = param;
    }

    @computed get getFacSpot() {
        return this.facSpot;
    }


    //맵 로딩
    @observable mapLoading: boolean = false;

    @action setMapLoading(param: boolean) {
        this.mapLoading = param;
    }

    @computed get getMapLoading() {
        return this.mapLoading;
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
// MapLocation (전체공장)
    @observable mapLocation: Array<MapLocation> = [];

    @action setMapLocation(param: Array<MapLocation>) {
        this.mapLocation = param;
    }

    @computed get getMapLocation() {
        return this.mapLocation;
    }


// Factory Message Info(공장메세지)
    @observable companyMessage: Array<FactoryMessage> = [];

    @action setCompanyMessage(param: Array<FactoryMessage>) {
        this.companyMessage = param;
    }

    @computed get getCompanyMessage() {
        return this.companyMessage;
    }

// globalMap KPI (dashboard KPI)
    @action setDashboardKpi(param: GlobalKpi) {
        this.globalKpiInfo = param.kpiInfo;
        this.globalKpiData = param.globalKpiData;
        const globalKpiData = Object.entries(param.globalKpiData.reduce((p: any, c: any) => (p[c.kpiid] ? p[c.kpiid].push(c) : p[c.kpiid] = [c], p), {}))
        this.globalKpiData = globalKpiData;
    }

    @observable globalKpiInfo: Array<kpiInfo> = [];
    @observable globalKpiData: Array<any> = [];
    @computed get getGlobalKpiInfo() {
        return this.globalKpiInfo;
    }
    @computed get getTest() {
        return this.globalKpiData;
    }

    // visible
    @observable message: boolean = false;

    @action setMessage(param: boolean) {
        this.message = param;
    }

    @computed get getMessage() {
        return this.message;
    }

}

export default dashboard;