import Searvice from "../../../common/Searvice";
import {GlobalMapData, GlobalParam, GlobalParameter, MapLocation} from "./DashBoardType";
import StringUtile from "../../../utile/StringUtile";

class DashBoardService {

    public async getGlobalMapData(parameter:GlobalParameter){
        let resultData:MapLocation = {
            name: "",
            location: "",
            id:"",
            mapID: "",
            latitude: 0,
            modelID:"",
            longitude:0,
            companyID: "",
        } ;
        try {
            const resultData: any = await Searvice.getPost("dash/selectMapLocation.do", parameter);
            return resultData.data.mapLocation
        } catch (e) {
            console.log(e);
            return resultData
        }
    }

    public async factoryMessage(parameter:GlobalParameter){
        let resultData:MapLocation = {
            name: "",
            location: "",
            id:"",
            mapID: "",
            latitude: 0,
            modelID:"",
            longitude:0,
            companyID: "",
        };
        try {
            const resultData: any = await Searvice.getPost("dash/selectFactoryMessage.do", parameter);
            return resultData.data.factoryMessage
        } catch (e) {
            console.log(e);
            return resultData
        }
    }

    public async globalKpi(parameter:GlobalParameter){

        try {
            const resultData: any = await Searvice.getPost("dash/selectGlobalMap.do", parameter);
            return resultData.data
        } catch (e) {
            console.log(e);
        }
    }



    // dash/selectGlobalMap.do
}

export default DashBoardService;
