
export interface GlobalMapData{
    companyList:[],
    companyMessage:[],
    portletInfo:[],
    portletData:[]
}

export interface GlobalParam{
    companyid:string,
    date:string
}
export interface FactoryInfo{
    message?:string,
    id?:string,
    day?:string,
    time?:string,
    eventType?:string,
    factoryName:string,
    companyName?:string,
    relatedID?:string,
    companyID?:string,
    latitude:number,
    longitude:number,
    name?:string

}
export interface WeatherInfo{
    city:string,
    description:string,
    tempertaure:number,
    humidity:number,
    pressure:number,
}

export interface SpotChange extends WeatherInfo{
    latitude:number,
    longitude:number,
    zoom:number,
    factoryName?:string,
    modelId?:string,


}



// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
/**
 * @description 조회할때 필요한 파라미터인수
 */
export interface GlobalParameter{
    userId:any,
    date:string,
    factoryId?:number,
    factoryType?:string
}

/**
 * @ 전체 공장 좌표
 */
export interface MapLocation{
    name: string,
    location: string,
    id: string,
    mapID: string,
    latitude: number,
    modelID: string,
    longitude: number,
    companyID: string,
}

/**
 * @description 공장 메세지 정보
 */
export interface FactoryMessage{
    factoryID:string,
    factoryName:string,
    longitude:number,
    latitude:number,
    eventType:string,
    modelID:string,
    message:string,
    relatedID:string,
    day:string,
    time:string
}

/**
 * @description dashboard에서 보여질 kpi info,date 타입
 */
export interface GlobalKpi{
    kpiInfo : Array<any>
    globalKpiData :Array<any>
}

export const overlappingModes = ['stagger', 'rotate', 'hide', 'none'];