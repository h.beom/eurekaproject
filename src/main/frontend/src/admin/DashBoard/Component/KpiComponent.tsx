import React, {Component} from 'react';
import 'antd/dist/antd.css';
import Chart, {ArgumentAxis, Label, Series, Tooltip} from "devextreme-react/chart";
import PieChart, {Connector, Label as PieLabel, Legend, Series as PieSeries, Size} from "devextreme-react/pie-chart";
import {Card, Statistic,} from "antd";
import {inject, observer} from "mobx-react";
import {kpiData, kpiInfo} from "../../monitoring/typescript/MonitorType";


@inject('dashboard', 'monitor')
@observer
class KpiComponent extends Component<any, any> {


    componentDidMount() {

    }

    render() {

        const {dashboard} = this.props;
        return (
            <div style={{marginTop: 10, overflowY: "auto"}}>{dashboard.getGlobalKpiInfo.map((param: kpiInfo, index: number) => {
                return <> {dashboard.getTest.map((value: any, index: number) => {

                    if (param.kpiid === value[0]) {
                        return <Card key={index} title={param.kpiname} size={'small'}
                                     style={{height: 260, width: '33%', float: "left", overflowY: "auto"}}>


                            {
                                (() => {
                                    switch (param.uitype) {

                                        //막대기 차트일 경우
                                        case 'Bar':
                                            return <Chart id="chart" style={{height: 230}}
                                                          dataSource={value[1]}>
                                                <Series
                                                    valueField="value"
                                                    argumentField="name"
                                                    type={'bar'}
                                                />
                                                <ArgumentAxis>
                                                    <Label
                                                        wordWrap="none"
                                                        overlappingBehavior="rotate"
                                                    />
                                                </ArgumentAxis>
                                                <Legend visible={false}/>
                                                <Tooltip enabled={true} shared={true}/>
                                            </Chart>
                                            break;

                                        //꺽은쇠 차트일 경우
                                        case 'Line':
                                            return <Chart id="chart" style={{height: 230}}
                                                          dataSource={value[1]}>
                                                <Series
                                                    valueField="value"
                                                    argumentField="name"
                                                />
                                                <ArgumentAxis>
                                                    <Label
                                                        wordWrap="none"
                                                        overlappingBehavior="rotate"
                                                    />
                                                </ArgumentAxis>
                                                <Legend visible={false}/>
                                                <Tooltip enabled={true} shared={true}/>
                                            </Chart>
                                            break;

                                        //파이차트일 경우
                                        case 'Pie':
                                            return <span>  <PieChart dataSource={value[1]} style={{height: 190}}>
                                                        <PieSeries argumentField={'name'} valueField={'value'}>
                                                            <PieLabel visible={true}>
                                                                <Connector visible={true} width={1}/>
                                                            </PieLabel>
                                                        </PieSeries>
                                                        <Size/>
                                                         <Legend visible={true}/>
                                                    </PieChart>

                                                    <Tooltip enabled={true} shared={true}/>
                                                    </span>
                                            break;
                                        default :
                                            return value.flat().map((store: kpiData, index: number) => {

                                                if (index !== 0) {
                                                    return <span>
                                                      <div style={{fontSize:11, marginTop:15}}>  {store.text} </div>
                                                    <Statistic key={index}
                                                               style={{fontSize:7}}
                                                               value={store.value}
                                                               suffix={store.unit}
                                                    />
                                                    </span>
                                                }
                                            })
                                            break;
                                    }
                                })()
                            }

                        </Card>
                    }
                })}
                </>
            })}
            </div>
        );

    };
}


export default KpiComponent;
