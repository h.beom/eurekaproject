import {CSSProperties} from "react";
import {RouteComponentProps} from "react-router";

export interface ISimulationView{
    analysisTime: number,
    input: number,
    insDt: string,
    modelId: string,
    modelNm: string,
    ngCount: number,
    output: number,
    simReqId: string,
    simReqNm: string,
    simReqStNm: string,
    simReqTpNm: string,
    stdDeviation: string,
    userNm: string,
    warmUpTime: number
}

export interface ISimFacilityNgCntChartData{
    simReqId:string,
    processId:string,
    processNm:string,
    processSeq:number,
    okCount:number,
    totOkCount:number,
    avgNgRate:number,
    ngCount:number
}

export interface ISimulationResultView{
    simulationid:string,
    loading:boolean,
    facilityNgCntChartData : Array<ISimFacilityNgCntChartData>,
    viewData: ISimulationView,
    topMenuList:Array<ISimulationResultViewTopMenu>,
    contentList:Array<ISimulationResultViewTopMenu>,
    renderEvent:Function,
    selectionTabIdx:string
    fullscreenType: boolean;
}

export interface ISimulationResultViewTopMenu {
    url: string,
    text: React.ReactNode;
    icon?: string;
    style?: CSSProperties;
    selection?: boolean;
    render :React.ReactNode
}

export interface ISimulationResultContent{
    EventFullScreen:Function,
    visible?:boolean
}
