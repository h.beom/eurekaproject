import {
    IFactoryModelAttrData,
    IProcessAnimationInfo,
    IProcessMapInfo,
    IProcessModelInfo
} from "../../../threeView/IProcessModelMain";
import Searvice from "../../../common/Searvice";
import {ISimFacilityNgCntChartData, ISimulationView} from "../ISimulationResultView";
import {ISimulationChartParame, ITimeOutList} from "../store/SimulationType";
import {IHseAnimationInfo} from "../../../threeView/IHseModelMain";


class SimulationTapsService {

    public async getProcessModel(simulationid: string) {
        let parameter: { simulationid: string, loclid: string } = {simulationid: simulationid, loclid: "KO"};
        let mapData: Array<IProcessMapInfo> = [];
        let modelListData: Array<IProcessModelInfo> = [];
        let processAnimation: Array<IProcessAnimationInfo> = [];
        let processAnimationTotalTime: number = 0;

        try {
            const resultData: any = await Searvice.getPost("sim/getProcessModel.do", parameter);
            if (resultData != null) {
                mapData = resultData.data.mapData;
                modelListData = resultData.data.modelListData;
                modelListData = modelListData.map(function (value: IProcessModelInfo, index: number) {
                    value.stencLenz = (value.stencLenz == 0) ? 0.000001 : value.stencLenz;
                    value.stencPosz = (value.stencPosz == 0) ? 0.000001 : value.stencPosz;
                    return value;
                });
                processAnimation = resultData.data.processAnimation;
                processAnimationTotalTime = resultData.data.processAnimationTotalTime;

            }

            if (mapData == undefined || modelListData == undefined) {
                throw "Data Call Error";
            }

            return {
                mapData: mapData,
                modelListData: modelListData,
                processAnimation: processAnimation,
                processAnimationTotalTime: processAnimationTotalTime
            }
        } catch (e) {
            console.error(e);
            return {mapData: [], modelListData: [], processAnimation: [], processAnimationTotalTime: 0};
        }
    }

    public async getSimulationViewData(parameter:any) {
        let resultData: ISimulationView = {
            simReqId: "", modelNm: "", simReqNm: "", insDt: "", userNm: "", analysisTime: 0, warmUpTime: 0,
            simReqStNm: "", simReqTpNm: "", output: 0, ngCount: 0, modelId: "", stdDeviation: "", input: 0,
        };

        try {
            const searviceData: any = await Searvice.getPost("sim/getSimulationViewData.do", parameter);
            if (searviceData.data != null) {
                resultData = searviceData.data.viewData;
            }
            return resultData;
        } catch (e) {
            console.log(e);
            return resultData;
        }
    }


    public async getFacilityNgCntChartData(parameter:any) {
        let resultData: Array<ISimFacilityNgCntChartData> = [];

        try {
            const searviceData: any = await Searvice.getPost("sim/getFacilityNgCntChartData.do", parameter);
            if (searviceData.data != null) {
                resultData = searviceData.data.charData;
            }
        } catch (e) {
            console.log(e);
        }
        return resultData;
    }


    public async getTimeOutput(simulationid: string, loclid: string) {
        let parameter: { simulationid: string, loclid: string } = {simulationid: simulationid, loclid: loclid};
        try {
            const resultData2: any = await Searvice.getPost("sim/getTimeOutNg.do", parameter);
            const TimeOutDataNgData: Array<ITimeOutList> = resultData2.data.getTimeOutNg;
            return TimeOutDataNgData;
        } catch (e) {
            console.log(e, '캐치 이벤트 ');
            return [];
        }
    }

    public async getTimeBufferUser(parameter:any) {
        try {
            const resultData: any = await Searvice.getPost("sim/TimeBufferUsage.do", parameter);
            const charData: Array<ISimulationChartParame> = resultData.data.bufferData;
            return charData
        } catch (e) {
            console.log(e, '캐치 이벤트 ');
            return [];
        }
    }

    public async getFactoryModel(modelid?: string | null) {

        let parameter: { modelid: string } = {modelid: 'a76366ed-a1e8-4a06-870d-4dfb175e10e8'};
        if (modelid != null) {
            parameter.modelid = modelid;
        }
        let mapData: Array<IProcessMapInfo> = [];
        let modelListData: Array<IProcessModelInfo> = [];
        let modelAttrData: Array<IFactoryModelAttrData> = [];
        let hseAnimationData: { [index: string]: Array<IHseAnimationInfo> } = {};

        try {
            const resultData: any = await Searvice.getPost("sim/getFactoryModel.do", parameter);
            if (resultData != null) {
                mapData = resultData.data.mapData;
                modelListData = resultData.data.modelListData;
                modelListData = modelListData.map(function (value: IProcessModelInfo, index: number) {
                    value.stencLenz = (value.stencLenz == 0) ? 0.000001 : value.stencLenz;
                    value.stencPosz = (value.stencPosz == 0) ? 0.000001 : value.stencPosz;
                    return value;
                });
                modelAttrData = resultData.data.modelAttrData;

                if (parameter.modelid == "a76366ed-a1e8-4a06-870d-4dfb175e10e8") {
                    hseAnimationData = {
                        "33aad36a-00fe-4d74-90ba-e08914bf5754": [
                            {time: 0, x: 900, y: 1000, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 1500, x: 1300, y: 1000, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 1300, y: 1000, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 3000, x: 1300, y: 500, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 1300, y: 500, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 3000, x: 1300, y: 500, z: 0, rotationY: 0, scissorSize: 250},
                            {time: 3000, x: 1300, y: 500, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 3000, x: 900, y: 500, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 900, y: 500, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 3000, x: 900, y: 1000, z: 0, rotationY: 90, scissorSize: 10},
                        ],
                        "1fa07933-bc28-43e9-98a5-26517cbf182f": [
                            {time: 0, x: 300, y: 1100, z: 0, rotationY: 0, scissorSize: 250},
                            {time: 3000, x: 300, y: 1100, z: 0, rotationY: 0, scissorSize: 10},

                            {time: 500, x: 0, y: 1100, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 0, y: 1100, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 3000, x: 0, y: 1800, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 0, y: 1800, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 3000, x: 1000, y: 1800, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 1000, y: 1800, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 1500, x: 1000, y: 1300, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 1000, y: 1300, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 3000, x: 300, y: 1300, z: 0, rotationY: 0, scissorSize: 10},

                            {time: 0, x: 300, y: 1300, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 500, x: 300, y: 1100, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 300, y: 1100, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 3000, x: 300, y: 1100, z: 0, rotationY: 0, scissorSize: 250},
                        ],
                        "6f04ee18-5125-47ad-b6d1-4b52b8220975": [
                            {time: 0, x: 1750, y: 750, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 3000, x: 1750, y: 1800, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 1750, y: 1800, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 500, x: 1450, y: 1800, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 1450, y: 1800, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 3000, x: 1450, y: 1800, z: 0, rotationY: 90, scissorSize: 250},
                            {time: 3000, x: 1450, y: 1800, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 3000, x: 1450, y: 750, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 1450, y: 750, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 500, x: 1750, y: 750, z: 0, rotationY: 0, scissorSize: 10},
                        ],
                        "ae9fa00b-a7eb-4c2f-8293-7911807bde7d": [
                            {time: 0, x: 300, y: 700, z: 0, rotationY: 0, scissorSize: 250},
                            {time: 3000, x: 300, y: 700, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 500, x: 0, y: 700, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 0, y: 700, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 1000, x: 0, y: 200, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 0, y: 200, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 2000, x: 900, y: 200, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 0, x: 900, y: 200, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 1000, x: 900, y: 700, z: 0, rotationY: 90, scissorSize: 10},
                            {time: 0, x: 900, y: 700, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 1500, x: 300, y: 700, z: 0, rotationY: 0, scissorSize: 10},
                            {time: 3000, x: 300, y: 700, z: 0, rotationY: 0, scissorSize: 250},
                            {time: 3000, x: 300, y: 700, z: 0, rotationY: 0, scissorSize: 250},
                        ],

                        //작업자
                        "6ab9961c-f807-4f3d-b888-47579d1e70b5": [
                            {time: 0, x: 1300, y: 1800, z: 0, rotationY: 90},
                            {time: 10000, x: 100, y: 1800, z: 0, rotationY: 90},
                            {time: 0, x: 100, y: 1800, z: 0, rotationY: 270},
                            {time: 10000, x: 1300, y: 1800, z: 0, rotationY: 270},
                        ],

                        "19aa3771-99c3-466d-9124-0469aee36a6d": [
                            {time: 0, x: 300, y: 1500, z: 0, rotationY: 180},
                            {time: 10000, x: 300, y: 500, z: 0, rotationY: 180},
                            {time: 0, x: 300, y: 500, z: 0, rotationY: 0},
                            {time: 10000, x: 300, y: 1500, z: 0, rotationY: 0},
                        ],

                        "f9fe4f95-7874-4d17-bb87-7745ca61e1f5": [
                            {time: 0, x: 500, y: 100, z: 0, rotationY: 0},
                            {time: 10000, x: 500, y: 1800, z: 0, rotationY: 0},
                            {time: 0, x: 500, y: 1800, z: 0, rotationY: 180},
                            {time: 10000, x: 500, y: 100, z: 0, rotationY: 180},
                        ],
                        "28fba9a2-af5b-4e49-80d8-0cd909e0937f": [
                            {time: 0, x: 800, y: 700, z: 0, rotationY: 90},
                            {time: 5000, x: 0, y: 700, z: 0, rotationY: 90},
                            {time: 0, x: 0, y: 700, z: 0, rotationY: 270},
                            {time: 5000, x: 800, y: 700, z: 0, rotationY: 270},
                        ],

                        "ea660bcf-4ef4-49a8-82a4-f7d940fe884f": [
                            {time: 0, x: 1500, y: 900, z: 0, rotationY: 90},
                            {time: 10000, x: 0, y: 900, z: 0, rotationY: 90},
                            {time: 0, x: 0, y: 900, z: 0, rotationY: 270},
                            {time: 10000, x: 1500, y: 900, z: 0, rotationY: 270},
                        ],

                        "5e6334a1-bd4a-45c6-89d1-35ad6f333c6e": [
                            {time: 0, x: 1000, y: 600, z: 0, rotationY: 270},
                            {time: 5000, x: 1500, y: 600, z: 0, rotationY: 270},
                            {time: 0, x: 1500, y: 600, z: 0, rotationY: 90},
                            {time: 5000, x: 1000, y: 600, z: 0, rotationY: 90},
                        ],

                        "36aad24a-5645-4b5a-a595-f5c72243cc1c": [
                            {time: 0, x: 1000, y: 1600, z: 0, rotationY: 180},
                            {time: 10000, x: 1000, y: 0, z: 0, rotationY: 180},
                            {time: 0, x: 1000, y: 0, z: 0, rotationY: 0},
                            {time: 10000, x: 1000, y: 1600, z: 0, rotationY: 0},
                        ],

                        "41b734c9-cb7c-4248-8711-f468dfd097da": [
                            {time: 0, x: 900, y: 1300, z: 0, rotationY: 0},
                            {time: 7000, x: 900, y: 0, z: 0, rotationY: 0},
                            {time: 0, x: 900, y: 0, z: 0, rotationY: 180},
                            {time: 7000, x: 900, y: 1300, z: 0, rotationY: 180},
                        ],
                        "dfacb015-9489-46ee-8f0c-462a983c1729": [
                            {time: 0, x: 300, y: 800, z: 0, rotationY: 270, rotationZ: 180},
                            {time: 1000, x: 500, y: 800, z: 0, rotationY: 270, rotationZ: 180},
                            {time: 0, x: 500, y: 800, z: 0, rotationY: 270, rotationZ: 40},
                            {time: 1000, x: 500, y: 800, z: -450, rotationY: 270, rotationZ: 40},
                            {time: 0, x: 500, y: 800, z: -450, rotationY: 270, rotationZ: 90},
                            {time: 10000, x: 500, y: 800, z: -450, rotationY: 270, rotationZ: 90},
                        ],
                        "9db3b3ff-f035-45d5-a5e7-dcc88711b51f": [
                            {time: 0, x: 900, y: 100, z: 0, rotationY: 90},
                            {time: 5000, x: 500, y: 100, z: 0, rotationY: 90},
                            {time: 0, x: 500, y: 100, z: 0, rotationY: 270},
                            {time: 5000, x: 900, y: 100, z: 0, rotationY: 270},
                        ],


                    }
                }

                // processAnimation = resultData.data.processAnimation;
            }

            if (mapData == undefined || modelListData == undefined) {
                throw "Data Call Error";
            }

            return {
                mapData: mapData,
                modelListData: modelListData,
                modelAttrData: modelAttrData,
                hseAnimationData: hseAnimationData
            }
        } catch (e) {
            console.error(e);
            return {mapData: [], modelListData: [], modelAttrData: [], hseAnimationData: []};
        }
    }

}

export default SimulationTapsService;
