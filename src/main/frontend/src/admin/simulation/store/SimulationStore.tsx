import {action, computed, observable, runInAction} from 'mobx';
import {ColumnProps} from "antd/es/table";
import {
    FacilityOperatingRateType,
    FacilityOutputItemType,
    IFacilityOperatingDataSource,
    ISimFacilityNgCntChartData,
    ISimulationChartParame,
    ITimeOutList,
    SimulationFormType,
    SimulationListBtnType, SimulationMenuType,
    SimulationTreeKey,
    SimulationType
} from "./SimulationType";
import {RadioChangeEvent} from "antd/es/radio";
import StringUtile from "../../../utile/StringUtile";
import moment from 'moment';
import Searvice from "../../../common/Searvice";
import {message} from "antd";
import SimulationTapsService from "../Service/SimulationTapsService";
import {ISimulationView} from "../ISimulationResultView";
import DataSourceHandler from "../../../utile/DataSourceHandler";
import {SmulationList} from "../typescript/SimulationType";

class SimulationStore {

    @observable test12: boolean = false;
    @action setTest12(parameter: boolean) {
        this.test12 = parameter;
    }
    @computed get getTest12() {
        return this.test12;
    }

    // ===================================
// all
// all
// all
// all
// all

@action deleteSimulationDataStore(param:string) {
    let substring = "/SimulationPage/";
    const result = param.indexOf(substring);
    if(result === -1){
        this.setFacilityNgCntChartData(null);
        this.setTimeBufferUserData(null);
        this.resetSimulationId('');
        this.setFacilityOperatingRate(null);
        this.setTimeOutchartData(null);
        this.setProcessList([]);
    }
}


    /**
     * @description 초기 ProcessId 전체를 보관하기위한 store
     */
    @observable productCheckData:any = null;
    @action setProductCheckData(parameter:any) {
        this.productCheckData = parameter;
    }
    @computed get getProductCheckData() {
        return this.productCheckData;
    }





//DashBoard
//DashBoard
//DashBoard
//DashBoard
//DashBoard

    /**
     * @description 설비별 생산량/불량건수 Store
     */
    @observable facilityNgCnt: any = null;
    @action setFacilityNgCntChartData(parameter: any) {
        this.facilityNgCnt = parameter;
    }
    @computed get getFacilityNgCntChartData() {
        return this.facilityNgCnt;
    }

    /**
     * @description 설비별 생산량/불량건수를 제외한 나머지 DASHBOARD Store
     */
    @observable dashboardChartData:ISimulationView = {
        analysisTime: 0,
        input: 0,
        insDt: '',
        modelId: '',
        modelNm: '',
        ngCount: 0,
        output: 0,
        simReqId: '',
        simReqNm: '',
        simReqStNm: '',
        simReqTpNm: '',
        stdDeviation: '',
        userNm: '',
        warmUpTime: 0
    };
    @action setDashboardChartData(parameter: any) {
        this.dashboardChartData = parameter;
    }
    @action resetSimulationId(parameter: any) {
        this.dashboardChartData.simReqId = parameter;
    }
    @computed get getDashboardChartData() {
        return this.dashboardChartData;
    }


    /**
     * @description 설비 Id, Nm을 맵핑(차트 밑에 설비 이름을 출력하기위한)
     */
    @observable chartItemName: { [index: string]: string } = {};
    @action setChartItemName(parameter: { [index: string]: string }) {
        this.chartItemName = parameter;
    }
    @computed get getChartItemName() {
        return this.chartItemName;
    }

    /**
     * @description 설비별 생산량/불량건수 차트 로딩위한 변수
     */
    @observable facilityChartLoading:boolean = true;

    @action setFacilityChartLoading(param:boolean) {

        this.facilityChartLoading = param;
    }
    @computed get getFacilityChartLoading() {
        return this.facilityChartLoading;
    }


    /**
     * @description DASHBOARD에서 설비를 제외한 모든 차트 로딩을 위한 변수
     */
    @observable dashboardChartLoading:boolean = true;

    @action setDashboardChartLoading(param:boolean) {

        this.dashboardChartLoading = param;
    }
    @computed get getDashboardChartLoading() {
        return this.dashboardChartLoading;
    }






// ============================
//TimeOutput
//TimeOutput
//TimeOutput
//TimeOutput
//TimeOutput

    /**
     * @description TIMEOUTPUT DATA store
     */
    @observable timeOutchartData: any = null

    @action setTimeOutchartData(parameter: any) {
        this.timeOutchartData = parameter;
    }

    @computed get getTimeOutchartData() {
        return this.timeOutchartData;
    }



    /**
     * @description TIMEOUTPUT에서 차트 로딩을 위한 변수
     */
    @observable timeOutputLoading:boolean = true;

    @action setTimeOutputLoading(param:boolean) {

        this.timeOutputLoading = param;
    }
    @computed get getTimeOutputLoading() {
        return this.timeOutputLoading;
    }

    // =============================================
    /**
     * @description  불량품 O퍼센테이지 그림에 들어갈 Data
     */
    @observable ngCountPercent: number = 0;
    @action setNgCountPercent = (parame: number) => {
        this.ngCountPercent = parame;
    };

    @computed get getNgCountPercent() {
        return this.ngCountPercent;
    };

    @observable facilityOutputNgCountData: Array<ISimFacilityNgCntChartData> = [];
    @action setFacilityOutputNgCountData = (parame: Array<ISimFacilityNgCntChartData>) => {
        this.facilityOutputNgCountData = parame;
    };

    @computed get getFacilityOutputNgCountData() {
        return this.facilityOutputNgCountData;
    };


    /**
     * @description  생산량 O퍼센테이지 그림에 들어갈 Data
     */
    @observable outPutPercent: number = 0;
    @action setOutPutPercent = (parame: number) => {
        this.outPutPercent = parame;
    };

    @computed get getOutPutPercent() {
        return this.outPutPercent;
    };


    /**
     * @description 해당 시뮬레이션에 대한 정보Data
     */
    @observable viewdata: ISimulationView = {
        analysisTime: 0,
        input: 0,
        insDt: '',
        modelId: '',
        modelNm: '',
        ngCount: 0,
        output: 0,
        simReqId: '',
        simReqNm: '',
        simReqStNm: '',
        simReqTpNm: '',
        stdDeviation: '',
        userNm: '',
        warmUpTime: 0
    };

    @action setViewdata = (parame: ISimulationView) => {
        this.viewdata = parame;
    };

    @computed get getViewdata() {
        return this.viewdata;
    };


    @action dashBoardSimInfo = (viewdata: ISimulationView, facilityOutputNgCountData: Array<ISimFacilityNgCntChartData>) => {
        this.viewdata = viewdata;
        this.facilityOutputNgCountData = facilityOutputNgCountData;
    };


    // timeBufferUserData
    @observable timeBufferUserData: any = null;
    @action setTimeBufferUserData = (parame:any) => {
        this.timeBufferUserData = parame;
    };

    @computed get getTimeBufferUserData() {
        return this.timeBufferUserData;
    };


    // timeOutData
    @observable timeOutData: Array<ITimeOutList> = [];
    @action setTimeOutData = (parame: Array<ITimeOutList>) => {
        this.timeOutData = parame;
    };

    @computed get getTimeOutData() {
        return this.timeOutData;
    };



    // @action
    // async setDashBoardData(simReqId:string){
    //     const service:SimulationTapsService = new SimulationTapsService();
    //     this.timeOutData = await service.getTimeOutput( simReqId , "KO");
    //     console.log('timeOutDataTest'+this.timeOutData);
    //     this.timeBufferUserData = await service.getTimeBufferUser( simReqId , "KO");
    //     console.log('timeBufferUserDataTest'+this.timeBufferUserData);
    // }


    /**
     * @description 대시보드에서 열기위한 시뮬레이션 요청아이디
     */
    @observable simReqId: string = "";

    /**
     * @description 리스트에서 선택한 해당 시뮬레이션의 요청아이디(simReqId)를 mobxStore에 저장
     * @param parameter 시뮬레이션 요청아이디
     */
    @action setSimReqId(parameter: string) {
        this.simReqId = parameter;
    }

    /**
     * @description mobxStore에 저장된 시뮬레이션 요청아이디를 반환한다.
     * @returns 시뮬레이션 요청아이디
     */
    @computed get getSimReqId() {
        return this.simReqId;
    }


    @action onReset() {
        this.search = "";
        this.userName = "";
        this.fromDate = StringUtile.setDay(StringUtile.formatDate(), -7);
        this.toDate = StringUtile.formatDate();
        this.selectionBtn = SimulationListBtnType.WEEK;
        this.simulationType = SimulationType.ALL;
    }

    @observable fromDate: any = StringUtile.setDay(StringUtile.formatDate(), -7);

    @action setFromDate(date: moment.Moment | null, dateString: string) {
        this.fromDate = date;
    }

    @computed get getFromDate() {
        return this.fromDate;
    }

    @observable selectionBtn: string = SimulationListBtnType.WEEK;

    @computed get getSelectionBtn() {
        return this.selectionBtn;
    }

    @observable toDate: any = StringUtile.formatDate();

    @action setToDate(date: moment.Moment | null, dateString: string) {
        this.toDate = date;
    }

    @computed get getToDate() {
        return this.toDate;
    }


    //Data 버튼 클릭시 두 시간간의 차이
    @observable returnMessage: boolean = false;

    @computed get getReturnMessage() {
        return this.returnMessage;
    }

    @action setSelectionBtn(event: RadioChangeEvent) {
        let todate: string = this.toDate;
        let patt: RegExp = new RegExp("^(19|20)\\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$");
        if (!patt.test(todate)) {
            this.returnMessage = true;
        }
        switch (event.target.value) {
            case SimulationListBtnType.WEEK :
                this.fromDate = StringUtile.setDay(todate, -7);
                this.selectionBtn = SimulationListBtnType.WEEK;
                break;
            case SimulationListBtnType.ONEMONTH :
                this.fromDate = StringUtile.setMonth(todate, -1);
                this.selectionBtn = SimulationListBtnType.ONEMONTH;
                break;
            case SimulationListBtnType.THREEMONTH :
                this.fromDate = StringUtile.setMonth(todate, -3);
                this.selectionBtn = SimulationListBtnType.THREEMONTH;
                break;
            case SimulationListBtnType.YEAR :
                this.fromDate = StringUtile.setYear(todate, -1);
                this.selectionBtn = SimulationListBtnType.YEAR;
                break;
        }
    }


    @observable search: string = '';

    @action setSearch(parameter: string) {
        this.search = parameter;
    }

    @computed get getSearch() {
        return this.search;
    }

    @observable userName: string = '';

    @action setUserName(parameter: string) {
        this.userName = parameter;
    }

    @computed get getUserName() {
        return this.userName;
    }

    @observable simulationType: SimulationType = SimulationType.ALL;

    @action setSimulationType(paramter: SimulationType) {
        return this.simulationType = paramter;
    }

    @computed get getSimulationType() {
        return this.simulationType;
    }

    @observable gridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [{
        title: '모델명',
        dataIndex: 'modelNm',
        key: 'modelNm',
        width: '280px'
    }, {
        title: '시뮬레이션 명',
        dataIndex: 'simReqNm',
        key: 'simReqNm',
        // render: function(text: any, record:any){
        //     const item : ISimulationListDataSource = record;
        //     return <Link to={"/simulationView/" }>{text}</Link>
        // }
    }, {
        title: '요청일',
        dataIndex: 'insDt',
        key: 'insDt',
        align: "center",
        width: '200px'
    }, {
        title: '시뮬레이션 상태',
        dataIndex: 'simReqStNm',
        key: 'simReqStNm',
        align: "center",
        width: '200px'
    }, {
        title: '시뮬레이션 요청자',
        dataIndex: 'userNm',
        key: 'userNm',
        align: "center",
        width: '200px'
    }, {
        title: '시뮬레이션 요청 종류',
        dataIndex: 'simReqTpNm',
        key: 'simReqTpNm',
        align: "center",
        width: '200px'
    }];


    /**
     * @description 시뮬레이션 리스트 데이터를 담는다.
     */
    @observable simulationList:Array<SmulationList>= [];

    /**
     * @description 시뮬레이션 리스트 데이터값 반환
     */
    @computed get getSimulationList() {
        return this.simulationList;
    }

    // @action set_dataGridCoulumn = () => {
    // };

    @computed get getGridCoulumn() {
        return this.gridCoulumn;
    }

    /**
     * @description 파라미터들을 보내기위한 서비스(검색조건)
     */
    @computed get sendParameter(): any {
        let parameter: any = {
            search: this.search,
            username: this.userName,
            fromdate: this.fromDate,
            todate: this.toDate,
            loclid: "KO",
            simulationtype: this.simulationType
        };
        return parameter;
    }

// ==============================================

    /**
     * @description 데이터 출력시 영어를 -> 한글로 바꾸어줌
     */
    @action
    async printList() {
        let parameter = this.sendParameter;
        try {
            const resultData: any = await Searvice.getPost("sim/getSimulationListData.do", parameter);

            const list:Array<SmulationList> = resultData.data.list;
            list.map(function (value: any) {
                if (value.simReqStNm == "Simulation Running") {
                    value.simReqStNm = "시뮬레이션 진행"
                    return value;
                } else if (value.simReqStNm == "Simulation Request") {
                    value.simReqStNm = "시뮬레이션 요청"
                    return value;
                } else if (value.simReqStNm == "Simulation Ng") {
                    value.simReqStNm = "시뮬레이션 NG"
                    return value;
                } else if (value.simReqStNm == "Simulation Complete") {
                    value.simReqStNm = "시뮬레이션 완료"
                    return value;
                }
            });
            this.simulationList = list;
        } catch (error) {
            message.error('데이터 불러오기에 실패하였습니다.', 5);
            return null;
        }
    }

    // =============================================================================

    @observable processList: Array<any> = [];
    @action setProcessList = (parameter: Array<any>) => {
        this.processList = parameter;
    };

    @computed get getProcessList() {
        return this.processList;
    }

    @observable processChartList: Array<any> = [];

    @action setProcessChartList = (parameter: any) => {
        this.processChartList = parameter;
    };

    @computed get getProcessChartList() {
        return this.processChartList;
    }

    @observable processModelData: any = {
        mapData: [],
        modelListData: [],
        processAnimation: [],
        processAnimationTotalTime:''
    };

    @observable processModel: any = [];
    @action setProcessModelData = (parameter: any) => {
        this.processModel = parameter;
    };

    @computed get getProcessModelData() {
        return this.processModel;
    }

    @observable treeList: any = [{
        simReqId: "",
        processId: "",
        processNm: "",
        processSeq: 0,
        ngCount: 0,
        okCount: 0,
        totOkCount: 0,
        avgNgRate: 0
    }];

    @action setTreeList(parameter: any) {
        this.treeList = parameter;
    }

    @computed get getTreeList() {
        return this.treeList;
    }

    /**
     * @description 제품별생산량에서 보여질 차트 데이터 변수입니다.
     */
    @observable chartData: any = [{
        simReqId: "",
        processId: "",
        processNm: "",
        processSeq: 0,
        ngCount: 0,
        okCount: 0,
        totOkCount: 0,
        avgNgRate: 0
    }];

    @action setChartData(parameter: any) {
        this.chartData = parameter;
    }

    @computed get getChartData() {
        return this.chartData;
    }

    /**
     * @description 차트데이터 (정렬속 재정렬을 하기위한 데이터)
     */
    @observable orgChartData: Array<any> = [];

    @action setOrgChartData(parameter: any) {
        this.orgChartData = parameter;
    }

    @computed get getOrgChartData() {
        return this.orgChartData;
    }






    @observable simReqNm: string = '';

    @action setSimReqNm = (parame: string) => {
        this.simReqNm = parame;
    };

    @computed get getSimReqNm() {
        return this.simReqNm;
    };


    /**
     *  @description 선택된 (정렬구분값)sortName에 따라 정렬을 바꿔주는 Method
     *  @returns 선택된 정렬값으로 인해 재정렬된 []값을 반환한다.
     */
    @computed
    private get commonlogic() {
        let newPostList: Array<any> = [];
    if(this.timeBufferUserData !== null){
        switch (this.sortName) {
            case SimulationTreeKey.BUF :
                newPostList = this.timeBufferUserData.sort((a: any, b: any) => {
                    return b.conveyorCountForHour - a.conveyorCountForHour;
                });
                break;
            case SimulationTreeKey.DEBUF :
                newPostList = this.timeBufferUserData.sort((a: any, b: any) => {
                    return a.conveyorCountForHour - b.conveyorCountForHour;
                });
                break;
            case SimulationTreeKey.ASC :
                newPostList = this.timeBufferUserData.sort((a: any, b: any) => {
                    return StringUtile.getFullTextSort(a.simLoadingTimeHour, b.simLoadingTimeHour);
                });
                break;
            case SimulationTreeKey.DESC :
                newPostList = this.timeBufferUserData.sort((a: any, b: any) => {
                    return StringUtile.getFullTextSort(b.simLoadingTimeHour, a.simLoadingTimeHour);
                });
                break;
        }
        return newPostList;
    }
    }

    @observable sortName: SimulationTreeKey = SimulationTreeKey.BUF;

    @action setSortName(parameter: SimulationTreeKey) {
        this.sortName = parameter;
    }

    @computed get getSortName() {
        return this.sortName;
    }




    /** @description 차트 image저장을 위한 변수  */
    @observable chartPngDownloadData: string = "";

    @action setChartPng(parameter: string) {
        this.chartPngDownloadData = parameter;
    }

    @computed get getChartPng() {
        return this.chartPngDownloadData;
    }


    @observable tableData: [] = [];

    /**
     * @description
     * @param parameter차트의 데이터를 테이블(표)로 보려고 할때 데이터를 담을 변수
     */
    @action setTableData(parameter: []) {
        this.tableData = parameter;
    }

    @computed get getTableData() {
        return this.tableData;
    }

    /**
     * @description chart(그림) or table(표) 상태를 보여주기 위한 설정 변수
     */
    @observable selectionView: SimulationFormType = SimulationFormType.CHART;

    @action setSelectionView(parameter: SimulationFormType) {
        this.selectionView = parameter;
    }

    @computed get getSelectionView() {
        return this.selectionView;
    }

    /**
     * @description 선택한 정렬옵션을 적용하는 메서드
     * @param value 선택한 정렬옵현 (NAME | ASC | DESC)
     */
    @action
    public async setAlign(value: string) {
        let chartData: Array<any> = DataSourceHandler.chartDataSort(this.chartData, value, this.countType, this.textSortAttrName);
        this.chartData = chartData;
        this.productOutputSortname = value;

    }

    /**
     * @description 선택한 정렬옵션을 적용하는 메서드
     * @param value 선택한 정렬옵현 (NAME | ASC | DESC)
     */
    @action
    public async setAlign2(value?: string) {
        let chartData: Array<any> = DataSourceHandler.chartDataSort(this.chartData, this.facilitySortType, this.countType, this.textSortAttrName);
        this.chartData = chartData;
        this.facilityOutputSortname = value;
    }


    /** @description 정렬속성이름 */
    @observable textSortAttrName: string = "processNm";

    /**
     * @description mobxStore의 정렬속성이름을 변경하는 method
     * @param parameter 정렬속성이름
     */
    @action setTextSortAttrName(parameter: string) {
        this.textSortAttrName = parameter;
    }

    /**
     * * @returns mobxStore의 정렬속성이름을 반환
     */
    @computed get getTextSortAttrName() {
        return this.textSortAttrName;
    }

    /**
     * @description 제품선택시 체크한 제품만 출력하기 위한 메서드
     * @param checkedKeys 체크된 해당제품
     */
    @action
    public setCheck(checkedKeys: any) {
        this.setCheckedKeys(checkedKeys);
        let list: Array<ISimFacilityNgCntChartData> = Object.assign([], this.orgChartData);
        let listFilter: Array<ISimFacilityNgCntChartData> = list.filter(function (value: ISimFacilityNgCntChartData) {
            return checkedKeys.indexOf(value.processId) > -1;
        });
        let chartData: Array<any> = DataSourceHandler.chartDataSort(listFilter, this.sortType, FacilityOutputItemType.OK_COUNT, this.textSortAttrName);
        this.chartData = chartData;
        this.setAlign(this.productOutputSortname);
    }

    /**
     * @description 정렬에 대한 타입변수 입니다.
     */
    @observable sortType: SimulationTreeKey = SimulationTreeKey.TIME;

    /**
     * @description 선택된 정렬타입을 mobx변수에 저장합니다.
     * @param parameter 선택된 정렬타입에 대한 변수
     */
    @action setSortType(parameter: SimulationTreeKey) {
        this.sortType = parameter;
    }

    /**
     * @description mobx에 저장된 정렬타입을 저장합니다.
     * @returns 저장된 정렬타입
     */
    @computed get getSortType() {
        return this.sortType;
    }

    /**
     * @description 시간별사용량 chartData 가지고오기
     */
    @action
    public async getProductOutput() {
        let parameter: any = {simulationid: this.simReqId, loclid: "KO"};
        try {
            let chartItemName: { [index: string]: string } = {};
            const resultData: any = await Searvice.getPost("sim/getFacilityNgCntChartData.do", parameter);
            const charData: Array<ISimulationChartParame> = resultData.data.charData.sort(function (leftItem: ISimFacilityNgCntChartData, rightItem: ISimFacilityNgCntChartData) {
                return StringUtile.getFullTextSort(leftItem.processNm, rightItem.processNm);
            });
            const treeData: Array<ISimulationChartParame> = resultData.data.charData.sort((leftItem: ISimFacilityNgCntChartData, rightItem: ISimFacilityNgCntChartData) => {
                return StringUtile.getFullTextSort(leftItem.processNm, rightItem.processNm);
            });
            resultData.data.charData.forEach(function (value: ISimFacilityNgCntChartData) {
                chartItemName[value.processId] = value.processNm;
            });

            runInAction(() => {
                this.sortType = SimulationTreeKey.NAME;
                this.chartData = charData;
                this.orgChartData = charData;
                this.chartItemName = chartItemName;
                this.treeList = treeData;
            });
        } catch (e) {
            console.log(e, '캐치 이벤트 ');
        }
    }

    /**
     * @description 정렬 선택시 부가정렬 보임 | 숨김 변수
     */
    @observable alignOptionBtnVisible: "hidden" | "visible" = "hidden";

    /**
     * 정렬 선택시 부가정렬할 설정을 저장
     * @param parameter 보임 | 숨김을 결정하는 파라미터
     */
    @action setAlignOptionBtnVisible(parameter: "hidden" | "visible") {
        this.alignOptionBtnVisible = parameter;
    }

    /**
     * @returns * 정렬 선택시 부가정렬한 상태(보임 | 숨김)를 반환
     */
    @computed get getAlignOptionBtnVisible() {
        return this.alignOptionBtnVisible;
    }

    /**
     * @description 설비별가용률 데이터가 시작할때 해당데이터를 조회하여
     * 정렬, 상태순서 등 옵션의 디폴트값으로 초기화면을 구성하는 메서드
     */
    @action
    async setFacData() {
        if (this.simReqId != "") {
            try {
                const chartData: Array<IFacilityOperatingDataSource> = this.facilityOperatingRate;

                let resultData: Array<any> = DataSourceHandler.chartDataSort(chartData, SimulationTreeKey.NAME, this.facilitySortName, "processNm");
                let chartItemName: { [index: string]: string } = {};
                chartData.forEach(function (value) {
                    chartItemName[value.processId] = value.processNm;
                });
                this.sortType = SimulationTreeKey.NAME;

                this.facilityOperatingRate = resultData;
                resultData.map(function(value){
                    value.sum = value.working + value.waiting + value.blocking + value.failure
                });

                this.chartItemName = chartItemName;
                this.orgChartData = resultData;
            } catch (e) {
                console.log(e, '캐치 이벤트 ');
                return null;
            }
        }
    }


//FacilityOperatingRate
//FacilityOperatingRate
//FacilityOperatingRate
//FacilityOperatingRate
//FacilityOperatingRate
    @observable facilityOperatingRate: any = null;

    /**
     * @description 설비용가용률(facilityOperatingRate) chartData을 mobxStore에 저장
     * @param parameter 설비용가용률(facilityOperatingRate) chartData
     */
    @action setFacilityOperatingRate(parameter: any) {
        this.facilityOperatingRate = parameter;
    }

    /**
     * @description mobxStore에 저장된 설비용가용률(facilityOperatingRate) Data를 반환
     * @returns mobxStore에 저장된 설비용가용률(facilityOperatingRate) Data를 반환
     */
    @computed get getFacilityOperatingRate() {
        return this.facilityOperatingRate;
    }


    /**
     * @description 설비별 가용률 차트 로딩위한 변수
     */
    @observable facilityOperatingChartLoading:boolean = true;

    @action setFacilityOperatingChartLoading(param:boolean) {

        this.facilityOperatingChartLoading = param;
    }
    @computed get getFacilityOperatingChartLoading() {
        return this.facilityOperatingChartLoading;
    }



//=============================================
    @observable facilitySortName: FacilityOperatingRateType = FacilityOperatingRateType.SUM;

    @action setFacilitySortName(parameter: FacilityOperatingRateType) {
        this.facilitySortName = parameter;
    }

    @computed get getFacilitySortName() {
        return this.facilitySortName;
    }




    @observable facilityOutputSortName: FacilityOutputItemType = FacilityOutputItemType.OK_COUNT;

    @action setFacilityOutputSortName(parameter: FacilityOutputItemType) {
        this.facilityOutputSortName = parameter;
    }

    @computed get getFacilityOutputSortName() {
        return this.facilityOutputSortName;
    }



    /**
     * @description 페이징값 변수
     */
    @observable listPaging: number = 10;
    /**
     * @description 페이징값 변수 set
     */
    @action setListPaging(parameter: number) {
        this.listPaging = parameter;
    }

    /**
     * @description 페이징값 변수 return
     * @return 페이징값
     */
    @computed get getListPaging() {
        return this.listPaging;
    }




    @observable checkedKeys: any = ["all"];

    @action setCheckedKeys(parameter: any) {
        this.checkedKeys = parameter;
    }

    @computed get getCheckedKeys() {
        return this.checkedKeys;
    }
    // =================================================================================================================

    /**
     * @description chart(그림) or table(표) 상태를 보여주기 위한 설정 변수
     */
    @observable timeOutputSelectionView: SimulationFormType = SimulationFormType.CHART;

    @action setTimeOutputSelectionView(parameter: SimulationFormType) {
        this.timeOutputSelectionView = parameter;
    }

    @computed get getTimeOutputSelectionView() {
        return this.timeOutputSelectionView;
    }
    /**
     * @description chart(그림) or table(표) 상태를 보여주기 위한 설정 변수
     */
    @observable facilityOutputSelectionView: SimulationFormType = SimulationFormType.CHART;

    @action setFacilityOutputSelectionView(parameter: SimulationFormType) {
        this.facilityOutputSelectionView = parameter;
    }

    @computed get getFacilityOutputSelectionView() {
        return this.facilityOutputSelectionView;
    }

    /**
     * @description chart(그림) or table(표) 상태를 보여주기 위한 설정 변수
     */
    @observable productOutputSelectionView: SimulationFormType = SimulationFormType.CHART;

    @action setProductOutputSelectionView(parameter: SimulationFormType) {
        this.productOutputSelectionView = parameter;
    }

    @computed get getProductOutputSelectionView() {
        return this.productOutputSelectionView;
    }

    // =================================================================================================================

    /**
     * @description 상위메뉴 버튼타입
     */
    @observable topMenuSelectView: SimulationMenuType = SimulationMenuType.DASHBOARD;

    @action setTopMenuSelectView(parameter: SimulationMenuType) {
        this.topMenuSelectView = parameter;
    }

    @computed get getTopMenuSelectView() {
        return this.topMenuSelectView;
    }


    /**
     * @description FacilityOutput에서 (ASC || DESC || NAME 에대한 타입 저장해두기)
     */
    @observable facilitySortType: SimulationTreeKey = SimulationTreeKey.NAME;

    @action setFacilitySortType(parameter: SimulationTreeKey) {
        this.facilitySortType = parameter;
    }

    @computed get getFacilitySortType() {
        return this.facilitySortType;
    }

    /**
     * @description FacilityOutput에서 (okCount || ngCount 에대한 타입 저장해두기)
     */
    @observable countType: FacilityOutputItemType = FacilityOutputItemType.OK_COUNT;

    @action setCountType(parameter: FacilityOutputItemType) {
        this.countType = parameter;
    }

    @computed get getCountType() {
        return this.countType;
    }





    @observable facilityoutputKeys: any = ["all"];

    @action setFacilityoutputKeys(parameter: any) {
        this.facilityoutputKeys = parameter;
    }

    @computed get getFacilityoutputKeys() {
        return this.facilityoutputKeys;
    }


    @observable prcessModelData: any = ["all"];

    @action setPrcessModelData(parameter: any) {
        this.prcessModelData = parameter;
    }

    @computed get getPrcessModelData() {
        return this.prcessModelData;
    }

    // 제품별 생산량 관련 SORTNAME
    @observable productOutputSortname: any = SimulationTreeKey.NAME

    @action setProductOutputSortname(parameter: any) {
        this.productOutputSortname = parameter;
    }

    @computed get getProductOutputSortname() {
        return this.productOutputSortname;
    }


    /**
     * @description 설비별 생산량 정렬값 받을때 재정렬하는 메서드
     * @param value 선택한 정렬옵현 (NAME | ASC | DESC)
     */
    @action
    public async setFacilityOutputAlign(value: string) {
        let chartData: Array<any> = DataSourceHandler.chartDataSort(this.chartData, value, this.countType, this.textSortAttrName);
        this.chartData = chartData;
        this.facilityOutputData = chartData;
        this.facilityOutputSorttype = value;
    }


    // 제품별 생산량 관련 SORTNAME
    @observable facilityOutputSortname: any = 'okCount'

    @action setFacilityOutputSortname(parameter: any) {
        this.facilityOutputSortname = parameter;
    }
    @computed get getFacilityOutputSortname() {
        return this.facilityOutputSortname;
    }


    // 제품별 생산량 관련 SORTTYPE
    @observable facilityOutputSorttype: any = SimulationTreeKey.NAME

    @action setFacilityOutputSorttype(parameter: any) {
        this.facilityOutputSorttype = parameter;
    }
    @computed get getFacilityOutputSorttype() {
        return this.facilityOutputSorttype;
    }

    //productoutput 전체데이터 받기
    @observable productOutputData: any = null;

    @action setProductOutputData(parameter: any) {
        this.productOutputData = parameter;
    }
    @computed get getProductOutputData() {
        return this.productOutputData;
    }

//FacilityOutput
//FacilityOutput
//FacilityOutput
//FacilityOutput
//FacilityOutput
    /**
     * @description 제품선택시 체크한 제품만 출력하기 위한 메서드
     * @param checkedKeys 체크된 해당제품
     */
    @action
    public setFacilityCheck(checkedKeys: any) {
        this.setCheckedKeys(checkedKeys);
        let list: Array<ISimFacilityNgCntChartData> = Object.assign([], this.facilityTemporaryData);
        let listFilter: Array<ISimFacilityNgCntChartData> = list.filter(function (value: ISimFacilityNgCntChartData) {
            return checkedKeys.indexOf(value.processId) > -1;
        });
        let chartData: Array<any> = DataSourceHandler.chartDataSort(listFilter, this.sortType, FacilityOutputItemType.OK_COUNT, this.textSortAttrName);
        this.chartData = chartData;
        this.setAlign2(this.facilityOutputSortname);
    }


    /**
     * @description 정렬시 복제값을 담기 위한 메서드(Object.assign)
     */
    @observable facilityTemporaryData: any = null;
    @action setFacilityTemporaryData(parameter: any) {
        this.facilityTemporaryData = parameter;
    }
    @computed get getFacilityTemporaryData() {
        return this.facilityTemporaryData;
    }

    /**
     * @description FACILITYOUTPUT DATA store
     */
    @observable facilityOutputData: any = null;
    @action setFacilityOutputData(parameter: any) {
        this.facilityOutputData = parameter;
    }
    @computed get getFacilityOutputData() {
        return this.facilityOutputData;
    }

    /**
     * @description 비동기로 받아온 데이터를 임시보관(마운트 해제될때까지 사용) DATA store
     */
    @observable facOperData: any = null;
    @action setFacOperData(parameter: any) {
        this.facOperData = parameter;
    }
    @computed get getFacOperData() {
        return this.facOperData;
    }
//BufferUsage
//BufferUsage
//BufferUsage
//BufferUsage
//BufferUsage
    /**
     * @description chart(그림) or table(표) 상태를 보여주기 위한 설정 변수
     */
    @observable timeBufferUsageSelectionView: SimulationFormType = SimulationFormType.CHART;

    @action setTimeBufferUsageSelectionView(parameter: SimulationFormType) {
        this.timeBufferUsageSelectionView = parameter;
    }

    @computed get getTimeBufferUsageSelectionView() {
        return this.timeBufferUsageSelectionView;
    }

    /**
     * @description 시간별사용량 chartData 가지고오기
     */
    @action
    public async getTimeBufferUser(parameter: any) {
        try {
            const resultData: any = await Searvice.getPost("sim/TimeBufferUsage.do", parameter);
            this.timeBufferUserData = resultData.data.bufferData;
        } catch (e) {
            console.log(e, '캐치 이벤트 ');
        }
    }

    /**
     * @description 버퍼사용량 차트 로딩위한 변수
     */
    @observable timeBufferChartLoading:boolean = true;

    @action setTimeBufferChartLoading(param:boolean) {

        this.timeBufferChartLoading = param;
    }
    @computed get getTimeBufferChartLoading() {
        return this.timeBufferChartLoading;
    }


    /**
     * @description 시뮬레이션 리스트 로딩
     */
    @observable simListLoading:boolean = false;

    @action setSimListLoading(param:boolean) {

        this.simListLoading = param;
    }
    @computed get getSimListLoading() {
        return this.simListLoading;
    }



    /**
     * @description 타임라인 관련 스토어
     */
    @observable playIcon:string = "play";

    @action setPlayIcon(param:string) {

        this.playIcon = param;
    }
    @computed get getPlayIcon() {
        return this.playIcon;
    }
}

export default SimulationStore;