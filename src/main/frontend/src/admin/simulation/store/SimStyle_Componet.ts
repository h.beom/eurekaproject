import styled from "styled-components";
import {Button, Card, DatePicker, PageHeader, Radio, Row, Select} from "antd";
import Chart from "devextreme-react/chart";
import Icon from "antd/lib/icon";


// ==============================================================================================================
// ==============================================================================================================
// 시뮬레이션 Styled-Component


/**
 * @description 시뮬레이션 좌,우측 레이아웃을 제외한 css
 */
export const Comp = styled.div`
         height: 100%;
         min-height:100%;
         display:grid;
         grid-template-columns: 250px auto;
         grid-template-rows: 55px auto;
         grid-template-areas:
         "ViewHeader ViewTopMenu"
         "ViewHeader ViewContent";
          margin:0px;
          `;

/**
 * @description 상단메뉴 버튼리스트 css
 */
export const TopBtnList = styled.div`
  
`;

/**
 * @description 상단메뉴 버튼리스트 css
 */
export const TopBtn = styled(Button)`
        width:12.3r%;
`;


// ===================================================================

/**
 * @description 시뮬레이션 해당 TabPage를 덮고있는 div (div css)
 */
export const Division = styled.div`
    height : calc(100%);
`;

/**
 * @description ViewChart버튼 과 chartSave버튼 (Button css)
 */
export const OptionButton = styled(Button)`
    margin-Left: 5px;
`;

/**
 * @description chart를 보여주는 content 박스(Card css)
 */
export const ChartBox = styled(Card)`
    height : 100%;
    
`;


/**
 * @description chart를 보여주는 content 박스(Card css)
 */
export const ChartContendBox = styled(Chart)`
    width: 100%;
`;

/**
 * @description 프로세스 or 제품dml checkbox를 선택하는 Box contend (Card css)
 */
export const CheckBoxContend = styled(Card)`
    height: 416px;
    margin-Bottom: 10px;
    margin-Top: 5px;
`;

/**
 * @description 정렬을 설정하는 버튼이 있는 Box contend (Card css)
 */
export const SortContend = styled(Card)`
       height:235px;
      margin-Top : 5px;
`;


/**
 * @description 시뮬레이션 시간별사용량에서 정렬을 설정하는 ContendBox (Card css)
 */
export const TBUSortContend = styled(Card)`
    margin-Top:5px;
`;

/**
 * @description 정렬을 설정하는 버튼 Selector(Select css)
 */
export const AlignSelector = styled(Select)`
     width: 100%;
`;

/**
 * @description 정렬을 설정하는 버튼 Selector(Select css)
 */
export const TitleText = styled.div`
     width : 100%;
     height : 100%;
`;

// =====================================================================================
// ======================================================================================
// SimulationListHeader
export const TextSpan = styled.span` line-height: 42px;`;

/**
 * @description SimulationListHeader의 전체 레이아웃 (PageHeader의 css)
 */
export const Header = styled(PageHeader)`
             gridArea: ant-page-header;
             min-width: 1280px;
             margin: 20px;
             background-color:white;
             
          `;

/**
 * @description 날짜입력 (DatePicker의 css)
 */
export const DateText = styled(DatePicker)`
             width:120px;
             margin:0px 0px 0px 0px;
`;

/**
 * @description SimulationListHeader의 컨텐츠를 담는 css
 */
export const Compo = styled.div`
             height:auto;
             display:grid;
             min-height:100%;
             grid-template-rows: 50% 50%;
             grid-template-columns: 150px 650px 100px calc( 100% - 150px - 650px - 100px - 150px ) 150px;
             grid-gap: 5px;
          `;
/**
 * @description
 */
export const SearchBtn = styled(Button)`
             width:85%;            
`;

/**
 * @description 날짜 조회버튼 css
 */
export const DateBtn = styled(Radio.Button)`
             width:90px;
`;

/**
 * @description (1년 | 3개월 | 1개월 | 1주) 로 나누어진 버튼 css
 */
export const DateGroupBtn = styled(Radio.Group)`
             text-align:center;
`;

// =========================================================================================
// ==========================================================================================
// PageButtonList
/**
 * @description Excel 다운로드버튼
 */
export const ExcelBtn = styled(Button)` float : left;`;

/**
 * @description 전체화면 전환 버튼 css
 */
export const FullBtn = styled(ExcelBtn)` margin-right : 10px;`;

/**
 * @description 버튼 css
 */
export const ButtonBtn = styled(Row)` margin-bottom : 10px;`;


// =========================================================================================
// ==========================================================================================
// SimulationList

/**
 * @description 페이징버튼과 엑셀다운로드를 감싸고 있는 div css
 */
export const Contents = styled.div`
    margin: 20px;
`;

/**
 * @description 엑셀 다운로드버튼 css (Button css)
 */
export const ExcelDownloadBtn = styled(Button)`
    float : right;
`;

/**
 * @description dashboard 버트는 감싸는 css
 */
export const DashComp = styled.div`
    display: block;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background:rgba(0,0,0,0.3);
    zIndex: 10;
`;

/**
 * @description dashboard Icon css (Icon css)
 */
export const BoardIcon = styled(Icon)`
    color:#40a9ff; 
    cursor:pointer;
`;


// ======================================================================================
// ======================================================================================
// ProcessModel

export const BtnTimeLine = styled(Button)`
@media only screen and (min-width: 375px) {
            width:13px;
            padding-left:5px;
            padding-right:25px;
            margin-right:3px;
}
@media only screen and (min-width: 650px) {
            width:45px;
            padding-left:12px;
            padding-right:25px;
}
`;
export const TimeLineControl = styled.div`
 
`;

export const HSEControl = styled.div`
 backgroundColor: white;
 width:calc(100% - 25px);
`;

export const TImeLineLayout = styled.div`
    width : 100%;
    height : 20px;

    margin: 0px 5px 0px 5px;
`;
export const TimeControlBtnLayout = styled.div`
    float:left;

`;

export const SliderLayout = styled.div`
    width : 100%;
`;

export const TimeText = styled.span`
    font-size:13px;
    float:right;
    margin-top:13px;
`;