import {CSSProperties} from "react";
import {IProcessAnimationInfo, IProcessMapInfo, IProcessModelInfo} from "../../../threeView/IProcessModelMain";

export enum SimulationType {
    /** @description 상태 = 전체 */
    ALL = "ALL",
    /** @description 상태 = 시뮬레이션 완료  */
    ST_Completed = "ST_Completed",
    /** @description  시뮬레이션 요청 */
    ST_Request = "ST_Request",
    /** @description 시뮬레이션 진행 */
    ST_Running = "ST_Running",
    /** @description 시뮬레이션 NG */
    ST_SimulationNG = "ST_SimulationNG",
}

/** 시뮬레이션리스트 검색조건 요청일 범위 설정 */
export enum SimulationListBtnType {
    /** @description 요청일 범위 = 1주일 */
    WEEK = "WEEK",
    /** @description 요청일 범위 = 1달 */
    ONEMONTH = "ONEMONTH",
    /** @description 요청일 범위 = 3달 */
    THREEMONTH = "THREEMONTH",
    /** @description 요청일 범위 = 1년 */
    YEAR = "YEAR",
}


export interface ISimulationListDataSource {
    analysisTime: number,
    input: number,
    insDt: string,
    modelId: string,
    modelNm: string,
    ngCount: number,
    output: number,
    simReqId: string,
    simReqNm: string,
    simReqStNm: string,
    simReqTpNm: string,
    stdDeviation: string,
    userNm: string,
    warmUpTime: number
}

/**
 * 시뮬레이션 시간별 생산/불량량에 대한 chartData 타입
 */
export interface ITimeOutList {
    /** @description  시뮬레이션 요청 Id */
    simReqId: string,
    /** @description  구분하는 해당 시간(ex 10:00, 11:00 시간단위) */
    simLoadingTimeHour: string,
    /** @description  구분시간에 대한 불량숫자 */
    ngCountForHour: number,
    /** @description  구분시간에 대한 생산숫자 */
    inputCountForHour: number
}

export interface ISimulationChartParame {
    /** @description 프로세스이름 */
    processName: string,
    output: number,
    kind: string
}

/**
 * 시뮬레이션 시간별 생산/불량량에 대한 chartData 타입
 */
export interface ITimeOutList {
    /** @description  시뮬레이션 요청 Id */
    simReqId: string,
    /** @description  구분하는 해당 시간(ex 10:00, 11:00 시간단위) */
    simLoadingTimeHour: string,
    /** @description  구분시간에 대한 불량숫자 */
    ngCountForHour: number,
    /** @description  구분시간에 대한 생산숫자 */
    inputCountForHour: number
}

export interface ISimFacilityNgCntChartData {
    simReqId: string,
    processId: string,
    processNm: string,
    processSeq: number,
    okCount: number,
    totOkCount: number,
    avgNgRate: number,
    ngCount: number
}

export interface ISimulationResultView {
    simulationid: string,
    loading: boolean,
    facilityNgCntChartData: Array<ISimFacilityNgCntChartData>,
    viewData: ISimulationView,
    topMenuList: Array<ISimulationResultViewTopMenu>,
    contentList: Array<ISimulationResultViewTopMenu>,
    renderEvent: Function,
    selectionTabIdx: string
    fullscreenType: boolean;
}

export interface ISimulationView {
    analysisTime: number,
    input: number,
    insDt: string,
    modelId: string,
    modelNm: string,
    ngCount: number,
    output: number,
    simReqId: string,
    simReqNm: string,
    simReqStNm: string,
    simReqTpNm: string,
    stdDeviation: string,
    userNm: string,
    warmUpTime: number
}

export interface ISimulationResultViewTopMenu {
    url: string,
    text: React.ReactNode;
    icon?: string;
    style?: CSSProperties;
    selection?: boolean;
    render: React.ReactNode;
    type:string;
}

export interface IProcessModelData {
    mapData: IProcessMapInfo[];
    modelListData: IProcessModelInfo[];
    processAnimation: IProcessAnimationInfo[];
    processAnimationTotalTime: number;
}

/**
 * 정렬 타입에 대한 설명
 */
export enum SimulationTreeKey {
    /** @description 이름순으로 정렬 */
    NAME = "name",
    /** @description 시간순으로 정렬 */
    TIME = 'time',
    /** @description 오름차순으로 정렬 */
    ASC = 'asc',
    /** @description 내림차순으로 정렬 */
    DESC = 'desc',
    /** @description 버퍼량으로 정렬 */
    BUF = 'buf',
    /** @description 버퍼량 역숙으로 정렬 */
    DEBUF = 'debuf'
}


/**
 * 시뮬레이션 결과양식을 chart로 볼지, table표로 볼지 선택 타입
 */
export enum SimulationFormType {
    /** @description  chart그림 */
    CHART = "chart",
    /** @description  table 표 */
    TABLE = "table",
}

/**
 * 설비별 생산량 or 제품별 생산량에서 chartData로 받는 타입
 */
export enum FacilityOutputItemType {
    /** @description  생산량 */
    OK_COUNT = "okCount",
    /** @description  불량건수 */
    NG_COUNT = 'ngCount'
}


/**
 * 시간별생산량 에서 오름/내림차순 선택시 생산량 or 불량건수 선택타입
 */
export enum SimulationItemType {
    /** @description 생산량으로 정렬 */
    INPUT_COUNT_FOR_HOUR = "inputCountForHour",
    /** @description 불량건수로 정렬 */
    NG_COUNT_FOR_HOUR = 'ngCountForHour'
}


/**
 * 설비별 가용률 데이터에 대한 타입
 */
export interface IFacilityOperatingDataSource {
    /** @description 프로세스 아이디 */
    processId: string,
    /** @description 프로세스 이름 */
    processNm: string,
    /** @description 시뮬레이션 아이디 */
    simReqId: string,
    /** @description 프로세스 순서 */
    processSeq: number,
    /** @description 해당 프로세스 작업 작업 */
    working: number,
    /** @description 해당 프로세스 작업 불량 */
    failure: number,
    /** @description 해당 프로세스 작업 대기 */
    waiting: number,
    /** @description 해당 프로세스 작업 금지 */
    blocking: number,
    /** @description 해당 프로세스 작업 합계 */
    sum: number
}

/**
 * 설비별 가용률 에서 chart에 표출할(설비별생산량/불량건수) 상태선택 목록타입
 * (정렬 선택시 부가정렬)
 */
export enum FacilityOperatingRateType {
    /** @description  합계*/
    SUM = "sum",
    /** @description  작업중*/
    WORKING = "working",
    /** @description  불량*/
    FAILURE = "failure",
    /** @description  대기*/
    WAITING = "waiting",
    /** @description  반환*/
    BLOCKING = "blocking"
}

/**
 * DashBoard ????????????????????
 */
export interface IProcessModelData {
    mapData: IProcessMapInfo[];
    modelListData: IProcessModelInfo[];
    processAnimation: IProcessAnimationInfo[];
    processAnimationTotalTime: number;
}

/**
 * 시간별생산량 에서 오름/내림차순 선택시 생산량 or 불량건수 선택타입
 */
export enum SimulationMenuType {
    /** @description  */
    DASHBOARD = "dashboard",
    /** @description  */
    TIMEOUT = "timeout",
    /** @description  */
    FACILITYOUTPUT = "facilityoutput",
    /** @description  */
    PRODUCTOUTPUT = "productoutput",
    /** @description  */
    FACILITYOPERATINGRATE = "facilityoperatingrate",
    /** @description  */
    TIMEBUFFERUSAGE = "timebufferusage"
}