import React, {Component, CSSProperties, Fragment} from 'react';
import 'antd/dist/antd.css';
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import {ISimulationResultViewTopMenu, SimulationFormType, SimulationMenuType} from "../store/SimulationType";
import {TopBtn, TopBtnList} from "../store/SimStyle_Componet";
import {ButtonType} from "antd/es/button";
import {Link, Route, RouteComponentProps, Switch} from "react-router-dom";
import DashBoard from "./TapService/DashBoard";
import TimeOutput from "./TapService/TimeOutput";
import TimeBufferUsage from "./TapService/TimeBufferUsage";
import ProductOutput from "./TapService/ProductOutput";
import FacilityOutput from "./TapService/FacilityOutput";
import FacilityOperatingRate from "./TapService/FacilityOperatingRate";
import {Button, Card} from "antd";
import Media from "react-media";
import DropMenu from "../../../layout/TopDropMenu/DropMenu";
import DropMenu2 from "../../../layout/TopDropMenu/DropMenu2";

/**
 * @description SIMULATION 의 세부 컴포넌트들(TapService)을 컨드롤하는 ROUTER가 포함된 클래스
 */
@inject('SimulationStore')
@observer
class SimulationPage extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            contentList: [],
            renderEvent: function (type: boolean) {
            },
            topMenuList: [
                {
                    url: "/Main/SimulationPage/dashboard",
                    text: 'Dashboard',
                    render: <SimulationPage/>,
                    style: {width: "200px"},
                    type: SimulationMenuType.DASHBOARD
                },
                {
                    url: "/Main/SimulationPage/timeout",
                    text: '시간별생산',
                    render: <TimeOutput/>,
                    style: {width: "200px"},
                    type: SimulationMenuType.TIMEOUT,

                },
                {
                    url: "/Main/SimulationPage/facilityoutput",
                    text: '설비별 생산량',
                    render: <FacilityOutput/>,
                    style: {width: "200px"},
                    type: SimulationMenuType.FACILITYOUTPUT
                },
                {
                    url: "/Main/SimulationPage/productoutput",
                    text: '제품별 생산량',
                    render: <ProductOutput/>,
                    style: {width: "200px"},
                    type: SimulationMenuType.PRODUCTOUTPUT
                },
                {
                    url: "/Main/SimulationPage/facilityoperatingrate",
                    text: '설비별 가용률',
                    render: <FacilityOperatingRate/>,
                    style: {width: "200px"},
                    type: SimulationMenuType.FACILITYOPERATINGRATE
                },
                {
                    url: "/Main/SimulationPage/timebufferusage",
                    text: '버퍼사용량',
                    render: <TimeBufferUsage/>,
                    style: {width: "200px"},
                    type: SimulationMenuType.TIMEBUFFERUSAGE
                },
            ],
        };
    }


    /**
     * @description 시간별 생산량을 렌더할 메서드
     */
    @autobind
    private onRenderTimeOutput(props: RouteComponentProps<any>): React.ReactNode {
        return <DashBoard setRenderEvent={this.setRenderEvent}/>;
    }


    /**
     * @description 시간별 생산량을 렌더할 메서드
     */
    @autobind
    private onRenderFacilityOperatingRate(props: RouteComponentProps<any>): React.ReactNode {
        return <FacilityOperatingRate setRenderEvent={this.setRenderEvent} routerData={props} />;
    }

    /**
     * @description 시간별 생산량을 렌더할 메서드
     */
    @autobind
    private onRenderFacilityOutput(props: RouteComponentProps<any>): React.ReactNode {
        return <FacilityOutput setRenderEvent={this.setRenderEvent} routerData={props}/>;
    }

    /**
     * @description 시간별 생산량을 렌더할 메서드
     */
    @autobind
    private onRenderProductOutput(props: RouteComponentProps<any>): React.ReactNode {
        return <ProductOutput setRenderEvent={this.setRenderEvent} routerData={props}/>;
    }

    /**
     * @description 시간별 생산량을 렌더할 메서드
     */
    @autobind
    private onRenderTimeBufferUsage(props: RouteComponentProps<any>): React.ReactNode {
        return <TimeBufferUsage setRenderEvent={this.setRenderEvent} routerData={props}/>;
    }

    /**
     * @description 시간별 생산량을 렌더할 메서드
     */
    @autobind
    private onRenderTimeOut(props: RouteComponentProps<any>): React.ReactNode {
        return <TimeOutput setRenderEvent={this.setRenderEvent} routerData={props}/>;
    }


    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function): void {
        this.setState({renderEvent: callBack});
    }


    /**
     * @description 컴포넌트 실행시 설비별생산량, 생샹량/불량건수을 표출하기위한
     * 해당 시뮬레이션ID의 데이터를 수집한다.
     */
    public componentDidMount() {
        const {SimulationStore, match} = this.props;
        // SimulationStore.setSimReqId(match.params.postid);
    }


    /** @description */
    @autobind
    private onClickEventonTopMenuBtn(value:any){
        this.props.SimulationStore.setTopMenuSelectView(value);
    }

    render() {
        const {SimulationStore} = this.props;


        return (
            <div>
                    <Card style={{margin:'auto', marginBottom:10}}>
                        {this.state.topMenuList.map(function (this: SimulationPage, value: ISimulationResultViewTopMenu, idx: number) {
                            //맞으면 버튼타입을 바꿔준다.


                            // const selection: ButtonType = (value.type === true) ? "primary" : "default";
                            // const btnStyle: CSSProperties | undefined | null = (value.style !== null) ? value.style : {};

                            return <Link to={value.url + "/" + SimulationStore.getSimReqId} key={idx}>
                                <Button type={(SimulationStore.getTopMenuSelectView == value.type) ? "primary" : "default"}
                                       style={{ width:'15.6%', margin:5}} key={idx}
                                    onClick={() => this.onClickEventonTopMenuBtn(value.type)}>
                                    {value.text}
                                </Button>
                            </Link>
                        }, this)}
                    </Card>

                <Switch>
                    {/*<Route path="/Main/SimulationPage/dashboard/:postid" render={this.onRenderTimeOutput}/>*/}
                    {/*<Route path="/Main/SimulationPage/facilityoperatingrate/:postid" render={this.onRenderFacilityOperatingRate}/>*/}
                    {/*<Route path="/Main/SimulationPage/facilityoutput/:postid" render={this.onRenderFacilityOutput}/>*/}
                    {/*<Route path="/Main/SimulationPage/productoutput/:postid" render={this.onRenderProductOutput}/>*/}
                    {/*<Route path="/Main/SimulationPage/timebufferusage/:postid" render={this.onRenderTimeBufferUsage}/>*/}
                    {/*<Route path="/Main/SimulationPage/timeout/:postid" render={this.onRenderTimeOut}/>*/}
                </Switch>
            </div>
        );

    };
}


export default SimulationPage;
