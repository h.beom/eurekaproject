import React, {Component, CSSProperties, RefObject} from 'react';
import 'antd/dist/antd.css';
import {ChartBox, ChartContendBox, OptionButton, SortContend, TitleText} from "../../store/SimStyle_Componet";
import {Button, Card, Select, Table, Tree} from "antd";
import {ArgumentAxis, Label, Legend, Series, Size, Tooltip, ValueAxis} from "devextreme-react/chart";
import autobind from "autobind-decorator";
import {
    FacilityOutputItemType,
    ISimFacilityNgCntChartData,
    SimulationFormType,
    SimulationTreeKey
} from "../../store/SimulationType";
import html2canvas from "html2canvas";
import {Link, Route, Switch} from "react-router-dom";
import {ColumnProps} from "antd/es/table";
import SimulationStore from "../../store/SimulationStore";
import {inject, observer} from "mobx-react";

/**
 * @description SIMULATION 의 설비별 생산량을 담당하는 클래스
 */
@inject('SimulationStore', 'ExcelStore')
@observer
class FacilityOutput extends Component<any, any> {
    private textSortAttrName: string = "simLoadingTimeHour";
    private _barChart: RefObject<any> = React.createRef();
    private offFullScreen: CSSProperties = {height: "calc(100% - 54px)"};
    private onFullScreen: CSSProperties = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
        zIndex: 999,
        backgroundColor: "#ffffff"
    };
    private readonly _dataGridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [
        {
            title: '설비명',
            dataIndex: 'processNm',
            key: 'processNm'
        },
        {
            title: '생산량',
            dataIndex: 'okCount',
            key: 'okCount',
            align: "right",
            width: '100px'
        },
        {
            title: '불량건수',
            dataIndex: 'ngCount',
            key: 'ngCount',
            align: "right",
            width: '100px'
        }
    ];

    constructor(props: any) {
        super(props);

        this.state = {
            chartPngDownloadData: "",
            fullscreenType: false,
            renderEvent: function (type: boolean) {
            }
        };

    }

    /**
     * @description 컴포넌트가 열릴때 설비별생산량을 조회후 출력 및 설정하는 메서드
     */
    public async componentDidMount() {
        this.setRenderEvent(this._renderEvent);
        const {SimulationStore} = this.props;
        SimulationStore.setCheckedKeys(SimulationStore.getProductCheckData);
        SimulationStore.setFacilityOutputSorttype("name");
        SimulationStore.setAlignOptionBtnVisible("hidden");
        SimulationStore.setFacilityOutputSelectionView(SimulationFormType.CHART);

        const reData =  SimulationStore.getFacilityNgCntChartData;

        SimulationStore.setFacilityTemporaryData(reData);
        SimulationStore.setChartData(reData);
        SimulationStore.setAlign("name");


    }

    @autobind
    private _renderEvent(type?: boolean): void {
        let barChart: any = this._barChart.current.instance;
        barChart.render();
        barChart.render();
    }


    /**
     * @description 제품선택시 체크한 제품만 출력하기 위한 메서드
     * @param checkedKeys 체크된 해당제품
     */
    @autobind
    private onCheck(checkedKeys: any): void {
        this.props.SimulationStore.setFacilityCheck(checkedKeys);
    };

    /**
     * @description 조회된 chart표를 이미지(jpg)파일로 저장하는 run Method
     * @param event 마우스 클릭시에 반응하는 event파라미터
     */
    @autobind
    private onClickChartDownload(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void {
        let element: HTMLElement | null = document.getElementById("facilityOutputChartContent");
        let hideDownloadLink: HTMLElement | null = document.getElementById("facilityOutputChartDownloadLink");
        if (element != null) {
            let setSteteCompleteEvent: () => void = function () {
                if (hideDownloadLink != null) {
                    hideDownloadLink.click();
                }
            }.bind(this);

            let callback: (this: FacilityOutput, canvas: HTMLCanvasElement) => void = function (this: FacilityOutput, canvas: HTMLCanvasElement) {
                this.setState({
                        chartPngDownloadData: canvas.toDataURL("image/png")
                    },
                    setSteteCompleteEvent);
            }.bind(this);
            html2canvas(element).then(callback);
        }
    }

    /**
     * @description 테이블(표)를 클릭시 출력하기위한 trigger
     * @param event 마우스 클릭시 이벤트
     */
    @autobind
    private onClickBtnChartTable(event: React.MouseEvent<HTMLButtonElement>): void {
        this.props.SimulationStore.setFacilityOutputSelectionView(event.currentTarget.name);
    }
    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function): void {
        this.setState({renderEvent: callBack});
    }

    /**
     * @description 버튼 클릭시 시뮬레이션정보를 전체화면으로 보는 메서드
     */
    @autobind
    private onClickEventFullScreen(): void {

        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;

        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }

    /** @description 차트 테이블을 엑셀파일로 다운로드 */
    @autobind
    private onClickBtnExcelDownload() {
        const {SimulationStore} = this.props;
        const checkedKeys = SimulationStore.getCheckedKeys;
        const sortname = SimulationStore.getFacilityOutputSortname;
        const sorttype = SimulationStore.getFacilityOutputSorttype;

        this.props.ExcelStore.facilityoutputExcel(SimulationStore.getSimReqId, checkedKeys, sortname, sorttype);
    }

    /**
     * @description 선택한 정렬옵션을 적용하는 메서드
     * @param value 선택한 정렬옵현 (NAME | ASC | DESC)
     */
    @autobind
    private onChangeBtnAlignOption(value: string): void {
        const {SimulationStore} = this.props;
        let alignOptionBtnVisible: "hidden" | "visible" = "visible";
        if (value == SimulationTreeKey.ASC || value == SimulationTreeKey.DESC) {
            alignOptionBtnVisible = "visible";
        }

        SimulationStore.setAlignOptionBtnVisible(alignOptionBtnVisible);
        this.props.SimulationStore.setFacilityOutputAlign(value);
        SimulationStore.setFacilitySortType(value);


    }

    /**
     * @description 해당 파라미터
     * @param value (SUM | WARNING | WORKING | BLOCKING | WAITING) 에대한 정보
     */
    @autobind
    private onChangeSortName(value: string): void {
        const {SimulationStore} = this.props;
        SimulationStore.setCountType(value);
        SimulationStore.setAlign2(value);
    }


    render() {
        const {SimulationStore} = this.props;
        console.log();
        return (
            <>
                <div style={{width:'77%', float:'left'}}>
                    <a href={this.state.chartPngDownloadData} download={"ProductOutput.png"} style={{display: "none"}}
                       id="facilityOutputChartDownloadLink"></a>
                            <ChartBox
                                title={'제품별 생산량'}
                                type={"inner"}
                                bodyStyle={(this.state.fullscreenType == true) ? this.onFullScreen : this.offFullScreen}
                                extra={[
                                    <OptionButton key="1" size={"small"}onClick={this.onClickEventFullScreen}>크게보기</OptionButton>,
                                    <Switch>
                                        <Route path={`${this.props.routerData.match.url}/table`}>
                                            <OptionButton key="2" size={"small"}onClick={this.onClickBtnExcelDownload}>
                                                엑셀 다운로드
                                            </OptionButton>
                                        </Route>
                                        <Route path={`${this.props.routerData.match.url}`}>
                                            <OptionButton key="2" size={"small"}onClick={this.onClickChartDownload}>
                                                차트 다운로드
                                            </OptionButton>
                                        </Route>
                                    </Switch>
                                ]}>
                                <Switch>
                                    <Route path={`${this.props.routerData.match.url}/table`}>
                                        <div>
                                            {(this.state.fullscreenType == true) ? <Button key="1" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40, marginBottom:70, marginTop:5}}>원본크기</Button> : null}

                                            <Table columns={this._dataGridCoulumn}
                                               dataSource={SimulationStore.getChartData}
                                               scroll={{y: 460}}
                                               size={"middle"} bordered={true}/>
                                        </div>
                                    </Route>
                                    <Route path={`${this.props.routerData.match.url}`}>

                                        <TitleText id="facilityOutputChartContent">

                                            {(this.state.fullscreenType == true) ? <Button key="2" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40}}>원본크기</Button> : null}
                                            <ChartContendBox ref={this._barChart}
                                                             title={'설비별 생산량 / 불량건수'}
                                                             dataSource={SimulationStore.getChartData}
                                                             id={'chart'}>
                                                {(this.state.fullscreenType == true) ?  <Size height={700} /> : <Size height={675} width={"100%"}/>}
                                                <ArgumentAxis>
                                                    <Label customizeText={function (this: FacilityOutput, info: any) {
                                                        if (info.valueText != "") {
                                                            return `${SimulationStore.getChartItemName[info.valueText]}`;
                                                        }
                                                        return `${info.valueText}%`;
                                                    }.bind(this)}/>
                                                </ArgumentAxis>

                                                <ValueAxis name={'percentage'} position={'left'} showZero={true}
                                                           valueMarginsEnabled={false}>
                                                    <Label customizeText={function customizePercentageText(info: any) {
                                                        return `${info.valueText}%`;
                                                    }}/>
                                                </ValueAxis>

                                                <Series name={'생산량'} valueField={'okCount'} type={'bar'} color={'#42a5f5'}
                                                        argumentField={'processId'}/>
                                                <Series name={'불량건수'} valueField={'ngCount'} type={'bar'} color={'#ffd700'}
                                                        argumentField={'processId'}/>

                                                <Tooltip enabled={true} shared={true}/>

                                                <Legend verticalAlignment={'top'} horizontalAlignment={'right'}/>

                                                {/*<CommonSeriesSettings label={{ customizeText:function(info:any){*/}
                                                {/*    return info;*/}
                                                {/*}}}/>*/}

                                            </ChartContendBox>
                                        </TitleText>
                                    </Route>
                                </Switch>
                            </ChartBox>
                        </div>

                <div style={{width:' 22%', float:'right'}}>
                            <Card title={'결과양식'} type={"inner"}
                                  style={{height: "110px"}}>
                                <Link to={`${this.props.routerData.match.url}`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)", marginRight: "10px"}}
                                        type={(SimulationStore.getFacilityOutputSelectionView == SimulationFormType.CHART) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.CHART}>Chart</Button>
                                </Link>
                                <Link to={`${this.props.routerData.match.url}/table`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)"}}
                                        type={(SimulationStore.getFacilityOutputSelectionView == SimulationFormType.TABLE) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.TABLE}>Table</Button>
                                </Link>
                            </Card>
                            <Card title={'제품'} type={"inner"} style={{marginTop: "5px"}}
                                             bodyStyle={{
                                                 height: "365px",
                                                 overflowY: "auto",
                                                 padding: "0px",
                                             }}>
                                <Tree
                                    checkable
                                    defaultExpandedKeys={['all']}
                                    defaultCheckedKeys={['all']}
                                    onCheck={this.onCheck}
                                >
                                    <Tree.TreeNode title="All" key="all">
                                        {SimulationStore.getTreeList.map(function (item: ISimFacilityNgCntChartData) {
                                            return <Tree.TreeNode title={item.processNm} key={item.processId}/>;
                                        })}
                                    </Tree.TreeNode>
                                </Tree>
                            </Card>
                            <SortContend title={'정렬'} type={"inner"}>
                                <Select defaultValue={SimulationTreeKey.NAME} size={"default"} style={{width: "100%"}}
                                        onChange={this.onChangeBtnAlignOption}>
                                    <Select.Option value={SimulationTreeKey.NAME}>이름순</Select.Option>
                                    <Select.Option value={SimulationTreeKey.ASC}>오름차순</Select.Option>
                                    <Select.Option value={SimulationTreeKey.DESC}>내림차순</Select.Option>
                                </Select>
                                <br/>
                                <br/>
                                <Select style={{
                                    width: "100%",
                                    visibility: SimulationStore.getAlignOptionBtnVisible
                                }}
                                        size={'default'} onChange={this.onChangeSortName}>
                                    <Select.Option value={FacilityOutputItemType.OK_COUNT}>생산량</Select.Option>
                                    <Select.Option value={FacilityOutputItemType.NG_COUNT}>불량건수</Select.Option>
                                </Select>
                            </SortContend>
                </div>

            </>
        );
    };
}

export default FacilityOutput;
