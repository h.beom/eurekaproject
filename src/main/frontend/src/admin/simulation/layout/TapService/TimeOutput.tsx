import React, {Component, CSSProperties, RefObject} from 'react';
import 'antd/dist/antd.css';
import {ChartBox, ChartContendBox, OptionButton, TitleText} from "../../store/SimStyle_Componet";
import {Button, Card, Col, Row, Select, Table} from "antd";
import {
    ArgumentAxis,
    CommonSeriesSettings,
    ConstantLine,
    Label,
    Legend,
    Series, Size,
    Tooltip,
    ValueAxis
} from "devextreme-react/chart";
import autobind from "autobind-decorator";
import SimulationTapsService from "../../Service/SimulationTapsService";
import {ITimeOutList, SimulationFormType, SimulationItemType, SimulationTreeKey} from "../../store/SimulationType";
import DataSourceHandler from "../../../../utile/DataSourceHandler";
import html2canvas from "html2canvas";
import {Link, Route, Switch} from "react-router-dom";
import {ColumnProps} from "antd/es/table";
import {inject, observer} from "mobx-react";


@inject('SimulationStore', 'ExcelStore')
@observer
class TimeOutput extends Component<any, any> {


    private offFullScreen: CSSProperties = {height: "calc(100% - 154px)"};
    private onFullScreen: CSSProperties = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
        zIndex: 999,
        backgroundColor: "#ffffff"
    };
    private onFullScreenSize: CSSProperties = {
        width: "1500px",
        margin : "auto"
    };
    private textSortAttrName: string = "simLoadingTimeHour";
    private _barChart: RefObject<any> = React.createRef();

    private readonly _dataGridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [
        {
            title: '시간대',
            dataIndex: 'simLoadingTimeHour',
            key: 'simLoadingTimeHour'
        },
        {
            title: '불량품',
            dataIndex: 'ngCountForHour',
            key: 'ngCountForHour',
            align: "right",
            width: '100px'
        },
        {
            title: '생산량',
            dataIndex: 'inputCountForHour',
            key: 'inputCountForHour',
            align: "right",
            width: '100px'
        }
    ];

    constructor(props: any) {
        super(props);
        this.state = {
            chartPngDownloadData: "",
            fullscreenType: false,
            renderEvent: function (type: boolean) {
            }
        };
    }

    private _renderEvent = (type?: boolean) =>{
        let barChart: any = this._barChart.current.instance;
        barChart.render();
        barChart.render();
    }

    // ========================================================

    /**'
     * @description simulationid가 존재한다면(리스트에서 선택한 시뮬레이션)
     */
    public async componentDidMount() {
        const service: SimulationTapsService = new SimulationTapsService();
        this.setRenderEvent(this._renderEvent);
        const {SimulationStore} = this.props;
        SimulationStore.setTimeOutputLoading(true);
        SimulationStore.setSortName('inputCountForHour');
        SimulationStore.setAlignOptionBtnVisible("hidden");
        SimulationStore.setTimeOutputSelectionView(SimulationFormType.CHART);


        if(SimulationStore.getTimeOutchartData == null) {
            const timeOutDataNgData: Array<ITimeOutList> = await service.getTimeOutput(SimulationStore.getSimReqId, "KO");
            SimulationStore.setTimeOutchartData(timeOutDataNgData);
        }
        // SimulationStore.setOrgChartData(timeOutDataNgData);
        let boardData = SimulationStore.getTimeOutchartData;
        SimulationStore.setTimeOutchartData(boardData);
        SimulationStore.setOrgChartData(boardData);




        SimulationStore.setTimeOutputLoading(false);




    }

    /**
     * @description 선택한 정렬타입으로 데이터를 정렬한 각테이터를 설정한다.
     * @param value UI에서 선택한 정렬타입
     */
    @autobind
    private onChangeBtnAlignOption(value: string){
        const {SimulationStore} = this.props;
        let alignOptionBtnVisible: "hidden" | "visible" = "hidden";
        let sortName: string = SimulationStore.getSortName;
        if (value == SimulationTreeKey.ASC || value == SimulationTreeKey.DESC) {
            alignOptionBtnVisible = "visible";
        }


        let chartData: Array<any> = DataSourceHandler.chartDataSort(SimulationStore.getOrgChartData, value, sortName, this.textSortAttrName);
        SimulationStore.setAlignOptionBtnVisible(alignOptionBtnVisible);
        SimulationStore.setSortType(value);
        SimulationStore.setSortName(sortName);
        SimulationStore.setTimeOutchartData(chartData);
    }



    /**
     * @description 선택한 정렬타입안에서 부가적으로 선택된 정렬으로 데이터를 재편성한다.
     * @param value 정렬에대한 생산량 | 불량건수를 선택하는 2차정렬옵션
     */
    @autobind
    private onChangeSortName(value: string){
        const {SimulationStore} = this.props;
        let chartData: Array<any> = DataSourceHandler.chartDataSort(SimulationStore.getOrgChartData, SimulationStore.getSortType, value, this.textSortAttrName);
        SimulationStore.setSortName(value);
        SimulationStore.setTimeOutchartData(chartData);
    }


    /**
     * @description 차트를 이미지로 저장하는 메서드
     * @param event 마우스 클릭 이벤트에대한 정보
     */
    @autobind
    private onClickChartDownload(event: React.MouseEvent<HTMLButtonElement, MouseEvent>){
        let element: HTMLElement | null = document.getElementById("timeOutputChartContent");
        let hideDownloadLink: HTMLElement | null = document.getElementById("timeOutputChartDownloadLink");
        if (element != null) {
            let setSteteCompleteEvent: () => void = function () {
                if (hideDownloadLink != null) {
                    hideDownloadLink.click();
                }
            }.bind(this);

            let callback: (this: TimeOutput, canvas: HTMLCanvasElement) => void = function (this: TimeOutput, canvas: HTMLCanvasElement) {
                this.setState({
                        chartPngDownloadData: canvas.toDataURL("image/png")
                    },
                    setSteteCompleteEvent);
            }.bind(this);
            html2canvas(element).then(callback);
        }
    }

    /**
     * @description 테이블(표)를 클릭시 출력하기위한 trigger
     * @param event 마우스 클릭시 이벤트
     */
    @autobind
    private onClickBtnChartTable(event: React.MouseEvent<HTMLButtonElement>) {
        this.props.SimulationStore.setTimeOutputSelectionView(event.currentTarget.name);
    }


    /**
     * @description 버튼 클릭시 시뮬레이션정보를 전체화면으로 보는 메서드
     */
    @autobind
    private onClickEventFullScreen(){

        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;

        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });

    }


    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function){
        this.setState({renderEvent: callBack});
    }

    /** @description 차트 테이블을 엑셀파일로 다운로드 */
    @autobind
    private onClickBtnExcelDownload() {
        const {SimulationStore, ExcelStore} = this.props;
        const sortType = SimulationStore.getSortType
        const sortName = SimulationStore.getSortName
        ExcelStore.excelDownload(SimulationStore.getSimReqId, sortType, sortName);
    }


    render() {
        const {SimulationStore} = this.props;
        return (
            <>
                <div style={{width:'77%', float:'left'}}>
                    <a href={this.state.chartPngDownloadData} download={"TimeOutput.png"} style={{display: "none"}}
                       id="timeOutputChartDownloadLink"></a>

                            <ChartBox title={'시간별 생산량'}
                                      type={"inner"}
                                      bodyStyle={(this.state.fullscreenType == true) ? this.onFullScreen : this.offFullScreen}
                                      extra={[
                                          <OptionButton key="largeView" size={"small"} onClick={this.onClickEventFullScreen}>크게보기</OptionButton>,
                                          <Switch>
                                              <Route path={`${this.props.routerData.match.url}/table`}>
                                                  <OptionButton key="excel" size={"small"}onClick={this.onClickBtnExcelDownload}>
                                                      엑셀 다운로드
                                                  </OptionButton>
                                              </Route>
                                              <Route path={`${this.props.routerData.match.url}`}>
                                                  <OptionButton key="chart" size={"small"}onClick={this.onClickChartDownload}>
                                                      차트 다운로드
                                                  </OptionButton>
                                              </Route>
                                          </Switch>
                                      ]}
                                      loading={SimulationStore.getTimeOutputLoading}
                            >


                                <Switch>
                                    <Route path={`${this.props.routerData.match.url}/table`}>
                                        <div>
                                            {(this.state.fullscreenType == true) ? <Button key="full" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40, marginBottom:70, marginTop:5}}>원본크기</Button> : null}
                                         <Table columns={this._dataGridCoulumn}
                                               dataSource={SimulationStore.getTimeOutchartData}
                                               scroll={{y: 460}}
                                               size={"middle"} bordered={true}/>
                                        </div>
                                    </Route>
                                    <Route path={`${this.props.routerData.match.url}`}>
                                        <TitleText id="timeOutputChartContent">
                                            {(this.state.fullscreenType == true) ? <Button key="excelfull" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40, marginBottom:70, marginTop:5}}>원본크기</Button> : null}
                                            <ChartContendBox ref={this._barChart}
                                                             title={'시간별 생산량 / 불량건수'}
                                                             dataSource={SimulationStore.getTimeOutchartData}
                                                             id={'chart'}>
                                                {(this.state.fullscreenType == true) ?  <Size height={700} /> : <Size height={675} width={"100%"}/>}
                                                <ArgumentAxis>
                                                    <Label overlappingBehavior={'simLoadingTimeHour'}/>
                                                </ArgumentAxis>

                                                <ValueAxis name={'frequency'} position={'left'}/>
                                                <ValueAxis name={'percentage'} position={'right'} showZero={true}
                                                           valueMarginsEnabled={false}>
                                                    <Label customizeText={function customizePercentageText(info: any) {
                                                        return `${info.valueText}%`;
                                                    }}/>
                                                    <ConstantLine value={80} width={2} color={'#fc3535'}
                                                                  dashStyle={'dash'}>
                                                        <Label visible={false}/>
                                                    </ConstantLine>
                                                </ValueAxis>

                                                <Series
                                                    name={'생산량'}
                                                    valueField={'inputCountForHour'} type={'bar'} color={'#42a5f5'}/>
                                                <Series
                                                    name={'불량건수'}
                                                    valueField={'ngCountForHour'} type={'bar'} color={'#ffd700'}/>
                                                <Tooltip enabled={true} shared={true}/>
                                                <Legend verticalAlignment={'top'} horizontalAlignment={'right'}/>
                                                <CommonSeriesSettings argumentField={'simLoadingTimeHour'}/>
                                            </ChartContendBox>
                                        </TitleText>
                                    </Route>
                                </Switch>
                            </ChartBox>
                </div>


                <div style={{width:' 22%', float:'right'}}>
                            <Card title={'결과양식'} type={"inner"} id={"one"}>
                                <Link to={`${this.props.routerData.match.url}`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)", marginRight: "10px"}}
                                        type={(SimulationStore.getTimeOutputSelectionView == SimulationFormType.CHART) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.CHART}>Chart</Button>
                                </Link>
                                <Link to={`${this.props.routerData.match.url}/table`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)"}}
                                        type={(SimulationStore.getTimeOutputSelectionView == SimulationFormType.TABLE) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.TABLE}>Table</Button>
                                </Link>
                            </Card>

                            <Card title={'정렬'} type={"inner"}id={"two"}>
                                <Select defaultValue={SimulationTreeKey.TIME} style={{width: "100%"}} size={"default"}
                                        onChange={this.onChangeBtnAlignOption}>
                                    <Select.Option value={SimulationTreeKey.TIME}>시간순</Select.Option>
                                    <Select.Option value={SimulationTreeKey.ASC}>오름차순</Select.Option>
                                    <Select.Option value={SimulationTreeKey.DESC}>내림차순</Select.Option>
                                </Select>
                                <br/>
                                <br/>
                                <Select style={{width: "100%", visibility: SimulationStore.getAlignOptionBtnVisible}}
                                        size={"default"} onChange={this.onChangeSortName}>
                                    <Select.Option value={SimulationItemType.INPUT_COUNT_FOR_HOUR}>생산량</Select.Option>
                                    <Select.Option value={SimulationItemType.NG_COUNT_FOR_HOUR}>불량건수</Select.Option>
                                </Select>
                            </Card>
                </div>
            </>
        );
    };
}

export default TimeOutput;
