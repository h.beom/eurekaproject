import React, {Component, CSSProperties, RefObject} from 'react';
import 'antd/dist/antd.css';
import {ChartBox, ChartContendBox, OptionButton, TitleText} from "../../store/SimStyle_Componet";
import {Button, Card, Col, Row, Select, Table, Tree} from "antd";
import {ColumnProps} from "antd/es/table";
import {inject, observer} from "mobx-react";
import {
    FacilityOperatingRateType,
    IFacilityOperatingDataSource,
    SimulationFormType,
    SimulationTreeKey
} from "../../store/SimulationType";
import autobind from "autobind-decorator";
import DataSourceHandler from "../../../../utile/DataSourceHandler";
import StringUtile from "../../../../utile/StringUtile";
import {
    ArgumentAxis,
    CommonSeriesSettings,
    Label,
    Legend,
    Series,
    ValueAxis,
    Tooltip,
    Size
} from "devextreme-react/chart";
import html2canvas from "html2canvas";
import {BrowserRouter, Link, Route, Switch} from "react-router-dom";
import Searvice from "../../../../common/Searvice";

/**
* @description SIMULATION 의 설비별 가용률을 담당하는 클래스
*/
@inject('SimulationStore', 'ExcelStore')
@observer
class FacilityOperatingRate extends Component<any, any> {


    constructor(props: any) {
        super(props);

        this.state = {
            chartPngDownloadData: "",
            fullscreenType: false,
            renderEvent: function (type: boolean) {
            }
        };
    }

    private _barChart: RefObject<any> = React.createRef();
    private textSortAttrName: string = "processNm";
    private offFullScreen: CSSProperties = {height: "calc(100% - 54px)"};
    private onFullScreen: CSSProperties = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
        zIndex: 999,
        backgroundColor: "#ffffff"
    };
    /**
     * @description 리스트에서 테이블 출력을 위한 컬럼표
     */
    private readonly _dataGridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [
        {
            title: '프로세스 명',
            dataIndex: 'processNm',
            key: 'processNm',
            width: '20%'
        },
        {
            title: 'Working',
            dataIndex: 'working',
            key: 'working',
            align: "right",
            width: '14%'
        },
        {
            title: 'Failure',
            dataIndex: 'failure',
            key: 'failure',
            align: "right",
            width: '14%'
        },
        {
            title: 'Waiting',
            dataIndex: 'waiting',
            key: 'waiting',
            align: "right",
            width: '14%'
        },
        {
            title: 'Blocking',
            dataIndex: 'blocking',
            key: 'blocking',
            align: "right",
            width: '14%'
        },
        {
            title: '합계',
            dataIndex: 'sum',
            key: 'sum',
            align: "right",
            width: '14%'
        }
    ];

    async componentDidMount() {
        const {SimulationStore} = this.props;
        this.setRenderEvent(this._renderEvent);
        SimulationStore.setFacilityOperatingChartLoading(true);
        if (SimulationStore.getFacilityOperatingRate == null) {
            let parameter: any = {
                simulationid: this.props.SimulationStore.getSimReqId,
                loclid: "KO"
            };

            const responseData: any = await Searvice.getPost("sim/getFacilityOperationgRate.do", parameter);
            const chartData: Array<IFacilityOperatingDataSource> = responseData.data.OperationgRateData;
            if (chartData !== undefined) {
                chartData.map(function (value: any) {
                    value.sum = value.working + value.waiting + value.blocking + value.failure
                });
                SimulationStore.setFacilityOperatingRate(chartData);
            }else{
                SimulationStore.setFacilityOperatingRate(null);
            }

        }

        const reseutData = SimulationStore.getFacilityOperatingRate;
        SimulationStore.setFacOperData(reseutData);

        SimulationStore.setAlignOptionBtnVisible("hidden");
        SimulationStore.setCheckedKeys(["all", "working", "failure", "waiting", "blocking"]);
        this.onChangeBtnAlignOption('name');
        SimulationStore.setSelectionView(SimulationFormType.CHART);
        SimulationStore.setFacilityOperatingChartLoading(false);
    }


    /**
     * @description 테이블(표)를 클릭시 출력하기위한 trigger
     * @param event 마우스 클릭시 이벤트
     */
    @autobind
    private onClickBtnChartTable(event: React.MouseEvent<HTMLButtonElement>): void {
        this.props.SimulationStore.setSelectionView(event.currentTarget.name);
    }

    @autobind
    private _renderEvent(type?: boolean): void {
        let barChart: any = this._barChart.current.instance;
        barChart.render();
        barChart.render();
    }

    /**
     * @description 정렬 선택시 추가 정렬( 숨김 | 보임 ) 을 결정하는 메서드
     * @param value 정렬( 숨김 | 보임 ) 을 결정하기 위해 결정할 문자열( ASC | DESC )
     */
    @autobind
    private onChangeBtnAlignOption(value: string): void {
        const {SimulationStore} = this.props;
        let alignOptionBtnVisible: "hidden" | "visible" = "hidden";
        if (value == SimulationTreeKey.ASC || value == SimulationTreeKey.DESC) {
            alignOptionBtnVisible = "visible";
        }
        let chartData: Array<any> = SimulationStore.getFacOperData;
        let resultData: Array<any> = DataSourceHandler.chartDataSort(chartData, value, SimulationStore.getFacilitySortName, this.textSortAttrName);
        SimulationStore.setFacOperData(resultData);
        SimulationStore.setAlignOptionBtnVisible(alignOptionBtnVisible);
        SimulationStore.setSortType(value);
    }

    /**
     * @description 해당 파라미터
     * @param value (SUM | WARNING | WORKING | BLOCKING | WAITING) 에대한 정보
     */
    @autobind
    private onChangeSortName(value: string): void {

        const {SimulationStore} = this.props;
        let chartData: Array<any> = SimulationStore.getFacOperData;
        let resultData: Array<any> = DataSourceHandler.chartDataSort(chartData, SimulationStore.getSortType, value, this.textSortAttrName);

        SimulationStore.setFacOperData(resultData);
        SimulationStore.setFacilitySortName(value);
    }

    /**
     * @description 차트의 데이터를 테이블(표)의 형식으로 출력하기위한 메서드
     * @param event 마우스 클릭시 이벤트
     */
    @autobind
    private onClickBtnTable(event: React.MouseEvent<HTMLButtonElement>): void {
        const {SimulationStore} = this.props;
        let chartData: Array<any> = this.getChartData();
        let resultData: Array<any> = DataSourceHandler.chartDataSort(chartData, SimulationStore.getSortType, SimulationStore.getFacilitySortName, this.textSortAttrName);
        SimulationStore.setTableData(resultData);
    }

    /**
     * @description (working | blocking | failure | waiting ) 체크한 값만을 차트에 표출하기 위한 메서드
     * @param checkValue 체크한 제품을 담은 값들
     */
    @autobind
    private getChartData(checkValue ?: any): Array<any> {
        const {SimulationStore} = this.props;
        let ckKey: Array<string> = Object.assign([], SimulationStore.getCheckedKeys);
        let list: any = Object.assign(SimulationStore.getFacilityOperatingRate, []);
        if (checkValue != undefined) {
            ckKey = checkValue;
        }
        let resultData: Array<IFacilityOperatingDataSource> = list.map((item: IFacilityOperatingDataSource) => {
            item = Object.assign({}, item);

            if (ckKey.indexOf("working") == -1) {
                item.working = 0;
            }

            if (ckKey.indexOf("blocking") == -1) {
                item.blocking = 0;
            }

            if (ckKey.indexOf("failure") == -1) {
                item.failure = 0;
            }

            if (ckKey.indexOf("waiting") == -1) {
                item.waiting = 0;
            }
            item.sum = StringUtile.setFloat({num: (item.working + item.blocking + item.failure + item.waiting)});
            return item;
        });
        return resultData;
    }


    /**
     * @description 체크한 작업요소 상태를 데이터출력
     * @param checkedKeys 상태선택 [[ISWorkState]]
     */
    @autobind
    private onCheck(checkedKeys: any): void {
        const {SimulationStore} = this.props;
        let chartData: Array<any> = this.getChartData(checkedKeys);
        let resultData: Array<any> = DataSourceHandler.chartDataSort(chartData, SimulationStore.getSortType, SimulationStore.getFacilitySortName, this.textSortAttrName);

        SimulationStore.setFacOperData(resultData);
        SimulationStore.setCheckedKeys(checkedKeys);
    };

    /**
     * @description 차트를 이미지로 저장하는 메서드
     * @param event 마우스 클릭 이벤트에대한 정보
     */
    @autobind
    private onClickChartDownload(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void {
        let element: HTMLElement | null = document.getElementById("FacilityOperatingRateChartContent");
        let hideDownloadLink: HTMLElement | null = document.getElementById("FacilityOperatingRateChartDownloadLink");
        if (element != null) {
            let setSteteCompleteEvent: () => void = function () {
                if (hideDownloadLink != null) {
                    hideDownloadLink.click();
                }
            }.bind(this);

            let callback: (this: FacilityOperatingRate, canvas: HTMLCanvasElement) => void = function (this: FacilityOperatingRate, canvas: HTMLCanvasElement) {
                this.setState({
                        chartPngDownloadData: canvas.toDataURL("image/png")
                    },
                    setSteteCompleteEvent);
            }.bind(this);
            html2canvas(element).then(callback);
        }
    }

    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function): void {
        this.setState({renderEvent: callBack});
    }

    /**
     * @description 버튼 클릭시 시뮬레이션정보를 전체화면으로 보는 메서드
     */
    @autobind
    private onClickEventFullScreen(): void {

        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;

        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }

    /** @description 차트 테이블을 엑셀파일로 다운로드 */
    @autobind
    private onClickBtnExcelDownload() {
        const {SimulationStore} = this.props;
        const simulationid = SimulationStore.getSimReqId;
        const checkedkeys = SimulationStore.getCheckedKeys;
        const sorttype = SimulationStore.getSortType;
        const sortname = SimulationStore.getFacilitySortName;


        this.props.ExcelStore.facilityoperatingrateExcel(simulationid, checkedkeys, sorttype, sortname);
    }

    render() {
        const {SimulationStore} = this.props;
        return (
            <>

                <div style={{width:'77%', float:'left'}}>
                    <a href={this.state.chartPngDownloadData} download={"TimeBufferUsage.png"}
                       style={{display: "none"}}
                       id="FacilityOperatingRateChartDownloadLink"></a>

                    <ChartBox
                        title={'설비별 가용률'}
                        type={"inner"}
                        bodyStyle={(this.state.fullscreenType == true) ? this.onFullScreen : this.offFullScreen}
                        extra={[
                            <OptionButton key="1" size={"small"}
                                          onClick={this.onClickEventFullScreen}>크게보기</OptionButton>,
                            <Switch>
                                <Route path={`${this.props.routerData.match.url}/table`}>
                                    <OptionButton key="2" size={"small"}
                                                  onClick={this.onClickBtnExcelDownload}>
                                        엑셀 다운로드
                                    </OptionButton>
                                </Route>
                                <Route path={`${this.props.routerData.match.url}`}>
                                    <OptionButton key="2" size={"small"}
                                                  onClick={this.onClickChartDownload}>
                                        차트 다운로드
                                    </OptionButton>
                                </Route>
                            </Switch>
                        ]}
                        loading={SimulationStore.getFacilityOperatingChartLoading}
                    >
                        <Switch>
                            <Route path={`${this.props.routerData.match.url}/table`}>
                                <div>
                                    {(this.state.fullscreenType == true) ?
                                        <Button key="1" size={"small"} onClick={this.onClickEventFullScreen}
                                                type={"primary"} style={{
                                            width: 200,
                                            height: 40,
                                            marginBottom: 70,
                                            marginTop: 5
                                        }}>원본크기</Button> : null}

                                    <Table columns={this._dataGridCoulumn}
                                           dataSource={SimulationStore.getFacOperData}
                                           scroll={{y: 460}}
                                           size={"middle"} bordered={true}/>
                                </div>
                            </Route>
                            <Route path={`${this.props.routerData.match.url}`}>
                                <TitleText id="FacilityOperatingRateChartContent">

                                    {(this.state.fullscreenType == true) ?
                                        <Button key="2" size={"small"} onClick={this.onClickEventFullScreen}
                                                type={"primary"}
                                                style={{width: 200, height: 40}}>원본크기</Button> : null}
                                    <ChartContendBox ref={this._barChart}
                                                     title={'설비별 생산량 / 불량건수'}
                                                     dataSource={SimulationStore.getFacOperData}
                                                     id={'chart'}>
                                        {(this.state.fullscreenType == true) ? <Size height={700}/> :
                                            <Size height={675} width={"100%"}/>}
                                        <ArgumentAxis>
                                            <Label
                                                customizeText={function (this: FacilityOperatingRate, info: any) {
                                                    if (info.valueText != "") {
                                                        return `${SimulationStore.getChartItemName[info.valueText]}`;
                                                    }
                                                    return `${info.valueText}`;
                                                }.bind(this)}/>
                                        </ArgumentAxis>

                                        <ValueAxis name={'percentage'} position={'left'} showZero={true}
                                                   valueMarginsEnabled={false}>
                                            <Label
                                                customizeText={function customizePercentageText(info: any) {
                                                    return `${info.valueText}%`;
                                                }}/>
                                        </ValueAxis>

                                        <Series name={"Working"} valueField={"working"}
                                                argumentField={"processId"}/>
                                        <Series name={"Failure"} valueField={"failure"}
                                                argumentField={"processId"}/>
                                        <Series name={"Waiting"} valueField={"waiting"}
                                                argumentField={"processId"}/>
                                        <Series name={"Blocking"} valueField={"blocking"}
                                                argumentField={"processId"}/>

                                        <Tooltip enabled={true} shared={true}/>

                                        <Legend verticalAlignment={'top'} horizontalAlignment={'right'}/>
                                        <CommonSeriesSettings argumentField={'processId'}
                                                              type={'stackedBar'}/>
                                    </ChartContendBox>
                                </TitleText>
                            </Route>
                        </Switch>
                    </ChartBox>
                </div>
                <div style={{width:' 22%', float:'right'}}>
                    <Card title={'결과양식'} type={"inner"}
                          style={{height: "110px"}}>
                        <Link to={`${this.props.routerData.match.url}`}>
                            <Button
                                style={{width: "calc(50% - 5px)", marginRight: "10px"}}
                                type={(SimulationStore.getSelectionView == SimulationFormType.CHART) ? "primary" : "default"}
                                size={"small"}
                                onClick={this.onClickBtnChartTable}
                                name={SimulationFormType.CHART}>Chart</Button>
                        </Link>
                        <Link to={`${this.props.routerData.match.url}/table`}>
                            <Button
                                style={{width: "calc(50% - 5px)"}}
                                type={(SimulationStore.getSelectionView == SimulationFormType.TABLE) ? "primary" : "default"}
                                size={"small"}
                                onClick={this.onClickBtnChartTable}
                                name={SimulationFormType.TABLE}>Table</Button>
                        </Link>
                    </Card>


                    <Card
                        title={'상태선택'}
                        type={"inner"}
                        style={{
                            height: "250px",
                            marginTop: "5px",
                            marginBottom: "10px"
                        }}
                        bodyStyle={{padding: "5px"}}>
                        <Tree
                            checkable
                            defaultExpandedKeys={['all']}
                            defaultSelectedKeys={['all']}
                            defaultCheckedKeys={['all']}
                            onCheck={this.onCheck}
                        >
                            <Tree.TreeNode title="All" key={"all"}>
                                <Tree.TreeNode title="Working" key={FacilityOperatingRateType.WORKING}/>
                                <Tree.TreeNode title="Failure" key={FacilityOperatingRateType.FAILURE}/>
                                <Tree.TreeNode title="Waiting" key={FacilityOperatingRateType.WAITING}/>
                                <Tree.TreeNode title="Blocking" key={FacilityOperatingRateType.BLOCKING}/>
                            </Tree.TreeNode>
                        </Tree>

                    </Card>
                    <Card title={'정렬'} type={"inner"}
                          className={'align'}
                          style={{
                              marginTop: "10px",
                          }}>
                        <Select defaultValue={SimulationTreeKey.NAME} style={{width: "100%"}}
                                size={'default'}
                                onChange={this.onChangeBtnAlignOption}>
                            <Select.Option value={SimulationTreeKey.NAME}>이름순</Select.Option>
                            <Select.Option value={SimulationTreeKey.ASC}>오름차순</Select.Option>
                            <Select.Option value={SimulationTreeKey.DESC}>내림차순</Select.Option>
                        </Select>
                        <br/>
                        <br/>
                        <Select defaultValue={SimulationStore.getFacilitySortName}
                                style={{
                                    width: "100%",
                                    visibility: SimulationStore.getAlignOptionBtnVisible
                                }}
                                size={'default'} onChange={this.onChangeSortName}>
                            <Select.Option value={FacilityOperatingRateType.SUM}>합계</Select.Option>
                            <Select.Option value={FacilityOperatingRateType.WORKING}>WORKING</Select.Option>
                            <Select.Option value={FacilityOperatingRateType.FAILURE}>FAILURE</Select.Option>
                            <Select.Option value={FacilityOperatingRateType.WAITING}>WAITING</Select.Option>
                            <Select.Option
                                value={FacilityOperatingRateType.BLOCKING}>BLOCKING</Select.Option>
                        </Select>
                        <br/>
                        <br/>
                    </Card>
                </div>
            </>
        );
    };
}

export default FacilityOperatingRate;
