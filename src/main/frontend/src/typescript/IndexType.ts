export interface MenuItemList{
    id: number,
    url:string,
    parentid:number,
    text:string,
    depth:number,
    icon?:string,
    visible:boolean,
    child:Array<MenuItemList>
};