import {Group} from "three";
import {IHtable} from "../ModelTs";
import Box3DObject from "../Box3DObject";

export default class HTable{

    private _ht:Group = new Group();

    //타입스크립트 만들기
    constructor(parameter:IHtable){

        this.htCreate(parameter);

        // 객체 만들기

        //

        // row 추가시...
        // column 추가시..

    }

    /** @description 안전책 객체(전역변수에 담긴 정보)를 반환한다. */
    public getGroup():Group{
        return this._ht;
    }

    private htCreate = (parameter:any) =>{
        // for(let num=0;  num < parameter.column; num++){
        let positionX: number = parameter.x + (parameter.x / 2);
        let positionY: number = parameter.y + (parameter.y / 2);
        let positionZ: number = parameter.z + (parameter.z / 2);

        let boxZ: number = parameter.depth / 2;
        let frameWidth: number = 5;
        let LacTop : Box3DObject = new Box3DObject({
            x: positionX + parameter.width/4,
            y: positionY,
            z: positionZ + (parameter.depth - parameter.depth/20),
            width: parameter.width/2,
            height: parameter.height,
            depth: parameter.depth / 40,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(LacTop.getGroup());

        let Lacmiddle : Box3DObject = new Box3DObject({
            x: positionX,
            y: positionY,
            z: positionZ + parameter.depth/2,
            width: parameter.width,
            height: parameter.height,
            depth: parameter.depth / 20,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(Lacmiddle.getGroup());

        let lacBridge: Box3DObject = new Box3DObject({
            x: positionX + parameter.width / 2 - frameWidth / 2,
            y: positionY + parameter.height / 2 - frameWidth / 2,
            z: positionZ + boxZ,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBridge.getGroup());

        let lacBridge2: Box3DObject = new Box3DObject({
            x: positionX + parameter.width / 2 - frameWidth / 2,
            y: positionY - parameter.height / 2 + frameWidth / 2,
            z: positionZ + boxZ,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBridge2.getGroup());

        let lacBridge3: Box3DObject = new Box3DObject({
            x: positionX - parameter.width / 2 + frameWidth / 2,
            y: positionY + parameter.height / 2 - frameWidth / 2,
            z: positionZ + boxZ/2,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth/2,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBridge3.getGroup());

        let lacBridge4: Box3DObject = new Box3DObject({
            x: positionX - parameter.width / 2 + frameWidth / 2,
            y: positionY - parameter.height / 2 + frameWidth / 2,
            z: positionZ + boxZ/2,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth/2,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBridge4.getGroup());

        let lacBottom1: Box3DObject = new Box3DObject({
            x: positionX + ((parameter.width/2) - ((parameter.width/20) - (parameter.width/40))),
            y: positionY,
            z: positionZ + parameter.depth / 10,
            width: parameter.width / 20,
            height: parameter.height,
            depth: parameter.depth / 20,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBottom1.getGroup());

        let lacBottom2: Box3DObject = new Box3DObject({
            x: positionX - (parameter.width/2) +((parameter.width/20) - (parameter.width/40)),
            y: positionY,
            z: positionZ + parameter.depth / 10,
            width: parameter.width / 20,
            height: parameter.height,
            depth: parameter.depth / 20,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBottom2.getGroup());

        let lacBottom3 : Box3DObject = new Box3DObject({
            x: positionX,
            y: positionY + (parameter.height/2) - ((parameter.height/20) - (parameter.height/40)),
            z: positionZ + parameter.depth/10,
            width: parameter.width,
            height: parameter.height/20,
            depth: parameter.depth / 20,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBottom3.getGroup());

        let lacBottom4 : Box3DObject = new Box3DObject({
            x: positionX,
            y: positionY - (parameter.height/2) + ((parameter.height/20) - (parameter.height/40)),
            z: positionZ + parameter.depth/10,
            width: parameter.width,
            height: parameter.height/20,
            depth: parameter.depth / 20,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._ht.add(lacBottom4.getGroup());
    }
    // }
}