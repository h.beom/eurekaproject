import {Group} from "three";
import {ILac} from "../I3DModel";
import Box3DObject from "../Box3DObject";
import autobind from "autobind-decorator";

export default class Lac3DObject{

    private _lac:Group = new Group();

    //타입스크립트 만들기
    constructor(parameter:ILac){

        this.lacCreate(parameter);

        // 객체 만들기

        //

        // row 추가시...
        // column 추가시..

    }

    /** @description 안전책 객체(전역변수에 담긴 정보)를 반환한다. */
    public getGroup():Group{
        return this._lac;
    }

    @autobind
    private lacCreate(parameter:any){
        // for(let num=0;  num < parameter.column; num++){
        let positionX: number = parameter.x + (parameter.x / 2);
        let positionY: number = parameter.y + (parameter.y / 2);
        let positionZ: number = parameter.z + (parameter.z / 2);

        let boxZ: number = parameter.depth / 2;
        let frameWidth: number = 5;
        let LacTop : Box3DObject = new Box3DObject({
            x: positionX,
            y: positionY,
            z: positionZ + parameter.depth,
            width: parameter.width,
            height: parameter.height,
            depth: parameter.depth / 20,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._lac.add(LacTop.getGroup());

        let lacBridge: Box3DObject = new Box3DObject({
            x: positionX + parameter.width / 2 - frameWidth / 2,
            y: positionY + parameter.height / 2 - frameWidth / 2,
            z: positionZ + boxZ,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._lac.add(lacBridge.getGroup());

        let lacBridge2: Box3DObject = new Box3DObject({
            x: positionX + parameter.width / 2 - frameWidth / 2,
            y: positionY - parameter.height / 2 + frameWidth / 2,
            z: positionZ + boxZ,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._lac.add(lacBridge2.getGroup());

        let lacBridge3: Box3DObject = new Box3DObject({
            x: positionX - parameter.width / 2 + frameWidth / 2,
            y: positionY + parameter.height / 2 - frameWidth / 2,
            z: positionZ + boxZ,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._lac.add(lacBridge3.getGroup());

        let lacBridge4: Box3DObject = new Box3DObject({
            x: positionX - parameter.width / 2 + frameWidth / 2,
            y: positionY - parameter.height / 2 + frameWidth / 2,
            z: positionZ + boxZ,
            width: parameter.width / 20,
            height: parameter.height / 20,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._lac.add(lacBridge4.getGroup());
    }
    // }
}