import {
    BoxGeometry,
    CanvasTexture,
    EdgesGeometry,
    Group,
    LineBasicMaterial,
    LineSegments,
    Mesh,
    MeshBasicMaterial,
    MeshLambertMaterial,
    MeshLambertMaterialParameters,
    PlaneBufferGeometry,
    RepeatWrapping,
    Texture
} from 'three';
import {IBox3DObject, ITextObject} from "./I3DModel";

export default class Box3DObject{

    private _axis:Group = new Group();
    private _textMash:Mesh | null = null;

    private _canvasRender2d:CanvasRenderingContext2D | null = null;

    private _boxMash:Mesh | null = null;
    private _lineMash:LineSegments | null = null;

    constructor( parameter:IBox3DObject ){
        let boxGeometry:BoxGeometry = new BoxGeometry ( 1,1,1);
        this.addBox(boxGeometry , 0, 0, 0 , parameter.width , parameter.depth , parameter.height , parameter.color  , parameter.opacity , parameter.texture);
        if(parameter.edgesVisible == true){
            if(parameter.borderColor == null){
                parameter.borderColor = 0x000000;
            }
            this.addEdges(boxGeometry, 0, 0, 0, parameter.width , parameter.depth , parameter.height , parameter.borderColor );
        }

        if(parameter.rotateX != undefined){
            this._axis.rotateX( parameter.rotateX * Math.PI / 180);
        }

        if(parameter.rotateY != undefined){
            this._axis.rotateY( parameter.rotateY * Math.PI / 180);
        }

        if(parameter.rotateZ != undefined){
            this._axis.rotateZ( parameter.rotateZ * Math.PI / 180);
        }


        if(parameter.textInfo != undefined ){
            let textInfo:ITextObject = parameter.textInfo;
            textInfo.fillStyle ={
                r:0,
                g:0,
                b:0,
            };
            textInfo.textAlign = "center";
            textInfo.textBaseline = "middle";
            textInfo.fontSize = 20;
            textInfo.fontface = "sans-serif";


            let ctx:CanvasRenderingContext2D | null = document.createElement("canvas").getContext("2d");
            if(ctx != null) {
                ctx.canvas.width = textInfo.width;
                ctx.canvas.height = textInfo.height;
                ctx.translate((textInfo.width/2), (textInfo.height/2 ) );
                ctx.fillStyle = `rgb(${textInfo.fillStyle.r},${textInfo.fillStyle.g},${textInfo.fillStyle.b})`;
                ctx.textAlign = textInfo.textAlign;  //CanvasTextAlign
                ctx.textBaseline = textInfo.textBaseline;  // CanvasTextBaseline
                ctx.font = textInfo.fontSize.toString()+"px "+textInfo.fontface; // string
                //→⇒≫▶
                ctx.fillText(textInfo.text, 0, 0);
                this._canvasRender2d = ctx;

                let texture:CanvasTexture = new CanvasTexture(this._canvasRender2d.canvas);
                texture.wrapS = RepeatWrapping;
                texture.wrapT = RepeatWrapping;
                texture.repeat.x = 1;
                texture.repeat.y = 1;

                let stripGeo:PlaneBufferGeometry = new PlaneBufferGeometry(parameter.width, parameter.height, 1 , 1);
                let stripMat:MeshBasicMaterial = new MeshBasicMaterial({
                    map: texture,
                    color:0xFFFFFF,
                    opacity: 1,
                    depthWrite: false,
                    depthTest: false,
                    transparent: true,
                });
                let textMash:Mesh = new Mesh(stripGeo, stripMat);
                textMash.rotation.x = -0.5 * Math.PI;
                textMash.position.set(0,(parameter.depth/2),0 );
                this._textMash = textMash;
                this._axis.add(textMash);
            }

        }

        this._axis.position.set( parameter.x , parameter.z , parameter.y );
    }

    public getGroup():Group{
        return this._axis;
    }

    private addBox(boxGeometry:BoxGeometry , x:number, y:number, z:number ,width:number , depth:number , height:number , color:number , opacity?:number , texture?:Texture  ):void{
        let zPosition:number = z;
        opacity = (opacity == undefined) ? 1 : opacity;
        let parame:MeshLambertMaterialParameters = {};
        parame.color = color;
        if(opacity != null){
            parame.transparent = true;
            parame.opacity= opacity;
        }
        if(texture != undefined){
            parame.map = texture;
            parame.color = undefined;
        }

        let leftBeltFrameMaterial:MeshLambertMaterial = new MeshLambertMaterial(parame);
        //, opacity:0.4 , transparent:true
        let leftBeltFrame:Mesh = new Mesh( boxGeometry , leftBeltFrameMaterial );
        leftBeltFrame.position.set ( x , y, zPosition );
        leftBeltFrame.scale.set( width, depth, height);

        this._boxMash = leftBeltFrame;

        this._axis.add(leftBeltFrame);
    }

    private addEdges( boxGeometry:BoxGeometry , x:number, y:number, z:number, width:number , depth:number , height:number , color:number ):void{
        let edges:EdgesGeometry = new EdgesGeometry( boxGeometry );
        let line:LineSegments = new LineSegments( edges, new LineBasicMaterial( { color: color , linewidth:10 } ) );
        line.position.set ( x, y, z);
        line.scale.set( width, depth, height);

        this._lineMash = line;

        this._axis.add(line);
    }

    public setSize( paramter:{width?:number, height?:number , depth?:number} ){
        if(this._boxMash == null){
            return;
        }

        if(paramter.width != undefined){
            this._boxMash.scale.x = paramter.width;
        }
        if(paramter.height != undefined){
            this._boxMash.scale.y= paramter.height;
        }
        if(paramter.depth != undefined){
            this._boxMash.scale.z = paramter.depth;
        }

        if(this._lineMash == null){
            return;
        }

        if(paramter.width != undefined){
            this._lineMash.scale.x = paramter.width;
        }
        if(paramter.height != undefined){
            this._lineMash.scale.y= paramter.height;
        }
        if(paramter.depth != undefined){
            this._lineMash.scale.z = paramter.depth;
        }
    }

    public setRotation(x:number ){
        //, y:number, z:number
        if(this._textMash != null){
            // this._textMash.rotation.x = -0.5 * Math.PI;
            // this._textMash.rotation.z = 1.57 * (x / 90);
        }
    }
}
