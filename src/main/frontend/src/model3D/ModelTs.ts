export interface IHtable{
    x:number,
    y:number,
    z:number,
    width:number,
    depth:number,
    height:number,
    edgesVisible?:number ,
    color?:number,
    borderColor?:number,
    rotateZ?:number,
    rotateY?:number
}