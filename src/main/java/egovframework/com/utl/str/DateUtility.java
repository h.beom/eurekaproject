package egovframework.com.utl.str;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateUtility {

	/**
	 * 이번달 날자 가져오기
	 * @author Dev.ywkim
	 * @return
	 */
	public static String getNowMonth(){

		String thisMonth = new SimpleDateFormat("yyyyMM" , Locale.getDefault()).format(Calendar.getInstance().getTime());

		return thisMonth;
	}
	/**
	 * 오늘날짜 가져오기
	 * @author Dev.ywkim
	 * @return
	 */
	public static String getTodayDate(String pattern){
		return new SimpleDateFormat(pattern , Locale.getDefault()).format(Calendar.getInstance().getTime());
	}

	/**
	 * 오늘날짜 가져오기
	 * @author Dev.ywkim
	 * @return
	 */
	public static String getTodayDate(){
		return new SimpleDateFormat("yyyy.MM.dd" , Locale.getDefault()).format(Calendar.getInstance().getTime());
	}

	/**
	 * 금일날자 가져오기
	 * @author Dev.ywkim
	 * @param pattern : 날자 포맷 패턴
	 * @return 오늘날자 문자열
	 */
	public static String getTodayString(String pattern){

		SimpleDateFormat formatter = new SimpleDateFormat (pattern);
		Calendar today = Calendar.getInstance();

		String todayStr = formatter.format(today.getTime());

		return todayStr;
	}
}
