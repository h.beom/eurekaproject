package egovframework.com.utl.excel;

import net.sf.jxls.transformer.XLSTransformer;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Map;

public class JxlsExcelView extends AbstractExcelView {
    private static final Logger logger = LoggerFactory.getLogger(JxlsExcelView.class);

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook,
                                      HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String fileName = "";
        if(StringUtils.isEmpty(( String ) model.get("fileName"))){
            logger.error("fileName is Empty");
            fileName = "testExcel.xls";
        }else{
            fileName = ( String )model.get("fileName");
        }

        String tempName = "";
        if(StringUtils.isEmpty(( String ) model.get("template"))){
            logger.error("Template Excel File is not Exist");
        }else{
            tempName = ( String ) model.get("template");
        }


        String tempPath = "/WEB-INF/exceltemplate/"+tempName;
        String tempRealPath = (request.getSession().getServletContext()).getRealPath(tempPath);

        setDisposition( fileName, request, response );
        InputStream template    = new FileInputStream(new File(tempRealPath));
        XLSTransformer transformer = new XLSTransformer();
        Map<String , Object> dataMap =  (Map)model.get("excelContent");
        HSSFWorkbook wb    = (HSSFWorkbook) transformer.transformXLS(template , dataMap);
        wb.createCellStyle().setWrapText(true);

        wb.write(response.getOutputStream());
    }

    /**
     * 브라우저 구분 얻기.
     *
     * @param request
     * @return
     */
    public String getBrowser(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        if (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1) {
            return "MSIE";
        } else if (header.indexOf("Chrome") > -1) {
            return "Chrome";
        } else if (header.indexOf("Opera") > -1) {
            return "Opera";
        }
        return "Firefox";
    }
    /**
     * Disposition 지정하기.
     *
     * @param filename
     * @param request
     * @param response
     * @throws Exception
     */
    public void setDisposition(String filename, HttpServletRequest request,
                               HttpServletResponse response) throws IOException {
        String browser = getBrowser(request);
        String dispositionPrefix = "attachment; filename=";
        String encodedFilename = null;
        if (browser.equals("MSIE")) {
            encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
        } else if (browser.equals("Firefox")) {
            encodedFilename = "\""+ new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
        } else if (browser.equals("Opera")) {
            encodedFilename = "\""+ new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
        } else if (browser.equals("Chrome")) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < filename.length(); i++) {
                char c = filename.charAt(i);
                if (c > '~') {
                    sb.append(URLEncoder.encode("" + c, "UTF-8"));
                } else {
                    sb.append(c);
                }
            }
            encodedFilename = sb.toString();
        } else {
            throw new IOException("Not supported browser");
        }
        response.setHeader("Content-Disposition", dispositionPrefix+ encodedFilename);
        response.setHeader("Content-Type", "application/vnd.ms-excel");
        if ("Opera".equals(browser)) {
            response.setContentType("application/octet-stream;charset=UTF-8");
        }
    }
}
