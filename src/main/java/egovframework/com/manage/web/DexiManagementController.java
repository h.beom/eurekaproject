package egovframework.com.manage.web;

import egovframework.com.manage.service.ManagementListService;
import egovframework.com.manage.service.manageCriteria;
import egovframework.com.manage.service.manageVO;
import egovframework.com.sim.service.criteria.SimulationViewCriteria;
import egovframework.com.utl.excel.JxlsExcelView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Controller
public class DexiManagementController {

    @Resource(name = "managementListService")
    ManagementListService managementListService;

    @Resource(name = "jxlsExcelView")
    private JxlsExcelView jxlsExcelView;

    //조회
    //부서 목록 정보 가져오기
    @RequestMapping(value = "manage/getManageList.do", method = RequestMethod.POST)
    public ModelAndView getManageList(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", managementListService.getManageList(criteria));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    //유저코드리스트 가져오기
    @RequestMapping(value = "manage/getUserCodeList.do", method = RequestMethod.POST)
    public ModelAndView getUserCodeList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", managementListService.getUserCodeList());
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    //전체 직원 목록 정보 가져오기
    @RequestMapping(value = "manage/getManageUserList.do", method = RequestMethod.POST)
    public ModelAndView getManageUserList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", managementListService.getManageUserList());
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    //추가
    //부서 추가
    @RequestMapping(value = "manage/addDeptInfo.do", method = RequestMethod.POST)
    public ModelAndView addDeptInfo(@RequestBody manageVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("addDeptInfo comfirming....");
        UUID one = UUID.randomUUID();
        if(vo.getDeptId() == null){
            System.out.println("possible");
            vo.setDeptId(one.toString());
        }
        System.out.println(vo.getDeptAtCd() + "::::" + vo.getDeptNm() + "::::" + vo.getUpDeptId() + ":::::" + vo.getDeptId());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        managementListService.addDeptInfo(vo);
        return modelAndView;
    }

//삭제
    //부서삭제
@RequestMapping(value = "manage/delManageList.do", method = RequestMethod.POST)
public ModelAndView delManageList(@RequestBody manageVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("jsonView");
    System.out.println("delManageList confirming......");
    System.out.println(vo.getCheckedDept());
    managementListService.delManageList(vo);
    return modelAndView;
}
//업데이트
    //부서에서 직원빼기
@RequestMapping(value = "manage/DelDepNm.do", method = RequestMethod.POST)
public ModelAndView DelDepNm(@RequestBody manageVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("jsonView");
    System.out.println("DelDepNm confirming......");
    System.out.println(vo.getCheckedDept());
    managementListService.DelDepNm(vo);
    return modelAndView;
}
    //부서로 직원 추가
    @RequestMapping(value = "manage/addDeptNm.do", method = RequestMethod.POST)
    public ModelAndView addDeptNm(@RequestBody manageVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        System.out.println("addDeptNm confirming......");
        System.out.println(vo.getUserId());
        managementListService.addDeptNm(vo);
        return modelAndView;
    }
//    ::::::::::::::::::::::::::::::::::::::::::::::::::::role.tsx::::::::::::::::::::::::::::::::::::::::::::::::::::
@RequestMapping(value = "manage/searchRole.do", method = RequestMethod.POST)
public ModelAndView searchRole(@RequestBody manageCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("jsonView");
    try {
        modelAndView.addObject("list", managementListService.searchRole(vo));
    } catch (Exception e) {
        System.out.println("test:" + e);
    }
    return modelAndView;
}

    @RequestMapping(value = "manage/searchDeptList.do", method = RequestMethod.POST)
    public ModelAndView searchDeptList(@RequestBody manageCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        System.out.print(vo.getLoclId());
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", managementListService.searchDeptList(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }
    @RequestMapping(value = "manage/searchDeptMember.do", method = RequestMethod.POST)
    public ModelAndView searchDeptMember(@RequestBody manageCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        System.out.print(vo.getLoclId());
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", managementListService.searchDeptMember(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }
    @RequestMapping(value = "manage/getManageOtherUser.do", method = RequestMethod.POST)
    public ModelAndView getManageOtherUser(@RequestBody manageCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        System.out.print(vo.getLoclId());
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", managementListService.getManageOtherUser(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }
    @RequestMapping(value = "manage/checkDeptNm.do", method = RequestMethod.POST)
    public ModelAndView checkDeptNm(@RequestBody manageVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        manageVO vo2 = managementListService.checkDeptNm(vo);
        System.out.println(vo2+"::::::");
        try {
            if(vo2 == null){
                modelAndView.addObject("list", vo2);
            }
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }
}

