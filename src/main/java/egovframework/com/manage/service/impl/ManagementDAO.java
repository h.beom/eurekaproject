package egovframework.com.manage.service.impl;


import egovframework.com.cmm.service.impl.DexiComAbstractDAO;
import egovframework.com.code.service.SysCodeVO;
import egovframework.com.manage.service.manageCriteria;
import egovframework.com.manage.service.manageVO;
import egovframework.com.sim.service.criteria.SimulationViewCriteria;
import egovframework.com.user.service.vo.UserListVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("managementDAO")
public class ManagementDAO extends DexiComAbstractDAO {


    public List<manageVO> getManageList(SimulationViewCriteria criteria) throws Exception{
        return selectList("managementDAO.getManageList", criteria);
    }

    public List<UserListVO> getManageUserList() throws Exception{
        return selectList("managementDAO.getManageUserList");
    }
    public List<SysCodeVO> getUserCodeList() throws Exception{
        return selectList("managementDAO.getUserCodeList");
    }
    public void addDeptInfo1(manageVO vo) throws Exception{
        insert("managementDAO.addDeptInfo1", vo);
    }
    public void addDeptInfo2(manageVO vo) throws Exception{
        insert("managementDAO.addDeptInfo2", vo);
    }
    public void delSysCodeList1(manageVO vo) throws Exception{
        delete("managementDAO.delSysCodeList1", vo);
    }
    public void delSysCodeList2(manageVO vo) throws Exception{
        delete("managementDAO.delSysCodeList2", vo);
    }
    public void delManageList(manageVO vo) throws Exception{
        delete("managementDAO.delManageList", vo);
    }
    public void DelDepNm(manageVO vo) throws Exception{
        update("managementDAO.DelDepNm", vo);
    }
    public void addDeptNm(manageVO vo) throws Exception{
        update("managementDAO.addDeptNm", vo);
    }
    public List<UserListVO> searchRole(manageCriteria vo) throws Exception{
        return selectList("managementDAO.searchRole", vo);
    }
    public List<UserListVO> searchDeptList(manageCriteria vo) throws Exception{
        return selectList("managementDAO.searchDeptList", vo);
    }
    public List<UserListVO> searchDeptMember(manageCriteria vo) throws Exception{
        return selectList("managementDAO.searchDeptMember", vo);
    }
    public List<UserListVO> getManageOtherUser(manageCriteria vo) throws Exception{
        return selectList("managementDAO.getManageOtherUser", vo);
    }
    public manageVO checkDeptNm(manageVO vo) throws Exception{
        return selectOne("managementDAO.checkDeptNm", vo);
    }
}