package egovframework.com.manage.service;

import egovframework.com.code.service.SysCodeVO;
import egovframework.com.sim.service.criteria.SimulationViewCriteria;
import egovframework.com.user.service.vo.UserListVO;

import java.util.List;

public interface ManagementListService {
    public List<manageVO> getManageList(SimulationViewCriteria criteria) throws Exception;
    public List<UserListVO> getManageUserList() throws Exception;
    public List<SysCodeVO> getUserCodeList() throws Exception;
    public void addDeptInfo(manageVO vo) throws Exception;
    public void delSysCodeList(manageVO vo) throws Exception;
    public void delManageList(manageVO vo) throws Exception;
    public void DelDepNm(manageVO vo) throws Exception;
    public void addDeptNm(manageVO vo) throws Exception;
    public List<UserListVO> searchRole(manageCriteria vo) throws Exception;
    public List<UserListVO> searchDeptList(manageCriteria vo) throws Exception;
    public List<UserListVO> searchDeptMember(manageCriteria vo) throws Exception;
    public List<UserListVO> getManageOtherUser(manageCriteria vo) throws Exception;
    public manageVO checkDeptNm(manageVO vo) throws Exception;

}