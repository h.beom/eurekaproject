package egovframework.com.dash.service.impl;

import egovframework.com.cmm.service.impl.DexiComAbstractDAO;
import egovframework.com.dash.service.criteria.DashBoard3DCriteria;
import egovframework.com.dash.service.criteria.DashBoardCriteria;
import egovframework.com.dash.service.criteria.KpiSearchCriteria;
import egovframework.com.dash.service.criteria.SimulationCriteria;
import egovframework.com.dash.service.vo.*;
import egovframework.com.sim.service.vo.ProcessMapInfoVO;
import egovframework.com.sim.service.vo.ProcessModelInfoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * 시뮬레이션 리스트 DAO 클래스를 정의한다.
 *
 * @author 고동현
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 * @since 2019.04.13
 */
@Component
@Repository("dashboardDAO")
public class DashboardDAO extends DexiComAbstractDAO {

    /**
     * 로깅 처리
     */
    private static final Logger log = LoggerFactory.getLogger(DashboardDAO.class);


    public List<MapLocationVO> selectMapLocation() {
        return selectList("dashBoardDAO.selectMapLocation");
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @return 공장 상태메세지 TIMELINE에 필요한 정보
     */
    public List<FactoryMessageVO> selectFactoryMessage(DashBoardCriteria criteria) {
        return selectList("dashBoardDAO.selectFactoryMessage", criteria);
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * Global Map 상 KPI 조회
     *
     * @param criteria
     * @return
     * @throws Exception
     */
    public GlobalMapVO selectGlobalMap(DashBoardCriteria criteria) {
        List<List<?>> result = selectList("dashBoardDAO.selectGlobalMap", criteria);

        GlobalMapVO globalMapVo = new GlobalMapVO();

        List<KpiInfoVO> kpiInfoVo = new ArrayList<KpiInfoVO>();
        try {
            kpiInfoVo = (List<KpiInfoVO>) result.get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        List<GlobalKpiDataVO> globalKpiData = new ArrayList<GlobalKpiDataVO>();
        try {
            globalKpiData = (List<GlobalKpiDataVO>) result.get(1);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        globalMapVo.setKpiInfo(kpiInfoVo);
        globalMapVo.setGlobalKpiData(globalKpiData);

        return globalMapVo;
    }


//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒


    /**
     * Factory Map 상 KPI 조회
     *
     * @param criteria
     * @return
     * @throws Exception
     */
    public FactoryMapVO selectFactorylMap(DashBoardCriteria criteria) {
        List<List<?>> result = selectList("dashBoardDAO.selectFactorylMap", criteria);

        FactoryMapVO factoryMapVO = new FactoryMapVO();


        List<FactoryInfoVO> factoryInfoVO = new ArrayList<FactoryInfoVO>();
        try {
            factoryInfoVO = (List<FactoryInfoVO>) result.get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }


        List<KpiInfoVO> kpiInfoVo = new ArrayList<KpiInfoVO>();
        try {
            kpiInfoVo = (List<KpiInfoVO>) result.get(1);
        } catch (Exception e) {
            log.error(e.getMessage());
        }


        List<KpiDataVO> kpiDataVO = new ArrayList<KpiDataVO>();
        try {
            kpiDataVO = (List<KpiDataVO>) result.get(2);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        factoryMapVO.setFactoryInfo(factoryInfoVO);
        factoryMapVO.setKpiInfo(kpiInfoVo);
        factoryMapVO.setKpiData(kpiDataVO);

        return factoryMapVO;
    }


//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @param criteria
     * @return
     */
    public List<Monitor3DVO> getFactory3DInfo(DashBoard3DCriteria criteria) {
        return selectList("dashBoardDAO.getFactory3DInfo", criteria);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * 해당 시뮬레이션 맵정보 가져오기
     *
     * @param criteria mapId
     * @return
     */
    public List<ProcessMapInfoVO> getSimulation(SimulationCriteria criteria) {
        return selectList("dashBoardDAO.getSimulation", criteria);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * 해당 시뮬레이션 모델정보 가져오기
     *
     * @param criteria mapId
     * @return
     */
    public List<ProcessModelInfoVO> getModel(SimulationCriteria criteria) {
        return selectList("dashBoardDAO.getModel", criteria);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * KPI Master List
     *
     * @return
     */
    public List<KpiMasterVO> getKpiMaster() {
        return selectList("dashBoardDAO.getKpiMaster");
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * KPI Master 추가
     */
    public void insertKpiMaster(KpiMasterVO vo) {

        insert("dashBoardDAO.insertKpiMaster", vo);
    }


    @Scheduled(cron="0 0 05 * * ?")
    public void insertBatch() {
        insert("dashBoardDAO.insertBatch");
    }


    /**
     * KPI Master 삭제
     */
    public void deleteKpiMaster(DeleteDataVO vo) {
        insert("dashBoardDAO.deleteKpiMaster", vo);
    }

    /**
     * KPI Update 수정
     */
    public int updateKpiMaster(KpiMasterVO vo) {

       return update("dashBoardDAO.updateKpiMaster", vo);
    }

    /**
     * KPI Record 조회(Depth = 1)
     */
    public List<KpiRecordVO> getKpiRecord(SimulationCriteria vo) {
        return selectList("dashBoardDAO.getKpiRecord",vo);
    }

    /**
     * KPI Record Search
     */
    public List<KpiSearchVO> getSearch(KpiSearchCriteria vo) {
        return selectList("dashBoardDAO.getSearch", vo);
    }

    /**
     * KPI Record Update 수정
     */
    public void updateKpiRecord(KpiRecordVO vo) {

        insert("dashBoardDAO.updateKpiRecord", vo);
    }
}