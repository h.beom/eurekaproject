package egovframework.com.dash.service.vo;

public class KpiSearchVO {
    private String id;
    private String type;
    private String name;
    private String hirerachyName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHirerachyName() {
        return hirerachyName;
    }

    public void setHirerachyName(String hirerachyName) {
        this.hirerachyName = hirerachyName;
    }
}
