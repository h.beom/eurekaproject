package egovframework.com.dash.service.vo;

public class FactoryInfoVO {
    private String ID;
    private String P_ID;
    private String Type;
    private String Name;
    private String Depth;
    private String Location;
    private String MapLocation;
    private String Latitude;
    private String Longitude;
    private String ModelID;
    private String MapID;
    private String Description;
    private String HirerachyID;
    private String HirerachyName;
    private String CompanyName;
    private String CompnayID;
    private String FactoryName;
    private String FactoryID;
    private String BuildingName;
    private String BuildingID;
    private String FloorName;
    private String FloorID;
    private String LineName;
    private String LineID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getP_ID() {
        return P_ID;
    }

    public void setP_ID(String p_ID) {
        P_ID = p_ID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDepth() {
        return Depth;
    }

    public void setDepth(String depth) {
        Depth = depth;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getMapLocation() {
        return MapLocation;
    }

    public void setMapLocation(String mapLocation) {
        MapLocation = mapLocation;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getModelID() {
        return ModelID;
    }

    public void setModelID(String modelID) {
        ModelID = modelID;
    }

    public String getMapID() {
        return MapID;
    }

    public void setMapID(String mapID) {
        MapID = mapID;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getHirerachyID() {
        return HirerachyID;
    }

    public void setHirerachyID(String hirerachyID) {
        HirerachyID = hirerachyID;
    }

    public String getHirerachyName() {
        return HirerachyName;
    }

    public void setHirerachyName(String hirerachyName) {
        HirerachyName = hirerachyName;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getCompnayID() {
        return CompnayID;
    }

    public void setCompnayID(String compnayID) {
        CompnayID = compnayID;
    }

    public String getFactoryName() {
        return FactoryName;
    }

    public void setFactoryName(String factoryName) {
        FactoryName = factoryName;
    }

    public String getFactoryID() {
        return FactoryID;
    }

    public void setFactoryID(String factoryID) {
        FactoryID = factoryID;
    }

    public String getBuildingName() {
        return BuildingName;
    }

    public void setBuildingName(String buildingName) {
        BuildingName = buildingName;
    }

    public String getBuildingID() {
        return BuildingID;
    }

    public void setBuildingID(String buildingID) {
        BuildingID = buildingID;
    }

    public String getFloorName() {
        return FloorName;
    }

    public void setFloorName(String floorName) {
        FloorName = floorName;
    }

    public String getFloorID() {
        return FloorID;
    }

    public void setFloorID(String floorID) {
        FloorID = floorID;
    }

    public String getLineName() {
        return LineName;
    }

    public void setLineName(String lineName) {
        LineName = lineName;
    }

    public String getLineID() {
        return LineID;
    }

    public void setLineID(String lineID) {
        LineID = lineID;
    }
}
