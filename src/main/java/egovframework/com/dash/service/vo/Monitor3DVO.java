package egovframework.com.dash.service.vo;

public class Monitor3DVO {
    private String modelItemId;
    private String upModelItemId;
    private String modelItemNm;
    private String mapId;
    private String bgImgPath;
    private int lvl;

    public String getModelItemId() {
        return modelItemId;
    }

    public void setModelItemId(String modelItemId) {
        this.modelItemId = modelItemId;
    }

    public String getUpModelItemId() {
        return upModelItemId;
    }

    public void setUpModelItemId(String upModelItemId) {
        this.upModelItemId = upModelItemId;
    }

    public String getModelItemNm() {
        return modelItemNm;
    }

    public void setModelItemNm(String modelItemNm) {
        this.modelItemNm = modelItemNm;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getBgImgPath() {
        return bgImgPath;
    }

    public void setBgImgPath(String bgImgPath) {
        this.bgImgPath = bgImgPath;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }
}
