package egovframework.com.dash.service.vo;

public class KpiInfoVO {
    private String KPIID ;
    private String KPIName ;
    private String UIType;
    private String Unit;


    public String getKPIID() {
        return KPIID;
    }

    public void setKPIID(String KPIID) {
        this.KPIID = KPIID;
    }

    public String getKPIName() {
        return KPIName;
    }

    public void setKPIName(String KPIName) {
        this.KPIName = KPIName;
    }

    public String getUIType() {
        return UIType;
    }

    public void setUIType(String UIType) {
        this.UIType = UIType;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }
}

