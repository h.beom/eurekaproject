package egovframework.com.dash.service.vo;

import java.util.List;

public class FactoryMapVO {
    private List<FactoryInfoVO> factoryInfo;
    private List<KpiInfoVO> kpiInfo;
    private List<KpiDataVO> kpiData;

    public List<FactoryInfoVO> getFactoryInfo() {
        return factoryInfo;
    }

    public void setFactoryInfo(List<FactoryInfoVO> factoryInfo) {
        this.factoryInfo = factoryInfo;
    }

    public List<KpiInfoVO> getKpiInfo() {
        return kpiInfo;
    }

    public void setKpiInfo(List<KpiInfoVO> kpiInfo) {
        this.kpiInfo = kpiInfo;
    }

    public List<KpiDataVO> getKpiData() {
        return kpiData;
    }

    public void setKpiData(List<KpiDataVO> kpiData) {
        this.kpiData = kpiData;
    }
}
