package egovframework.com.dash.service;

import egovframework.com.dash.service.criteria.DashBoard3DCriteria;
import egovframework.com.dash.service.criteria.DashBoardCriteria;
import egovframework.com.dash.service.criteria.KpiSearchCriteria;
import egovframework.com.dash.service.criteria.SimulationCriteria;
import egovframework.com.dash.service.vo.*;
import egovframework.com.sim.service.vo.ProcessMapInfoVO;
import egovframework.com.sim.service.vo.ProcessModelInfoVO;

import java.util.List;

/**
 * 시뮬레이션 리스트 인터페이스 클래스를 정의한다.
 *
 * @author 고동현
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 * @since 2019.04.13
 */
public interface DashBoardService {

    /**
     * @return 전체 공장들의 좌표출력 정보
     * @throws Exception
     */
    public List<MapLocationVO> selectMapLocation() throws Exception;
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @return 공장 상태메세지 TIMELINE에 필요한 정보
     * @throws Exception
     */
    public List<FactoryMessageVO> selectFactoryMessage(DashBoardCriteria criteria) throws Exception;
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @param criteria Day, UserID
     * @return DashBoard에서 필요한 MAP과 KPI정보들
     * @throws Exception
     */
    public GlobalMapVO selectGlobalMap(DashBoardCriteria criteria) throws Exception;
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @param criteria Day, FactoryID, FactoryType, UserID
     * @return KPI정보들을 반환한다.
     * @throws Exception
     */
    public FactoryMapVO selectFactorylMap(DashBoardCriteria criteria) throws Exception;
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * @param criteria ModelId, level
     * @return KPI정보들을 반환한다.
     * @throws Exception
     */
    public List<Monitor3DVO> getFactory3DInfo(DashBoard3DCriteria criteria) throws Exception;
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @param criteria mapId
     * @return 해당 3D map, model, animation 정보호출 반환
     * @throws Exception
     */
    public List<ProcessMapInfoVO> getSimulation(SimulationCriteria criteria) throws Exception;
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @param criteria mapId
     * @return 해당 시뮬레이션 모델정보 반환
     * @throws Exception
     */
    public List<ProcessModelInfoVO> getModel(SimulationCriteria criteria) throws Exception;
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @return KPI Master List
     * @throws Exception
     */
    public List<KpiMasterVO> getKpiMaster() throws Exception;

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @return KPI Master Insert
     * @throws Exception
     */

    public void insertKpiMaster(KpiMasterVO vo) throws Exception;
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return KPI Master Delete
     * @throws Exception
     */

    public void deleteKpiMaster(DeleteDataVO vo) throws Exception;
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return KPI Master Update
     * @throws Exception
     */

    public int updateKpiMaster(KpiMasterVO vo) throws Exception;

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return KPI Record List
     * @throws Exception
     */
    public List<KpiRecordVO> getKpiRecord(SimulationCriteria vo) throws Exception;

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @return KPI Record Search
     * @throws Exception
     */
    public List<KpiSearchVO> getSearch(KpiSearchCriteria vo) throws Exception;

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return KPI Record Update
     * @throws Exception
     */

    public void updateKpiRecord(KpiRecordVO vo) throws Exception;

}


//updateKpiRecord