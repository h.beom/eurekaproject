package egovframework.com.dash.service.criteria;

public class DashBoardMainCriteria{
    private String companyid;
    private String date;

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
