package egovframework.com.dash.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import egovframework.com.dash.service.DashBoardService;
import egovframework.com.dash.service.criteria.DashBoard3DCriteria;
import egovframework.com.dash.service.criteria.DashBoardCriteria;
import egovframework.com.dash.service.criteria.KpiSearchCriteria;
import egovframework.com.dash.service.criteria.SimulationCriteria;
import egovframework.com.dash.service.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 시뮬레이션 Controller
 * <p>
 * 수정일         수정자         수정내용
 * -------        -------     -------------------
 * 2019.03.12	고동현		최초생성
 *
 * @author 고동현
 * @version 1.0
 * @since 2019.03.12
 */
@JsonIgnoreProperties
@Controller
public class DexiDashBoardController {

    @Resource(name = "dashboardService")
    DashBoardService dashBoardService;

    /**
     * 로깅 처리
     */
    private static final Logger log = LoggerFactory.getLogger(DexiDashBoardController.class);

    /**
     * Global Map 상 전체 공장들 좌표 및 정보 가져오기
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "dash/selectMapLocation.do", method = RequestMethod.POST)
    public ModelAndView selectMapLocation() throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {
            modelAndView.addObject("mapLocation", dashBoardService.selectMapLocation());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return modelAndView;
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * 공장 상태메세지 TIMELINE에 필요한 정보
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "dash/selectFactoryMessage.do", method = RequestMethod.POST)
    public ModelAndView selectFactoryMessage(@RequestBody DashBoardCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {
            modelAndView.addObject("factoryMessage", dashBoardService.selectFactoryMessage(criteria));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return modelAndView;
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * Global Map 상 KPI 조회  (수정 2차)
     *
     * @param criteria
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "dash/selectGlobalMap.do", method = RequestMethod.POST)
    public ModelAndView selectGlobalMap(@RequestBody DashBoardCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {
            GlobalMapVO globalMapVo = dashBoardService.selectGlobalMap(criteria);

            modelAndView.addObject("kpiInfo", globalMapVo.getKpiInfo());
            modelAndView.addObject("globalKpiData", globalMapVo.getGlobalKpiData());

        } catch (Exception e) {
            log.error(e.getMessage());
        }


        return modelAndView;
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
        /**
         *  공장, 빌딩, 라인 KPI 조회  (수정 2차)
         * @param criteria
         * @return
         * @throws Exception
         */
        @RequestMapping(value = "dash/selectFactorylMap.do", method = RequestMethod.POST)
        public ModelAndView selectFactorylMap(@RequestBody DashBoardCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("jsonView");

            try {
                FactoryMapVO factoryMapVO = dashBoardService.selectFactorylMap(criteria);

                modelAndView.addObject("factoryInfo", factoryMapVO.getFactoryInfo());
                modelAndView.addObject("kpiInfo", factoryMapVO.getKpiInfo());
                modelAndView.addObject("kpiData", factoryMapVO.getKpiData());

            } catch (Exception e) {
                log.error(e.getMessage());
            }

            return modelAndView;
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * 공장, 라인 등 3D 정보 호출
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "dash/factory3DInfo.do", method = RequestMethod.POST)
    public ModelAndView getFactoryThreeInfo(@RequestBody DashBoard3DCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {
            modelAndView.addObject("factoryMessage", dashBoardService.getFactory3DInfo(criteria));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return modelAndView;
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * 해당 3D map, model, animation 정보호출
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "dash/getSimulation.do", method = RequestMethod.POST)
    public ModelAndView getSimulation(@RequestBody SimulationCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {
            modelAndView.addObject("simulationMap", dashBoardService.getSimulation(criteria));
            modelAndView.addObject("simulationModel", dashBoardService.getModel(criteria));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return modelAndView;
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Master List
     * @return KPI Master List
     * @throws Exception
     */
    @RequestMapping(value = "dash/getKpiMaster.do", method = RequestMethod.POST)
    public ModelAndView getKpiMaster(@RequestBody SimulationCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {

            modelAndView.addObject("kpiMasterList", dashBoardService.getKpiMaster());

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return modelAndView;
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Master Insert
     * @throws Exception
     */
    @RequestMapping(value = "dash/insertKpiMaster.do", method = RequestMethod.POST)
    public ModelAndView insertKpiMaster(@RequestBody KpiMasterVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            dashBoardService.insertKpiMaster(vo);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return modelAndView;
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Master Delete
     * @throws Exception
     */
    @RequestMapping(value = "dash/deleteKpiMaster.do", method = RequestMethod.POST)
    public ModelAndView deleteKpiMaster(@RequestBody DeleteDataVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {

            dashBoardService.deleteKpiMaster(vo);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return modelAndView;
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Master Update
     * @throws Exception
     */
    @RequestMapping(value = "dash/updateKpiMaster.do", method = RequestMethod.POST)
    public ModelAndView updateKpiMaster(@RequestBody KpiMasterVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            int result = dashBoardService.updateKpiMaster(vo);
            if(result == 1){
                modelAndView.addObject("result", true);
            }else{
                modelAndView.addObject("result", false);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return modelAndView;
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Record List
     * @return KPI Record List
     * @throws Exception
     */
    @RequestMapping(value = "dash/getKpiRecord.do", method = RequestMethod.POST)
    public ModelAndView getKpiRecord(@RequestBody SimulationCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {

            modelAndView.addObject("kpiRecord", dashBoardService.getKpiRecord(vo));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return modelAndView;
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Record List
     * @return KPI get Search Record List
     * @throws Exception
     */
    @RequestMapping(value = "dash/getSearch.do", method = RequestMethod.POST)
    public ModelAndView getSearch(@RequestBody KpiSearchCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {

            modelAndView.addObject("kpiSearch", dashBoardService.getSearch(vo));
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return modelAndView;
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     * KPI Record Update
     * @throws Exception
     */
    @RequestMapping(value = "dash/updateKpiRecord.do", method = RequestMethod.POST)
    public ModelAndView updateKpiRecord(@RequestBody KpiRecordVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {

            dashBoardService.updateKpiRecord(vo);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return modelAndView;
    }
}
