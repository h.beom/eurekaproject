package egovframework.com.user.service.impl;

import egovframework.com.cmm.service.impl.DexiComAbstractDAO;
import egovframework.com.user.service.vo.UserCriteria;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.com.user.service.vo.idCheckVO;
import org.springframework.stereotype.Repository;

/**
 * 시뮬레이션 리스트 DAO 클래스를 정의한다.
 *
 * @author 고동현
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 * @since 2019.04.13
 */
@Repository("userDAO")
public class UserDAO extends DexiComAbstractDAO {

    public void registerUser(UserListVO vo) {
        insert("userDAO.registerUser", vo);
    }

    public int idCheck(UserCriteria vo) {
        return selectOne("userDAO.idCheck", vo);

    }

    /**
     * @param vo
     * @return 로그인 정보가 일치할시 1 반환
     */
    public boolean userLogin(UserCriteria vo) {
        String name = selectOne("userDAO.userLogin", vo);
        return (Integer.parseInt(name)==0)?false:true;
    }

}