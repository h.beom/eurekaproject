package egovframework.com.model.service;

import java.util.List;

/**
 * 시뮬레이션 리스트 인터페이스 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
public interface ModelListService {
	public List<ModelListVO> select3DModelList(ModelListCriteria criteria) throws Exception;
}