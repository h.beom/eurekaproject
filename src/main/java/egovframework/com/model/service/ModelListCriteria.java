package egovframework.com.model.service;

public class ModelListCriteria {
    private String search;
    private String modeltype;
    private String loclid;

    public String getLoclid() {
        return loclid;
    }

    public void setLoclid(String loclid) {
        this.loclid = loclid;
    }

    public String getModeltype() {
        return modeltype;
    }

    public void setModeltype(String modeltype) {
        this.modeltype = modeltype;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
