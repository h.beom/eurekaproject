package egovframework.com.model.web;
import egovframework.com.model.service.*;
import egovframework.com.utl.excel.JxlsExcelView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 모델 리스트 Controller
 *
 *    수정일         수정자         수정내용
 *    -------        -------     -------------------
 *    2019.03.12	고동현		최초생성
 * @author 고동현
 * @since 2019.03.12
 * @version 1.0
 */
@Controller
public class DexiModelController {


	@Resource(name = "modelListService")
	ModelListService modelListService;


	@Resource(name="jxlsExcelView")
	private JxlsExcelView jxlsExcelView;

	/**
	 * 모델리스트 정보를 호출한다.
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "model/getModelListData.do", method = RequestMethod.POST)
	public ModelAndView getModelListData(@RequestBody ModelListCriteria criteria) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("jsonView");
		try {
			modelAndView.addObject("list", modelListService.select3DModelList(criteria));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}

}
