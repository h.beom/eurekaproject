package egovframework.com.code.service;

import java.util.List;

public interface CodeListService {


    public List<SysCodeVO> getSysCode() throws Exception;
    public List<SysCodeVO> getSysCodeList() throws Exception;
    public List<SysCodeVO> getCodeList() throws Exception;
    public List<SysCodeVO> getUserCodeList() throws Exception;
    public List<SysCodeVO> getCodeDetailList() throws Exception;
    public List<SysCodeVO> getSysCodeDetail(SysCodeVO vo) throws Exception;
    public void addSysCodeList(SysCodeVO vo) throws Exception;
    public void addSysCode(SysCodeVO vo) throws Exception;
    public void addUserCode(SysCodeVO vo) throws Exception;
    public void addUserCodeList(SysCodeVO vo) throws Exception;
    public void updateSysCode(SysCodeVO vo) throws Exception;
    public void updateCodeDetail(SysCodeVO vo) throws Exception;
    public void updateCode(SysCodeVO vo) throws Exception;
    public void delSysCodeList(SysCodeVO vo) throws Exception;
    public void delUserCodeList(SysCodeVO vo) throws Exception;
    public void delUserCode(SysCodeVO vo) throws Exception;
    public List<SysCodeVO> searchDetail(CodeCriteria vo) throws Exception;
    public List<SysCodeVO> searchCode(CodeCriteria vo) throws Exception;
    public List<SysCodeVO> searchSysCode(CodeCriteria vo) throws Exception;
    public List<SysCodeVO> searchSysDetail(CodeCriteria vo) throws Exception;
    public List<SysCodeVO> getSequence(SysCodeVO vo) throws Exception;
    public List<SysCodeVO> maxSequence(SysCodeVO vo) throws Exception;
    public List<SysCodeVO> thisSequence(SysCodeVO vo) throws Exception;
    public SysCodeVO checkCdDv(SysCodeVO vo) throws Exception;
    public SysCodeVO checkCdDv2(SysCodeVO vo) throws Exception;

}
