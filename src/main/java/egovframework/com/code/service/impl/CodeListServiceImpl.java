package egovframework.com.code.service.impl;

import egovframework.com.code.service.CodeCriteria;
import egovframework.com.code.service.CodeListService;
import egovframework.com.code.service.SysCodeVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("codeListService")
public class CodeListServiceImpl extends EgovAbstractServiceImpl implements CodeListService {

    @Resource(name="codeDAO")
    private CodeDAO codeDAO;



    @Override
    public void addSysCodeList(SysCodeVO vo) throws Exception {
        codeDAO.addSysCodeList(vo);
    }

    @Override
    public void delSysCodeList(SysCodeVO vo) throws Exception {
        codeDAO.delSysCodeList(vo);
    }

    @Override
    public void delUserCodeList(SysCodeVO vo) throws Exception {
        codeDAO.delUserCodeList(vo);
    }

    @Override
    public List<SysCodeVO> getCodeDetailList() throws Exception {
        return codeDAO.getCodeDetailList();
    }

    @Override
    public List<SysCodeVO> getSysCodeDetail(SysCodeVO vo) throws Exception {
        return codeDAO.getSysCodeDetail(vo);
    }

    @Override
    public void delUserCode(SysCodeVO vo) throws Exception {
        codeDAO.delUserCode(vo);
    }

    @Override
    public List<SysCodeVO> searchDetail(CodeCriteria vo) throws Exception {
       return codeDAO.searchDetail(vo);
    }

    @Override
    public void updateCodeDetail(SysCodeVO vo) throws Exception {
        codeDAO.updateCodeDetail(vo);
    }

    @Override
    public void updateCode(SysCodeVO vo) throws Exception {
        codeDAO.updateCode(vo);
    }
    @Override
    public void updateSysCode(SysCodeVO vo) throws Exception {
        codeDAO.updateSysCode(vo);
    }

    @Override
    public void addSysCode(SysCodeVO vo) throws Exception {
        codeDAO.addSysCode(vo);
    }

    @Override
    public void addUserCode(SysCodeVO vo) throws Exception {
        codeDAO.addUserCode(vo);
    }
    @Override
    public void addUserCodeList(SysCodeVO vo) throws Exception {
        codeDAO.addUserCodeList(vo);
    }
    @Override
    public List<SysCodeVO> getCodeList() throws Exception {
        return codeDAO.getCodeList();
    }

    @Override
    public List<SysCodeVO> getUserCodeList() throws Exception {
        return codeDAO.getUserCodeList();
    }

    @Override
    public List<SysCodeVO> getSysCode() throws Exception {
        return codeDAO.getSysCode();
    }

    @Override
    public List<SysCodeVO> getSysCodeList() throws Exception {
        return codeDAO.getSysCodeList();
    }
    @Override
    public List<SysCodeVO> searchCode(CodeCriteria vo) throws Exception {
        return codeDAO.searchCode(vo);
    }

    @Override
    public List<SysCodeVO> searchSysCode(CodeCriteria vo) throws Exception {
        return codeDAO.searchSysCode(vo);
    }

    @Override
    public List<SysCodeVO> searchSysDetail(CodeCriteria vo) throws Exception {
        return codeDAO.searchSysDetail(vo);
    }
    @Override
    public List<SysCodeVO> getSequence(SysCodeVO vo) throws Exception {
        return codeDAO.getSequence(vo);
    }

    @Override
    public List<SysCodeVO> maxSequence(SysCodeVO vo) throws Exception {
        return codeDAO.maxSequence(vo);
    }
    @Override
    public List<SysCodeVO> thisSequence(SysCodeVO vo) throws Exception {
        return codeDAO.thisSequence(vo);
    }
    @Override
    public SysCodeVO checkCdDv(SysCodeVO vo) throws Exception {
        return codeDAO.checkCdDv(vo);
    }
    @Override
    public SysCodeVO checkCdDv2(SysCodeVO vo) throws Exception {
        return codeDAO.checkCdDv2(vo);
    }
}
