package egovframework.com.code.web;

import egovframework.com.code.service.CodeCriteria;
import egovframework.com.code.service.CodeListService;
import egovframework.com.code.service.SysCodeVO;
import egovframework.com.utl.excel.JxlsExcelView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Controller
public class DexiCodeController {

    @Resource(name = "codeListService")
    CodeListService codeListService;

    @Resource(name="jxlsExcelView")
    private JxlsExcelView jxlsExcelView;

    @RequestMapping(value = "code/getSysCodeList.do", method = RequestMethod.POST)
    public ModelAndView getSysCodeList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.getSysCodeList());
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/getSysCode.do", method = RequestMethod.POST)
    public ModelAndView getSysCode(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.getSysCode());
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/addSysCode.do", method = RequestMethod.POST)
    public ModelAndView addSysCode(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        codeListService.addSysCode(vo);
        return modelAndView;
    }

    @RequestMapping(value = "code/addSysCodeList.do", method = RequestMethod.POST)
    public ModelAndView addSysCodeList(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<SysCodeVO> vo1 = codeListService.getSequence(vo);
        List<SysCodeVO> vo2 = codeListService.maxSequence(vo);
        List<SysCodeVO> vo3 = codeListService.thisSequence(vo);
        vo.setCdDvTp("S");
        //추가시에 seq설정(제일큰 seq + 1 한값 가져오기)
           //추가부분
        if(vo.getSeq() == 0){
            if(vo1.get(0).getSeqC() == 1){
                codeListService.addSysCodeList(vo);
            }else{
                vo.setSeq(vo2.get(0).getSeq());
                codeListService.addSysCodeList(vo);
            }
            //수정부분
        }else if(vo.getSeq() == 1){
            codeListService.addSysCodeList(vo);
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        return modelAndView;
    }

    @RequestMapping(value = "code/delSysCodeList.do", method = RequestMethod.POST)
    public ModelAndView delSysCodeList(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        codeListService.delSysCodeList(vo);
        return modelAndView;
    }

    @RequestMapping(value = "code/getUserCodeList.do", method = RequestMethod.POST)
    public ModelAndView getUserCodeList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.getUserCodeList());
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/getCodeDetailList.do", method = RequestMethod.POST)
    public ModelAndView getCodeDetailList(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.getCodeDetailList());
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/addUserCode.do", method = RequestMethod.POST)
    public ModelAndView addUserCode(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        codeListService.addUserCode(vo);
        return modelAndView;
    }

    @RequestMapping(value = "code/addUserCodeList.do", method = RequestMethod.POST)
    public ModelAndView addUserCodeList(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();

        List<SysCodeVO> vo2 = codeListService.getSequence(vo);
        if(vo2.get(0).getSeqC() != 1){
         List<SysCodeVO> seq = codeListService.maxSequence(vo);
            vo.setSeq(seq.get(0).getSeq());
        }
        modelAndView.setViewName("jsonView");
        codeListService.addUserCodeList(vo);
        return modelAndView;
    }

    @RequestMapping(value = "code/delUserCodeList.do", method = RequestMethod.POST)
    public ModelAndView delUserCodeList(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        codeListService.delUserCodeList(vo);
        return modelAndView;
    }

    @RequestMapping(value = "code/delUserCode.do", method = RequestMethod.POST)
    public ModelAndView delUserCode(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        codeListService.delUserCode(vo);
        return modelAndView;
    }

//codeDetailList 조회
    @RequestMapping(value = "code/getSysCodeDetail.do", method = RequestMethod.POST)
    public ModelAndView getSysCodeDetail(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        List<SysCodeVO> vo4 = codeListService.getSysCodeDetail(vo);
        try {
            System.out.println(vo4.get(0).getCdNm() + "codeName을 위한 Log");
            System.out.println(vo4.get(0).getCdId() + "codeName을 위한 Log 2");
            modelAndView.addObject("list",  codeListService.getSysCodeDetail(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/searchCode.do", method = RequestMethod.POST)
    public ModelAndView searchCode(@RequestBody CodeCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.searchCode(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/searchDetail.do", method = RequestMethod.POST)
    public ModelAndView searchDetail(@RequestBody CodeCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.searchDetail(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/searchSysCode.do", method = RequestMethod.POST)
    public ModelAndView searchSysCode(@RequestBody CodeCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.searchSysCode(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/searchSysDetail.do", method = RequestMethod.POST)
    public ModelAndView searchSysDetail(@RequestBody CodeCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.searchSysDetail(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/test.do", method = RequestMethod.POST)
    public ModelAndView test(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("list", codeListService.getSequence(vo));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }
    @RequestMapping(value = "code/checkCdDv.do", method = RequestMethod.POST)
    public ModelAndView checkCdDv(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        SysCodeVO vo2= codeListService.checkCdDv(vo);
        try {
        if(vo2 == null){
            modelAndView.addObject("list", "pass");
        }else{
            modelAndView.addObject("list", "none");
        }
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "code/checkCdDv2.do", method = RequestMethod.POST)
    public ModelAndView checkCdDv2(@RequestBody SysCodeVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        SysCodeVO vo2= codeListService.checkCdDv2(vo);
        try {
            if(vo2 == null){
                modelAndView.addObject("list", vo2);
            }
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }
}

