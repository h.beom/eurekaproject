package egovframework.com.sim.web;

import egovframework.com.cmm.service.EgovProperties;
import egovframework.com.sim.service.*;
import egovframework.com.sim.service.criteria.*;
import egovframework.com.sim.service.vo.*;
import egovframework.com.utl.excel.JxlsExcelView;
import egovframework.com.utl.str.DateUtility;
import net.sf.jxls.exception.ParsePropertyException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 시뮬레이션 Controller
 * <p>
 * 수정일         수정자         수정내용
 * -------        -------     -------------------
 * 2019.03.12	고동현		최초생성
 *
 * @author 고동현
 * @version 1.0
 * @since 2019.03.12
 */
@Controller
public class DexiSimulationController {

    @Resource(name = "simulationListService")
    SimulationListService simulationListService;


    @Resource(name = "jxlsExcelView")
    private JxlsExcelView jxlsExcelView;

    /** 로깅 처리 */
    private static final Logger log = LoggerFactory.getLogger(DexiSimulationController.class);

    /**
     * 시뮬레이션 리스트 정보를 호출한다.
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "sim/getSimulationListData.do", method = RequestMethod.POST)
    public ModelAndView getSimulationListData(@RequestBody SimulationCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        System.out.println(criteria.getSimulationtype());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {

            modelAndView.addObject("list", simulationListService.selectSimulationModelList(criteria));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
//		System.out.println(simulationListService.selectModelList(criteria));
        return modelAndView;
    }

    /**
     * @return
     * @throws Exception
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     */

    @RequestMapping(value = "sim/getSimulationListExcelReport.do", method = RequestMethod.GET)
    public ModelAndView getSimulationListExcelReport(
            @RequestParam("search") String search,
            @RequestParam("username") String username,
            @RequestParam("fromdate") String fromdate,
            @RequestParam("todate") String todate,
            @RequestParam("loclid") String loclid,
            @RequestParam("simulationtype") String simulationtype
    ) throws Exception, FileNotFoundException, IOException, ParsePropertyException, InvalidFormatException {


        SimulationCriteria criteria = new SimulationCriteria();
        criteria.setSearch(search);
        criteria.setUsername(username);
        criteria.setLoclid(loclid);
        criteria.setFromdate(fromdate);
        criteria.setTodate(todate);
        criteria.setSimulationtype(simulationtype);

        ModelAndView mav = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = "SimulationList_" + DateUtility.getTodayString("yyyyMMddhhmmss") + ".xls";
        String templateName = "simulationList.xls";

        List<SimulationListVo> sheet1 = simulationListService.selectSimulationModelList(criteria);
        dataMap.put("sheet1VO", sheet1);

        mav.addObject("fileName", fileName);
        mav.addObject("template", templateName);
        mav.addObject("excelContent", dataMap);
        mav.setView(this.jxlsExcelView);
        return mav;
    }


//    /**
//     * @return
//     * @throws Exception
//     * @throws FileNotFoundException
//     * @throws IOException
//     * @throws ParsePropertyException
//     * @throws InvalidFormatException
//     */
//    @RequestMapping(value = "sim/getSimulationModelFile.do", method = RequestMethod.GET)
//    public void getSimulationModelFile(
//            @RequestParam("stenc3did") String stenc3did,
//            HttpServletResponse response) throws IOException {
//
//        String filePatch = "";
//        ProcessModelFilePatchCriteria criteria = new ProcessModelFilePatchCriteria();
//        criteria.setStenc3did(stenc3did);
//
//        try {
//            filePatch = simulationListService.getModelFilePatch(criteria);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return;
//        }
//
//        if (filePatch == null) {
//            return;
//        }
//
//        String modelFilePatch = EgovProperties.getProperty("Globals.modelFilePath");
//        modelFilePatch = modelFilePatch + filePatch;
//        response.reset();
//        response.setHeader("Content-Type", "doesn/matter;");
//        response.setHeader("Content-Disposition", "attachment;filename=\"" + "test.glb" + "\"");
//        File fp = new File(modelFilePatch);
//        int read = 0;
//
//        byte[] b = new byte[(int) fp.length()]; // 파일 크기
//
//        if (fp.isFile()) {
//
//            FileInputStream fis = new FileInputStream(fp);
//            BufferedInputStream fin = new BufferedInputStream(fis);
//            BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
//            try {
//                while ((read = fin.read(b)) != -1) {
//                    outs.write(b, 0, read);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (outs != null) {
//                    outs.close();
//                }
//                if (fin != null) {
//                    fin.close();
//                }
//            }
//
//        }
//    }

    /**
     * @return
     * @throws Exception
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     */
    @RequestMapping(value = "sim/getSimulationModelFile.do", method = RequestMethod.GET)
    public void getSimulationModelFile(
            @RequestParam("stenc3did") String stenc3did,
            HttpServletResponse response) throws IOException {

        String filePatch = "";
        ProcessModelFilePatchCriteria criteria = new ProcessModelFilePatchCriteria();
        criteria.setStenc3did(stenc3did);

        try {
            filePatch = simulationListService.getModelFilePatch(criteria);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (filePatch == null) {
            return;
        }

        String modelFilePatch = EgovProperties.getProperty("Globals.modelFilePath");
        modelFilePatch = modelFilePatch + filePatch;
        response.reset();
        response.setHeader("Content-Type", "doesn/matter;");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + "test.glb" + "\"");
        File fp = new File(modelFilePatch);
        int read = 0;

        byte[] b = new byte[(int) fp.length()]; // 파일 크기

        if (fp.isFile()) {

            FileInputStream fis = new FileInputStream(fp);
            BufferedInputStream fin = new BufferedInputStream(fis);
            BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
            try {
                while ((read = fin.read(b)) != -1) {
                    outs.write(b, 0, read);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (outs != null) {
                    outs.close();
                }
                if (fin != null) {
                    fin.close();
                }
            }

        }
    }


    /**
     * @return
     * @throws Exception
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParsePropertyException
     * @throws InvalidFormatException
     */
    @RequestMapping(value = "sim/getMapFilePatch.do", method = RequestMethod.GET)
    public void getMapFile(
            @RequestParam("mapid") String mapid,
            @RequestParam("modelItemId") String modelItemId,
            HttpServletResponse response) throws IOException {

        String filePatch = "";
        ProcessMapFilePatchCriteria criteria = new ProcessMapFilePatchCriteria();
        criteria.setMapId(mapid);
        criteria.setModelItemId(modelItemId);

        try {
            filePatch = simulationListService.getMapFilePatch(criteria);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        if (filePatch == null) {
            return;
        }

        String modelFilePatch = EgovProperties.getProperty("Globals.modelFilePath");
        modelFilePatch = modelFilePatch + filePatch;
        response.reset();
        response.setHeader("Content-Type", "doesn/matter;");
        response.setHeader("Content-Disposition", "attachment;filename=\"" + "test.png" + "\"");
        File fp = new File(modelFilePatch);
        int read = 0;

        byte[] b = new byte[(int) fp.length()]; // 파일 크기

        if (fp.isFile()) {

            FileInputStream fis = new FileInputStream(fp);
            BufferedInputStream fin = new BufferedInputStream(fis);
            BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
            try {
                while ((read = fin.read(b)) != -1) {
                    outs.write(b, 0, read);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (outs != null) {
                    outs.close();
                }
                if (fin != null) {
                    fin.close();
                }
            }

        }
    }


    @RequestMapping(value = "sim/getSimulationViewData.do", method = RequestMethod.POST)
    public ModelAndView getSimulationViewData(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        SimulationViewVo result = new SimulationViewVo();
        try {
            result = simulationListService.selectModelView(criteria);

            modelAndView.addObject("viewData", result);
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }


    //facilityOutput 정보
    @RequestMapping(value = "sim/getfacilityOutput.do", method = RequestMethod.POST)
    public ModelAndView getTimeOutPut(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("facilityOutputData", simulationListService.getfacilityOutput(criteria));

        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    //FacilityOperationgRate 정보
    @RequestMapping(value = "sim/getFacilityOperationgRate.do", method = RequestMethod.POST)
    public ModelAndView getFacilityOperationgRate(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");

        try {
            modelAndView.addObject("OperationgRateData", simulationListService.getFacilityOperationgRate(criteria));

        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    //TimeBufferUsage 정보
    @RequestMapping(value = "sim/TimeBufferUsage.do", method = RequestMethod.POST)
    public ModelAndView getTimeBufferUsage(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        List<TimeOutBufferUsageVO> resultJSON = new ArrayList<TimeOutBufferUsageVO>();

        try {
            resultJSON = simulationListService.getTimeBufferUsage(criteria);
            modelAndView.addObject("bufferData", resultJSON);
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    /**
     * 결과요약 chart 데이터 , 설비별 생산량 chart 데이터 ,제품별 생산량 chart 데이터
     * FacilityNgCnt 차트 정보 호출
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "sim/getFacilityNgCntChartData.do", method = RequestMethod.POST)
    public ModelAndView getFacilityNgCntChartData(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        List<SimulationViewChartDataVo> resultJSON = new ArrayList<SimulationViewChartDataVo>();

        try {
            resultJSON = simulationListService.selectSimFacilityNgCntChart(criteria);
            modelAndView.addObject("charData", resultJSON);
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    /**
     * @param criteria
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "sim/getTimeOutNg.do", method = RequestMethod.POST)
    public ModelAndView getTimeOutNg(@RequestBody SimulationViewCriteria criteria, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("getTimeOutNg", simulationListService.getTimeOutNg(criteria));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }


    @RequestMapping(value = "sim/getProcessModel.do", method = RequestMethod.POST)
    public ModelAndView getProcessModel(@RequestBody SimulationViewCriteria criteria) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("mapData", simulationListService.getProcessMapInfoList(criteria));
            modelAndView.addObject("modelListData", simulationListService.getProcessModelInfoList(criteria));
            modelAndView.addObject("processAnimationTotalTime", simulationListService.getProcessAnimationTotalTime(criteria));
            modelAndView.addObject("processAnimation", simulationListService.getProcessAnimationList(criteria));
        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

    @RequestMapping(value = "sim/getFactoryModel.do", method = RequestMethod.POST)
    public ModelAndView getFactoryModel(@RequestBody ModelViewCriteria criteria) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            modelAndView.addObject("mapData", simulationListService.getFactoryMapInfoList(criteria));
            modelAndView.addObject("modelListData", simulationListService.getFactoryModelList(criteria));
            modelAndView.addObject("modelAttrData", simulationListService.getFactoryModelAttrList(criteria));

        } catch (Exception e) {
            System.out.println("test:" + e);
        }
        return modelAndView;
    }

//	==============================================================================================
//	시간별 생산량 엑셀출력

    @RequestMapping(value = "sim/timeoutExcelReport.do", method = RequestMethod.GET)
    public ModelAndView timeoutExcelReport(
            @RequestParam("simulationid") String simulationid, @RequestParam("loclid") String loclid,
            @RequestParam("sorttype") String sorttype,@RequestParam("sortname") String sortname
    ) throws Exception, FileNotFoundException, IOException, ParsePropertyException, InvalidFormatException {

        SimExcelParamVO criteria = new SimExcelParamVO();
        criteria.setSimulationid(simulationid);
        criteria.setLoclid(loclid);
        criteria.setSortname(sortname);
        criteria.setSorttype(sorttype);

        ModelAndView mav = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = "TimeOutputExcel_" + DateUtility.getTodayString("yyyyMMddhhmmss") + ".xls";
        String templateName = "timeoutChartList.xls";


        List<SimulationChartVO> sheet = simulationListService.getTimeoutChartExcel(criteria);

        dataMap.put("sheet1VO", sheet);

        mav.addObject("fileName", fileName);
        mav.addObject("template", templateName);
        mav.addObject("excelContent", dataMap);
        mav.setView(this.jxlsExcelView);
        return mav;
    }


//	==============================================================================================
//	설비별 생산량 엑셀출력

    @RequestMapping(value = "sim/facilityoutputExcelReport.do", method = RequestMethod.GET)
    public ModelAndView facilityoutputExcelReport(
            @RequestParam("simulationid") String simulationid, @RequestParam("loclid") String loclid, @RequestParam("checkedkeys") ArrayList<String> checkedkeys,
            @RequestParam("sorttype") String sorttype,@RequestParam("sortname") String sortname
    ) throws Exception, FileNotFoundException, IOException, ParsePropertyException, InvalidFormatException {

        SimExcelParamVO criteria = new SimExcelParamVO();
        criteria.setSimulationid(simulationid);
        criteria.setLoclid(loclid);
        criteria.setCheckedkeys(checkedkeys);
        criteria.setSorttype(sorttype);
        criteria.setSortname(sortname);

        ModelAndView mav = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = "facilityOutputExcel_" + DateUtility.getTodayString("yyyyMMddhhmmss") + ".xls";
        String templateName = "facilityoutputChartList.xls";


        List<SimulationViewChartDataVo> sheet = simulationListService.selectExcelFacilityNg(criteria);

        dataMap.put("sheet1VO", sheet);

        mav.addObject("fileName", fileName);
        mav.addObject("template", templateName);
        mav.addObject("excelContent", dataMap);
        mav.setView(this.jxlsExcelView);
        return mav;
    }
//	==============================================================================================
//	제품별 생산량 엑셀출력

    @RequestMapping(value = "sim/productoutputExcelReport.do", method = RequestMethod.GET)
    public ModelAndView productoutputExcelReport(
            @RequestParam("simulationid") String simulationid, @RequestParam("loclid") String loclid,
            @RequestParam("checkedkeys") ArrayList<String> checkedkeys, @RequestParam("sortname") String sortname
    ) throws Exception, FileNotFoundException, IOException, ParsePropertyException, InvalidFormatException {

        SimExcelParamVO criteria = new SimExcelParamVO();
        criteria.setSimulationid(simulationid);
        criteria.setLoclid(loclid);
        criteria.setCheckedkeys(checkedkeys);
        criteria.setSortname(sortname);
        ModelAndView mav = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = "productoutputExcel" + DateUtility.getTodayString("yyyyMMddhhmmss") + ".xls";
        String templateName = "productoutputChartList.xls";


        List<SimulationViewChartDataVo> sheet = simulationListService.productoutputExcel(criteria);

        dataMap.put("sheet1VO", sheet);

        mav.addObject("fileName", fileName);
        mav.addObject("template", templateName);
        mav.addObject("excelContent", dataMap);
        mav.setView(this.jxlsExcelView);
        return mav;
    }

//	==============================================================================================
//	설비별 가용률 엑셀출력

    @RequestMapping(value = "sim/facilityoperatingrateExcelReport.do", method = RequestMethod.GET)
    public ModelAndView facilityoperatingrateExcelReport(
            @RequestParam("simulationid") String simulationid, @RequestParam("loclid") String loclid, @RequestParam("checkedkeysstring") String checkedkeysstring,
            @RequestParam("sorttype") String sorttype, @RequestParam("sortname") String sortname
    ) throws Exception, FileNotFoundException, IOException, ParsePropertyException, InvalidFormatException {
        SimExcelParamVO criteria = new SimExcelParamVO();
        criteria.setSimulationid(simulationid);
        criteria.setLoclid(loclid);
        criteria.setCheckedkeysstring(checkedkeysstring);
        criteria.setSorttype(sorttype);
        criteria.setSortname(sortname);



        ModelAndView mav = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = "facilityoperatingrateExcel" + DateUtility.getTodayString("yyyyMMddhhmmss") + ".xls";
        String templateName = "facilityoperatingrateChartList.xls";

        List<FacOperateingExcelVO> sheet = simulationListService.getFacilityOperateExcel(criteria);

        dataMap.put("sheet1VO", sheet);

        mav.addObject("fileName", fileName);
        mav.addObject("template", templateName);
        mav.addObject("excelContent", dataMap);
        mav.setView(this.jxlsExcelView);
        return mav;
    }

//	==============================================================================================
//	버퍼사용량 엑셀출력

    @RequestMapping(value = "sim/timebufferusageExcelReport.do", method = RequestMethod.GET)
    public ModelAndView timebufferusageExcelReport(
            @RequestParam("simulationid") String simulationid, @RequestParam("loclid") String loclid,
            @RequestParam("sortname") String sortname
    ) throws Exception, FileNotFoundException, IOException, ParsePropertyException, InvalidFormatException {

        SimExcelParamVO criteria = new SimExcelParamVO();
        criteria.setSimulationid(simulationid);
        criteria.setLoclid(loclid);
        criteria.setSortname(sortname);
System.out.println(criteria.getSortname()+"++++++++++++++");
        ModelAndView mav = new ModelAndView();
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String fileName = "timebufferusageExcel" + DateUtility.getTodayString("yyyyMMddhhmmss") + ".xls";
        String templateName = "timebufferusageChartList.xls";


        List<TimeOutBufferUsageVO> sheet = simulationListService.getExcelTimeBufferUsage(criteria);

        dataMap.put("sheet1VO", sheet);

        mav.addObject("fileName", fileName);
        mav.addObject("template", templateName);
        mav.addObject("excelContent", dataMap);
        mav.setView(this.jxlsExcelView);
        return mav;
    }
}
