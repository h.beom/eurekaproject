package egovframework.com.sim.service.impl;

import egovframework.com.cmm.service.impl.DexiComAbstractDAO;
import egovframework.com.sim.service.FacOperateingExcelVO;
import egovframework.com.sim.service.criteria.*;
import egovframework.com.sim.service.vo.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 시뮬레이션 리스트 DAO 클래스를 정의한다.
 *
 * @author 고동현
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 * @since 2019.04.13
 */
@Repository("simulationDAO")
public class SimulationDAO extends DexiComAbstractDAO {

    /**
     * 시뮬레이션 리스트를 반환한다.
     *
     * @param criteria
     * @return
     * @throws Exception
     */
    public List<SimulationListVo> selectSimulationModelList(SimulationCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectSimulationModelList", criteria);
    }

    /**
     * 시뮬레이션 리스트 > 결과보기 를 반환한다.
     *
     * @param criteria
     * @return
     * @throws Exception
     */

    public SimulationViewVo selectModelView(SimulationViewCriteria criteria) throws Exception {
        return selectOne("simulationDAO.selectModelView", criteria);
    }

    //시간별 생산량
    public List<SimulationChartVO> getTimeOut(SimulationViewCriteria criteria) throws Exception {
        System.out.println("DAO를 실행중에 있습니다.");
        return selectList("simulationDAO.getTimeOut", criteria);
    }

    //설비별 생산량
    public List<SimulationChartVO> getfacilityOutput(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.getfacilityOutput", criteria);
    }

    //설비 가용률
    public List<SimulationFacilityVO> getFacilityOperationgRate(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.getFacilityOperationgRate", criteria);
    }

    //시간별 버퍼 사용량
    public List<TimeOutBufferUsageVO> getTimeBufferUsage(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.getTimeBufferUsage", criteria);
    }

    //시간별 불량 생산량
    public List<TimeOutListVO> getTimeOutNg(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.getTimeOutNg", criteria);
    }

    /**
     * 시뮬레이션 리스트 > 결과보기 > 차트 데이터 호출
     *
     * @param criteria
     * @return
     * @throws Exception
     */
    public List<SimulationViewChartDataVo> selectSimFacilityNgCntChart(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectSimFacilityNgCntChart", criteria);
    }

    public List<ProcessModelInfoVO> getProcessModelInfoList(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectProcessModelInfoList", criteria);
    }

    public List<ProcessMapInfoVO> getProcessMapInfoList(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.getProcessMapInfoList", criteria);
    }

    public String getModelFilePatch(ProcessModelFilePatchCriteria criteria) throws Exception {
        return selectOne("simulationDAO.getModelFilePatch", criteria);
    }

    public String getMapFilePatch(ProcessMapFilePatchCriteria criteria) throws Exception {
        return selectOne("simulationDAO.getMapFilePatch", criteria);
    }

    public List<ProcessAnimationDataVo> selectProcessAnimationList(SimulationViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectProcessAnimationList", criteria);
    }

    public int getProcessAnimationTotalTime(SimulationViewCriteria criteria) throws Exception {
        return (int) selectOne("simulationDAO.selectProcessAnimationTotalTime", criteria);
    }

    public List<ProcessMapInfoVO> selectFactoryMapInfoList(ModelViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectFactoryMapInfoList", criteria);
    }

    public List<ProcessModelInfoVO> selectFactoryModelList(ModelViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectFactoryModelList", criteria);
    }

    public List<FactoryModelAttrVO> selectFactoryModelAttrList(ModelViewCriteria criteria) throws Exception {
        return selectList("simulationDAO.selectFactoryModelAttrList", criteria);
    }


    public List<SimulationChartVO> getTimeoutChartExcel(SimExcelParamVO criteria) throws Exception {

        return selectList("simulationDAO.getTimeoutChartExcel", criteria);
    }

    public List<FacOperateingExcelVO> getFacilityOperateExcel(SimExcelParamVO criteria) throws Exception {
        return selectList("simulationDAO.getFacilityOperateExcel", criteria);
    }

    public List<SimulationViewChartDataVo> selectExcelFacilityNg(SimExcelParamVO criteria) throws Exception {

        return selectList("simulationDAO.ExcelFacilityNg", criteria);
    }

    public List<SimulationViewChartDataVo> productoutputExcel(SimExcelParamVO criteria) throws Exception {

        System.out.println(criteria);
        return selectList("simulationDAO.productoutputExcel", criteria);
    }
    public List<TimeOutBufferUsageVO> getExcelTimeBufferUsage(SimExcelParamVO criteria) throws Exception {

        return selectList("simulationDAO.getExcelTimeBufferUsage", criteria);
    }

}