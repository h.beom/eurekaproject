package egovframework.com.sim.service.impl;

import egovframework.com.sim.service.*;
import egovframework.com.sim.service.criteria.*;
import egovframework.com.sim.service.vo.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 시뮬레이션 리스트 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
@Service("simulationListService")
public class SimualtionListServiceImpl extends EgovAbstractServiceImpl implements SimulationListService {

	@Resource(name="simulationDAO")
	private SimulationDAO simulationDAO;


	/**
	 * 시뮬레이션 리스트를 반환한다.
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	public List<SimulationListVo> selectSimulationModelList(SimulationCriteria criteria) throws Exception{
		return simulationDAO.selectSimulationModelList(criteria);
	}

	/**
	 * 시뮬레이션 리스트 > 결과보기 를 반환한다.
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	public SimulationViewVo selectModelView(SimulationViewCriteria criteria) throws Exception{
		return simulationDAO.selectModelView(criteria);
	}


	/**
	 * 시뮬레이션 리스트 > 결과보기 를 반환한다.
//	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	//설비별 생산량
	public List<?> getfacilityOutput(SimulationViewCriteria criteria) throws Exception {
		return simulationDAO.getfacilityOutput(criteria);
	}
	//시간별 생산량
	public List<?> getTimeOut(SimulationViewCriteria criteria) throws Exception{
		System.out.println("현재 ServiceImpl.java 실행중에 있습니다.");
		return simulationDAO.getTimeOut(criteria);
	}
	//설비 가용률
	public List<SimulationFacilityVO> getFacilityOperationgRate(SimulationViewCriteria criteria) throws Exception {
		return simulationDAO.getFacilityOperationgRate(criteria);
	}
	//시간별 버퍼 사용량
	public List<TimeOutBufferUsageVO> getTimeBufferUsage(SimulationViewCriteria criteria) throws Exception{
		return simulationDAO.getTimeBufferUsage(criteria);
	}

	public List<SimulationViewChartDataVo> selectSimFacilityNgCntChart(SimulationViewCriteria criteria) throws Exception {
		return simulationDAO.selectSimFacilityNgCntChart(criteria);
	}

	//시간별 불량생산량
	@Override
	public List<TimeOutListVO> getTimeOutNg(SimulationViewCriteria criteria) throws Exception {
		return simulationDAO.getTimeOutNg(criteria);
	}

	@Override
	public List<ProcessModelInfoVO> getProcessModelInfoList(SimulationViewCriteria criteria) throws Exception{
		return  simulationDAO.getProcessModelInfoList(criteria);
	}


	@Override
	public List<ProcessMapInfoVO> getProcessMapInfoList(SimulationViewCriteria criteria) throws Exception{
		return  simulationDAO.getProcessMapInfoList(criteria);
	}

	public String getModelFilePatch(ProcessModelFilePatchCriteria criteria) throws Exception {
		return simulationDAO.getModelFilePatch(criteria);
	}

	public String getMapFilePatch(ProcessMapFilePatchCriteria criteria) throws Exception {
		return simulationDAO.getMapFilePatch(criteria);
	}

	public List<ProcessAnimationDataVo> getProcessAnimationList(SimulationViewCriteria criteria) throws Exception {
		return simulationDAO.selectProcessAnimationList(criteria);
	}

	public int getProcessAnimationTotalTime(SimulationViewCriteria criteria) throws Exception{
		return simulationDAO.getProcessAnimationTotalTime(criteria);
	}
	public List<ProcessMapInfoVO> getFactoryMapInfoList(ModelViewCriteria criteria) throws Exception{
		return simulationDAO.selectFactoryMapInfoList(criteria);
	}

	public List<ProcessModelInfoVO> getFactoryModelList(ModelViewCriteria criteria) throws Exception{
		return simulationDAO.selectFactoryModelList(criteria);
	}

	public List<FactoryModelAttrVO> getFactoryModelAttrList(ModelViewCriteria criteria) throws Exception{
		return simulationDAO.selectFactoryModelAttrList(criteria);
	}

	@Override
	public List<FacOperateingExcelVO> getFacilityOperateExcel(SimExcelParamVO criteria) throws Exception {
		return simulationDAO.getFacilityOperateExcel(criteria);
	}

	// 설비별생산량 엑셀
	@Override
	public List<SimulationViewChartDataVo> selectExcelFacilityNg(SimExcelParamVO criteria) throws Exception {
		return simulationDAO.selectExcelFacilityNg(criteria);
	}

	@Override
	public List<SimulationViewChartDataVo> productoutputExcel(SimExcelParamVO criteria) throws Exception {
		return simulationDAO.productoutputExcel(criteria);
	}

	@Override
	public List<SimulationChartVO> getTimeoutChartExcel(SimExcelParamVO criteria) throws Exception {
		return simulationDAO.getTimeoutChartExcel(criteria);
	}

	@Override
	public List<TimeOutBufferUsageVO> getExcelTimeBufferUsage(SimExcelParamVO criteria) throws Exception {
		return simulationDAO.getExcelTimeBufferUsage(criteria);
	}
}