package egovframework.com.sim.service.vo;

public class ProcessMapInfoVO {
    private String mapId;
    private String modelItemId;
    private int mapRangeLenx;
    private int mapRangeLeny;
    private int mapRangeLenz;
    private int mapGridx;
    private int mapGridy;
    private int mapGridz;

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getModelItemId() {
        return modelItemId;
    }

    public void setModelItemId(String modelItemId) {
        this.modelItemId = modelItemId;
    }

    public int getMapRangeLenx() {
        return mapRangeLenx;
    }

    public void setMapRangeLenx(int mapRangeLenx) {
        this.mapRangeLenx = mapRangeLenx;
    }

    public int getMapRangeLeny() {
        return mapRangeLeny;
    }

    public void setMapRangeLeny(int mapRangeLeny) {
        this.mapRangeLeny = mapRangeLeny;
    }

    public int getMapRangeLenz() {
        return mapRangeLenz;
    }

    public void setMapRangeLenz(int mapRangeLenz) {
        this.mapRangeLenz = mapRangeLenz;
    }

    public int getMapGridx() {
        return mapGridx;
    }

    public void setMapGridx(int mapGridx) {
        this.mapGridx = mapGridx;
    }

    public int getMapGridy() {
        return mapGridy;
    }

    public void setMapGridy(int mapGridy) {
        this.mapGridy = mapGridy;
    }

    public int getMapGridz() {
        return mapGridz;
    }

    public void setMapGridz(int mapGridz) {
        this.mapGridz = mapGridz;
    }

}
