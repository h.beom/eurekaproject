package egovframework.com.sim.service.vo;

public class ProcessAnimationDataVo {

    private String processType;
    private String processId;
    private String simIndex;
    private String productIdx;
    private String productSeq;
    private String mapId;
    private String stencNm;
    private String simLoadingTime;
    private String simProcessTime;
    private String simUnloadingTime;
    private int startTime;
    private int waitingTime;
    private int stencLenx;
    private int stencLeny;
    private int stencLenz;
    private int stencPosx;
    private int stencPosy;
    private int stencPosz;
    private String status;
    private int endTime;

    public int getEndTime() { return endTime; }

    public void setEndTime(int endTime) { this.endTime = endTime; }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getSimIndex() {
        return simIndex;
    }

    public void setSimIndex(String simIndex) {
        this.simIndex = simIndex;
    }

    public String getProductIdx() {
        return productIdx;
    }

    public void setProductIdx(String productIdx) {
        this.productIdx = productIdx;
    }

    public String getProductSeq() {
        return productSeq;
    }

    public void setProductSeq(String productSeq) {
        this.productSeq = productSeq;
    }

    public String getSimLoadingTime() {
        return simLoadingTime;
    }

    public void setSimLoadingTime(String simLoadingTime) {
        this.simLoadingTime = simLoadingTime;
    }

    public String getSimProcessTime() {
        return simProcessTime;
    }

    public void setSimProcessTime(String simProcessTime) {
        this.simProcessTime = simProcessTime;
    }

    public String getSimUnloadingTime() {
        return simUnloadingTime;
    }

    public void setSimUnloadingTime(String simUnloadingTime) {
        this.simUnloadingTime = simUnloadingTime;
    }

    public String getStencNm() {
        return stencNm;
    }

    public void setStencNm(String stencNm) {
        this.stencNm = stencNm;
    }

    public int getStencLenx() {
        return stencLenx;
    }

    public void setStencLenx(int stencLenx) {
        this.stencLenx = stencLenx;
    }

    public int getStencLeny() {
        return stencLeny;
    }

    public void setStencLeny(int stencLeny) {
        this.stencLeny = stencLeny;
    }

    public int getStencLenz() {
        return stencLenz;
    }

    public void setStencLenz(int stencLenz) {
        this.stencLenz = stencLenz;
    }

    public int getStencPosx() {
        return stencPosx;
    }

    public void setStencPosx(int stencPosx) {
        this.stencPosx = stencPosx;
    }

    public int getStencPosy() {
        return stencPosy;
    }

    public void setStencPosy(int stencPosy) {
        this.stencPosy = stencPosy;
    }

    public int getStencPosz() {
        return stencPosz;
    }

    public void setStencPosz(int stencPosz) {
        this.stencPosz = stencPosz;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }
}
