package egovframework.com.sim.service.vo;

public class SimulationViewChartDataVo {

    private String simReqId;// 시뮬레이션 아이디
    private String processId;// 프로세스 아이디
    private String processNm;// 프로세스 이름
    private String processNgNm; // 불량건수 컬럼명
    private int processSeq;// 프로세스 시퀀스
    private int okCount;// 생산량
    private int totOkCount;// 총생산량
    private float avgNgRate;// 평균생산량
    private int ngCount;// 불량건수
    private String simLoadingTimeHour; //버퍼사용 시간
    private int conveyorCountForHour;//버퍼시간당 사용량

    public String getSimLoadingTimeHour() {
        return simLoadingTimeHour;
    }

    public void setSimLoadingTimeHour(String simLoadingTimeHour) {
        this.simLoadingTimeHour = simLoadingTimeHour;
    }

    public int getConveyorCountForHour() {
        return conveyorCountForHour;
    }

    public void setConveyorCountForHour(int conveyorCountForHour) {
        this.conveyorCountForHour = conveyorCountForHour;
    }

    public String getSimReqId() {
        return simReqId;
    }

    public void setSimReqId(String simReqId) {
        this.simReqId = simReqId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessNm() {
        return processNm;
    }

    public void setProcessNm(String processNm) {
        this.processNm = processNm;
    }

    public int getProcessSeq() {
        return processSeq;
    }

    public void setProcessSeq(int processSeq) {
        this.processSeq = processSeq;
    }

    public int getOkCount() {
        return okCount;
    }

    public void setOkCount(int okCount) {
        this.okCount = okCount;
    }

    public int getTotOkCount() {
        return totOkCount;
    }

    public void setTotOkCount(int totOkCount) {
        this.totOkCount = totOkCount;
    }

    public float getAvgNgRate() {
        return avgNgRate;
    }

    public void setAvgNgRate(float avgNgRate) {
        this.avgNgRate = avgNgRate;
    }

    public int getNgCount() {
        return ngCount;
    }

    public void setNgCount(int ngCount) {
        this.ngCount = ngCount;
    }

    public String getProcessNgNm() {
        return processNgNm;
    }

    public void setProcessNgNm(String processNgNm) {
        this.processNgNm = processNgNm;
    }
}
