package egovframework.com.sim.service.vo;

import java.util.Date;

public class UserListVO {

    private String userId;
    private String userLoginId;
    private String userNm;
    private String userPw;
    private String userEmail;
    private String userGend;
    private String userTp;
    private String userBirday;
    private String uptDt;
    private String upDeptId;
    private String userAtNm;
    private String userPhoto;
    private String deptNm;
    private String deptAtCd;
    private String deptCd;
    private String deptId;
    private int deptAtInherYn;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeptCd() {
        return deptCd;
    }

    public void setDeptCd(String deptCd) {
        this.deptCd = deptCd;
    }

    public String getUpDeptId() {
        return upDeptId;
    }

    public void setUpDeptId(String upDeptId) {
        this.upDeptId = upDeptId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getDeptAtCd() { return deptAtCd; }

    public void setDeptAtCd(String deptAtCd) { this.deptAtCd = deptAtCd; }

    public String getUserBirday() { return userBirday; }

    public void setUserBirday(String userBirday) { this.userBirday = userBirday; }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getUserPw() {
        return userPw;
    }

    public void setUserPw(String userPw) {
        this.userPw = userPw;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserGend() {
        return userGend;
    }

    public void setUserGend(String userGend) {
        this.userGend = userGend;
    }

    public String getUserTp() {
        return userTp;
    }

    public void setUserTp(String userTp) {
        this.userTp = userTp;
    }

//    public Date getUserBirday() {
//        return userBirday;
//    }
//
//    public void setUserBirday(Date userBirday) {
//        this.userBirday = userBirday;
//    }

    public String getUptDt() {
        return uptDt;
    }

    public void setUptDt(String uptDt) {
        this.uptDt = uptDt;
    }

    public String getUserAtNm() {
        return userAtNm;
    }

    public void setUserAtNm(String userAtNm) {
        this.userAtNm = userAtNm;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public int getDeptAtInherYn() {
        return deptAtInherYn;
    }

    public void setDeptAtInherYn(int deptAtInherYn) {
        this.deptAtInherYn = deptAtInherYn;
    }
}
