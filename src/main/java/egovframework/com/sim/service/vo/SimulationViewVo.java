package egovframework.com.sim.service.vo;

public class SimulationViewVo {

    private String simReqId;
    private String modelNm;
    private String simReqNm;
    private String insDt;
    private String userNm;
    private int analysisTime;
    private int warmUpTime;
    private String simReqStNm;
    private String simReqTpNm;
    private int output;
    private int ngCount;
    private String modelId;
    private String stdDeviation;
    private int input;

    public String getSimReqId() {
        return simReqId;
    }

    public void setSimReqId(String simReqId) {
        this.simReqId = simReqId;
    }

    public String getModelNm() {
        return modelNm;
    }

    public void setModelNm(String modelNm) {
        this.modelNm = modelNm;
    }

    public String getSimReqNm() {
        return simReqNm;
    }

    public void setSimReqNm(String simReqNm) {
        this.simReqNm = simReqNm;
    }

    public String getInsDt() {
        return insDt;
    }

    public void setInsDt(String insDt) {
        this.insDt = insDt;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public int getAnalysisTime() {
        return analysisTime;
    }

    public void setAnalysisTime(int analysisTime) {
        this.analysisTime = analysisTime;
    }

    public int getWarmUpTime() {
        return warmUpTime;
    }

    public void setWarmUpTime(int warmUpTime) {
        this.warmUpTime = warmUpTime;
    }

    public String getSimReqStNm() {
        return simReqStNm;
    }

    public void setSimReqStNm(String simReqStNm) {
        this.simReqStNm = simReqStNm;
    }

    public String getSimReqTpNm() {
        return simReqTpNm;
    }

    public void setSimReqTpNm(String simReqTpNm) {
        this.simReqTpNm = simReqTpNm;
    }

    public int getOutput() {
        return output;
    }

    public void setOutput(int output) {
        this.output = output;
    }

    public int getNgCount() {
        return ngCount;
    }

    public void setNgCount(int ngCount) {
        this.ngCount = ngCount;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getStdDeviation() {
        return stdDeviation;
    }

    public void setStdDeviation(String stdDeviation) {
        this.stdDeviation = stdDeviation;
    }

    public int getInput() {
        return input;
    }

    public void setInput(int input) {
        this.input = input;
    }
}
