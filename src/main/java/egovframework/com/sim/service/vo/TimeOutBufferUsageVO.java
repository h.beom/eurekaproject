package egovframework.com.sim.service.vo;

public class TimeOutBufferUsageVO {

    private String simReqId;// 시뮬레이션 아이디
    private String simLoadingTimeHour; //버퍼사용 시간
    private int conveyorCountForHour;//버퍼시간당 사용량

    public String getSimLoadingTimeHour() {
        return simLoadingTimeHour;
    }

    public void setSimLoadingTimeHour(String simLoadingTimeHour) {
        this.simLoadingTimeHour = simLoadingTimeHour;
    }

    public int getConveyorCountForHour() {
        return conveyorCountForHour;
    }

    public void setConveyorCountForHour(int conveyorCountForHour) {
        this.conveyorCountForHour = conveyorCountForHour;
    }

    public String getSimReqId() {
        return simReqId;
    }

    public void setSimReqId(String simReqId) {
        this.simReqId = simReqId;
    }
}
