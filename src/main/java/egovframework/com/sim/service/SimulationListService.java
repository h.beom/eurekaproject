package egovframework.com.sim.service;

import egovframework.com.sim.service.criteria.*;
import egovframework.com.sim.service.vo.*;

import java.util.List;

/**
 * 시뮬레이션 리스트 인터페이스 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
public interface SimulationListService {
	/**
	 * 부서 목록조회 카운트를 반환한다
	 *
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	//리스트 출력
	public List<SimulationListVo> selectSimulationModelList(SimulationCriteria criteria) throws Exception;

	//결과요약
	public SimulationViewVo selectModelView(SimulationViewCriteria criteria) throws Exception;

	//시간별 생산량
	public List<?> getTimeOut(SimulationViewCriteria criteria) throws Exception;

	//설비별생산량
	public List<?> getfacilityOutput(SimulationViewCriteria criteria) throws Exception;

	//설비가용률
	public List<SimulationFacilityVO> getFacilityOperationgRate(SimulationViewCriteria criteria) throws Exception;

	//시간별 버퍼사용량
	public List<TimeOutBufferUsageVO> getTimeBufferUsage(SimulationViewCriteria criteria) throws Exception;

	public List<SimulationViewChartDataVo> selectSimFacilityNgCntChart(SimulationViewCriteria criteria) throws Exception;

	//시간별 불량건수
	public List<TimeOutListVO> getTimeOutNg(SimulationViewCriteria criteria) throws Exception;
	//공정 모델 > 3d 모델 정보
	public List<ProcessModelInfoVO> getProcessModelInfoList(SimulationViewCriteria criteria) throws Exception;
	//공정 모델 > Map 정보
	public List<ProcessMapInfoVO> getProcessMapInfoList(SimulationViewCriteria criteria) throws Exception;
	//공정 모델 > 모델 patch 정보 호출
	public String getModelFilePatch(ProcessModelFilePatchCriteria criteria) throws Exception;

	public String getMapFilePatch(ProcessMapFilePatchCriteria criteria) throws Exception;

	public List<ProcessAnimationDataVo> getProcessAnimationList(SimulationViewCriteria criteria) throws Exception;
	public int getProcessAnimationTotalTime(SimulationViewCriteria criteria) throws Exception;

	public List<ProcessMapInfoVO> getFactoryMapInfoList(ModelViewCriteria criteria) throws Exception;
	public List<ProcessModelInfoVO> getFactoryModelList(ModelViewCriteria criteria) throws Exception;
	public List<FactoryModelAttrVO> getFactoryModelAttrList(ModelViewCriteria criteria) throws Exception;

	public List<FacOperateingExcelVO> getFacilityOperateExcel(SimExcelParamVO criteria) throws Exception;

	// 설비별 생산량 EXCEL
	 public List<SimulationViewChartDataVo> selectExcelFacilityNg(SimExcelParamVO criteria) throws Exception;
	 // 제품별 생산량 EXCEL
	public List<SimulationViewChartDataVo> productoutputExcel(SimExcelParamVO criteria) throws Exception;
	// 시간별 생산량 EXCEL
	public List<SimulationChartVO> getTimeoutChartExcel(SimExcelParamVO criteria) throws Exception;
	// 버퍼사용량 EXCEL
	public List<TimeOutBufferUsageVO> getExcelTimeBufferUsage(SimExcelParamVO criteria) throws Exception;

}