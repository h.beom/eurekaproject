package egovframework.com.sim.service.criteria;

import java.util.ArrayList;

public class SimulationViewCriteria {
    private String simulationid;
    private String loclid;

    public String getLoclid() {
        return loclid;
    }

    public void setLoclid(String loclid) {
        this.loclid = loclid;
    }

    public String getSimulationid() {
        return simulationid;
    }

    public void setSimulationid(String simulationid) {
        this.simulationid = simulationid;
    }

}
