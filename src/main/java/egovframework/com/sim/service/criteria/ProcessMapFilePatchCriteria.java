package egovframework.com.sim.service.criteria;

public class ProcessMapFilePatchCriteria {
    private String mapId;
    private String modelItemId;

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getModelItemId() {
        return modelItemId;
    }

    public void setModelItemId(String modelItemId) {
        this.modelItemId = modelItemId;
    }
}
