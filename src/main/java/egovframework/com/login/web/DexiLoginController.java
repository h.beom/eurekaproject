package egovframework.com.login.web;


import egovframework.com.login.service.LogService;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.com.utl.excel.JxlsExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * 시뮬레이션 Controller
 * <p>
 * 수정일         수정자         수정내용
 * -------        -------     -------------------
 * 2019.03.12	고동현		최초생성
 *
 * @author 고동현
 * @version 1.0
 * @since 2019.03.12
 */
@Controller
public class DexiLoginController {

	@Resource(name = "logService")
    LogService logService;

    @Autowired
    private JavaMailSenderImpl EMSMailSender;

    @Resource(name = "jxlsExcelView")
    private JxlsExcelView jxlsExcelView;


    @RequestMapping(value = "login/sendMail.do", method = RequestMethod.POST)
    public ModelAndView SendMail(@RequestBody UserListVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");


        String setfrom = "lhbhb21c@naver.com";
        String tomail = "lhbhb21c@naver.com";     // 받는 사람 이메일
        String title = "sendMailTest";     // 제목
        String content = "아아아아 이러기냐 정말로.....";   // 내용
        System.out.println();
        String one = UUID.randomUUID().toString().replace("-", "");



        try {
            logService.pwUpdate(vo);
            UserListVO vo2 = logService.getPw(vo);

            MimeMessage message = EMSMailSender.createMimeMessage();
            MimeMessageHelper messageHelper
                    = new MimeMessageHelper(message, true, "UTF-8");

            messageHelper.setFrom(setfrom);  // 보내는사람 생략하거나 하면 정상작동을 안함
            messageHelper.setTo(tomail);     // 받는사람 이메일
            messageHelper.setSubject(title); // 메일제목은 생략이 가능하다
            messageHelper.setText(content, "" +
                            "  <div className=\"lista-tarefas\">\n" +
                            "                <h1 style=\"font-size:46px; color:yellowgreen; text-transform: uppercase;text-align: center; line-height: 60px;letter-spacing: 7px;  font-weight: bold;\">DEXI\n" +
                            "                    에서 임시 비밀번호를 발송합니다.</h1>\n" +
                            "                <center>\n" +
                            "                    <br><br><br>\n" +
                            "                        <input style=\" width: 250px; height: 40px; background: #FFF; margin-top: 10px; padding-left: 8px; letter-spacing: 2px; color: #262626;  font-size: 16px;  outline-color: transparent; border: none; \n" +
                            "\t                            border-bottom: 2px solid #F8B1C1; margin-bottom: 40px; \"\n" +
                            "                               id=\"tarefa\" type=\"text\" name=\"tarefa\" placeholder=\"Escreve uma tarefa\">\n" +
                            "                            <br>\n" +
                            "                                <button style=\" width: 150px; height: 55px; background: #FFF; color: #F8B1C1;   text-transform: uppercase; border: 2px solid #F8B1C1; cursor: pointer; font-size: 16px; \n" +
                            "\tletter-spacing: 3px; margin-right: 7px; margin-left: 7px;  font-weight: bold; margin-bottom: 30px;\" id=\"adicionar\"\n" +
                            "                                        type=\"submit\">Adicionar\n" +
                            "                                </button>\n" +
                            "                </center>\n" +
                            "            </div>"
//                    "        </div></div></div>" + vo2.getUserNm() +"님 새로 바뀌신 비밀번호는"+ vo2.getUserPw() + "입니다..."
            );  // 메일 내용
            EMSMailSender.send(message);
            modelAndView.addObject("list", "n");
        } catch (Exception e) {
            System.out.println(e + ":::::::::::");
        }
    return modelAndView;
    }


    @RequestMapping(value = "login/changePw.do", method = RequestMethod.POST)
    public ModelAndView changePw(@RequestBody UserListVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        UserListVO vo2 = logService.checkPw(vo);

        try {

            if(vo2 == null){
                modelAndView.addObject("list", "n");
            }else{
                modelAndView.addObject("list", "y");
            }
//            modelAndView.addObject("list", "n");
        } catch (Exception e) {
            System.out.println(e + ":::::::::::");
        }
        return modelAndView;
    }


    @RequestMapping(value = "login/updatePw.do", method = RequestMethod.POST)
    public ModelAndView updatePw(@RequestBody UserListVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jsonView");
        try {
            logService.pwUpdate(vo);
            modelAndView.addObject("list", "y");

        } catch (Exception e) {
            System.out.println(e + ":::::::::::");
        }

        return modelAndView;
    }

}